- Language: English

**Style**

- Begin all subject lines (i.e. first line of the commit message) with a capital letter
- Do not end the subject line with a period
- At most 72 characters in the subject line
- One empty line between the subject line and the message body
- Subject line should be written in imperative
- Wrap the body text at 72 characters

**Content**

- Explain what and why vs. how in commit message body
- If the commit includes bugged code, create an issue and assign it to yourself