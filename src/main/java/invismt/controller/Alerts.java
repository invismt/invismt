/*
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 */
package invismt.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import org.slf4j.Logger;

/**
 * Factory class to provide utility methods for creating {@link Alert}.
 *
 * @author Tim Junginger
 */
public final class Alerts {

	private Alerts() throws InstantiationException {
		throw new InstantiationException("Utility classes cannot be instantiated.");
	}

	/**
	 * Combines {@link #showAlert} with {@link #getLevelFromAlertType(AlertType)} to
	 * also log the alert message with the given logger.
	 *
	 * @param logger get the Logger to save the Log to
	 * @param type   is the Type of the Alert
	 * @param msg    is the Messing which will be Logged
	 */
	public static void showAlertAndLog(Logger logger, AlertType type, String msg) {
		showAlert(type, msg);
		switch (type) {
			case CONFIRMATION, INFORMATION, NONE:
				logger.info(msg);
				break;
			case ERROR:
				logger.error(msg);
				break;
			case WARNING:
				logger.warn(msg);
				break;
			default:
				logger.error(msg);
		}
	}

	/**
	 * Creates an Alert with the given AlertType and message and shows it. This
	 * method will return immediately.
	 *
	 * @param type is the Type of the Alert
	 * @param msg  is the Messing which will be Logged
	 */
	public static void showAlert(AlertType type, String msg) {
		Alert alert = new Alert(type, msg);
		alert.show();
	}
}
