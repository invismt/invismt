/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.controller;

import com.google.common.eventbus.EventBus;
import invismt.events.RuleRequestedEvent;
import invismt.grammar.Expression;
import invismt.rule.RuleWrapper;
import invismt.tree.PTElement;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Tim Junginger
 */
public class RuleContextMenuStarter implements EventDrivenController {

	private final ContextMenu menu;
	private final List<RuleWrapper> allRules;
	private Expression expr;
	private EventBus eventBus;
	private PTElement element;

	public RuleContextMenuStarter() {
		this.allRules = new ArrayList<>();
		this.menu = new ContextMenu();
	}

	/**
	 * Load all Rule in Context Menu
	 *
	 * @param rules is the RuleWrapper form which the Rule a Loaded
	 */
	public void setContent(List<RuleWrapper> rules) {
		this.allRules.addAll(rules);
	}

	/**
	 * Setter
	 *
	 * @param expr is the Expression to set
	 */
	public void setExpression(Expression expr) {
		this.expr = expr;
	}

	/**
	 * Setter
	 *
	 * @param element is the PTElement to set
	 */
	public void setPTElement(PTElement element) {
		this.element = element;
	}

	/**
	 * Start the Rule on which was Clicked
	 *
	 * @param node
	 */
	public void start(Node node) {
		List<MenuItem> items = new ArrayList<>();
		Set<RuleWrapper> applicable = this.getApplicapleRules();
		if (applicable.isEmpty()) {
			MenuItem item = new MenuItem("No rules applicable.");
			item.setDisable(true);
			items.add(item);
		} else if (!this.element.getChildren().isEmpty()) {
			applicable.clear();
			MenuItem item = new MenuItem("Rule already applied.");
			item.setDisable(true);
			items.add(item);
		}

		for (RuleWrapper wrapper : applicable) {
			MenuItem item = new MenuItem(wrapper.getName());
			item.setOnAction(event -> eventBus.post(new RuleRequestedEvent(wrapper, element, expr)));
			items.add(item);
		}
		this.menu.getItems().clear();
		this.menu.getItems().addAll(items);
		this.menu.show(node, Side.BOTTOM, 0, 0);
	}

	private Set<RuleWrapper> getApplicapleRules() {
		Set<RuleWrapper> applicable = new HashSet<>();
		if (element == null || expr == null) {
			return applicable;
		}
		for (RuleWrapper rule : allRules) {
			if (rule.mayApply(element.getScript(), expr)) {
				applicable.add(rule);
			}
		}
		return applicable;
	}

	@Override
	public void setEventBus(EventBus eventBus) {
		eventBus.register(this);
		this.eventBus = eventBus;
	}

}
