/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.controller;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import invismt.App;
import invismt.components.FormattedExpressionListCell;
import invismt.events.SaveToFileEvent;
import invismt.events.SetFormatterEvent;
import invismt.events.SetPTElementEvent;
import invismt.formatting.FormattedExpression;
import invismt.formatting.Formatter;
import invismt.formatting.PrettyPrinter;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.rule.RuleManager;
import invismt.tree.PTElement;
import invismt.util.NoSelectionModel;
import invismt.util.ScriptPrinter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Controller of ScriptView.
 */
public class ScriptController implements EventDrivenController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ScriptController.class);

	private static final String INDEX_METADATA = "index";
	private static final String VISIBILITY_METADATA = "visibility";

	private static final int FORMATTED_EXPRESSION_CACHE_SIZE = 1000;

	private final ObservableList<FormattedExpression> formattedExpressions;
	private final LoadingCache<Expression, FormattedExpression> formattedExpressionsCache;
	private Formatter formatter;
	private Script script;
	private RuleContextMenuStarter contextMenuStarter;

	@FXML
	private ListView<FormattedExpression> expressionList;

	/**
	 * Create new Script Controller.
	 */
	public ScriptController() {
		formatter = new PrettyPrinter();
		formattedExpressions = FXCollections.observableArrayList();

		formattedExpressionsCache = CacheBuilder.newBuilder().maximumSize(FORMATTED_EXPRESSION_CACHE_SIZE)
				.weakKeys()
				.build(new CacheLoader<>() {
					@Override
					public FormattedExpression load(Expression expression) {
						return formatter.print(expression);
					}
				});
	}

	@Override
	public void setEventBus(EventBus eventBus) {
		eventBus.register(this);
		contextMenuStarter.setEventBus(eventBus);
	}

	@FXML
	private void initialize() {
		RuleManager ruleManager = new RuleManager();
		contextMenuStarter = new RuleContextMenuStarter();
		contextMenuStarter.setContent(ruleManager.getRuleWrappers());

		// Disable selection of list cells
		expressionList.setSelectionModel(new NoSelectionModel<>());

		expressionList.setCellFactory(listView -> {
			FormattedExpressionListCell listCell = new FormattedExpressionListCell();

			// Setup context rul context menu
			listCell.setRuleContextMenuStarter(contextMenuStarter);

			// Setup collapse and expand functionality
			listCell.getVisibilityProperty().addListener(((observable, oldValue, newValue) -> {
				Expression expression = listCell.getItem().getExpression();
				script.setMetadata(expression, VISIBILITY_METADATA, newValue);
			}));

			// Setup drag and drop arrangement
			listCell.getDragAndDropEventProperty().addListener(((observable, oldValue, newValue) -> {
				if (newValue != null) {
					int draggedIndex = newValue.getDraggedIndex();
					int droppedIndex = newValue.getDroppedIndex();

					FormattedExpression dragged = formattedExpressions.get(draggedIndex);

					formattedExpressions.remove(draggedIndex);
					formattedExpressions.add(droppedIndex, dragged);

					// Update indices in script metadata
					for (FormattedExpression formattedExpression : formattedExpressions) {
						Expression expression = formattedExpression.getExpression();
						script.setMetadata(expression, INDEX_METADATA,
								formattedExpressions.indexOf(formattedExpression));
					}
				}
			}));

			return listCell;
		});
		this.setEventBus(App.getEventBus());
	}

	private void format() {
		if (script == null) {
			return;
		}

		List<Expression> expressions = script.getTopLevelExpressions();
		FormattedExpression[] formattedExpressionsArray = new FormattedExpression[expressions.size()];

		for (Expression expression : expressions) {
			try {
				FormattedExpression formattedExpression = formattedExpressionsCache.get(expression);
				Boolean isVisible = (Boolean) script.getMetadata(expression, VISIBILITY_METADATA);
				formattedExpression.setVisible(isVisible == null || isVisible);

				// Put result into formattedExpressionArray the index stored as script metadata
				int index = (Integer) script.getMetadata(expression, INDEX_METADATA);
				formattedExpressionsArray[index] = formattedExpression;

			} catch (ExecutionException e) {
				LOGGER.warn(e.getMessage(), e);
			}
		}

		formattedExpressions.setAll(formattedExpressionsArray);
		expressionList.setItems(formattedExpressions);
	}

	/**
	 * Handles an SetPTElementEvent that is passed via the EventBus.
	 *
	 * @param setPTElementEvent the SetPTElementEvent that is supposed to be
	 *                          handled.
	 */
	@Subscribe
	public void setPTElement(SetPTElementEvent setPTElementEvent) {
		PTElement element = setPTElementEvent.getPTElement();
		contextMenuStarter.setPTElement(element);
		script = element.getScript();
		format();
	}

	/**
	 * Handles a SetFormatterEvent that is passed via the EventBus.
	 *
	 * @param setFormatterEvent the SetFormatterEvent that is supposed to be
	 *                          handled.
	 */
	@Subscribe
	public void setFormatter(SetFormatterEvent setFormatterEvent) {
		formatter = setFormatterEvent.getNewFormatter();
		formattedExpressionsCache.invalidateAll();
		format();
	}

	/**
	 * Handles a request to save a given script (or the currently active one to
	 * file).
	 *
	 * @param evt is the Event to Save the active script
	 */
	@Subscribe
	public void handleSaveToFileEvent(SaveToFileEvent evt) {
		if (evt.getScript() == null) {
			this.saveToFile(this.script, evt.getFile());
		} else {
			this.saveToFile(evt.getScript(), evt.getFile());
		}
	}

	private void saveToFile(Script script, File file) {
		Expression last = script.getTopLevelExpressions().get(script.getTopLevelExpressions().size() - 1);
		Script withCheckSat = script
				.addTopLevelListInstead(last, List.of(last.deepCopy(), new SyntaxCommand.CheckSat()));
		ScriptPrinter printer = new ScriptPrinter();
		String content = printer.print(withCheckSat);
		try {
			Files.writeString(Paths.get(file.getAbsolutePath()), content);
		} catch (IOException e) {
			Alerts.showAlertAndLog(LOGGER, AlertType.ERROR, "Could not save file: " + e.getMessage());
		}
	}
}
