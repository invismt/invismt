/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import com.google.gson.Gson;
import invismt.grammar.*;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;

/**
 * Formats an {@link Expression} in a more human-readable way
 * than the SMT-LIB language.
 * <p>
 * E.g. that means that mathematical expressions are printed
 * in a more mathematical way.
 */
public class PrettyPrinter implements Formatter {

  private static final String CONFIG_PATH = "/config/PrettyPrinter.json";

  private static final String NAME = "InViSMT Pretty Printer";
  private static final String FILE_NOT_FOUND = "Pretty Printer config file " + CONFIG_PATH + " could not be found.";
  private static final PrettyPrinterConfig.Notation DEFAULT_NOTATION = PrettyPrinterConfig.Notation.PREFIX;

  private static final String RESERVED = "Reserved";
  private static final String CONSTANT = "Constant";
  private static final String COMMAND = "Command";
  private static final String GENERAL_EXPRESSION = "GeneralExpression";
  private static final String ITE = "IfThenElse";

  private final PrettyPrinterConfig config;
  private FormattedExpressionBuilder builder;

  /**
   * Create a new PrettyPrinter object whose configuration file
   * has the path {@link #CONFIG_PATH}.
   */
  public PrettyPrinter() {
    InputStream resourceStream = getClass().getResourceAsStream(CONFIG_PATH);
    if (resourceStream == null) {
      throw new IllegalArgumentException(FILE_NOT_FOUND);
    }
    Reader configReader = new InputStreamReader(resourceStream);
    config = new Gson().fromJson(configReader, PrettyPrinterConfig.class);
  }

  @Override
  public String getName() {
    return NAME;
  }

  /**
   * Create a {@link FormattedExpression} representing the given
   * {@link Expression} by visiting that expression and using
   * a {@link FormattedExpressionBuilder} to build the
   * FormattedExpression during the visiting process.
   *
   * @param expression the {@link Expression} that is to be formatted
   * @return the FormattedExpression representing the given expression
   */
  @Override
  public FormattedExpression print(Expression expression) {
    builder = new FormattedExpressionBuilder();
    expression.accept(this);
    return builder.getFormattedExpression(expression);
  }

  @Override
  public Void visit(GeneralExpression expression) {
    builder.append(expression.toString(), expression, GENERAL_EXPRESSION);
    return null;
  }

  @Override
  public Void visit(QuantifiedTerm.ForAllTerm forallTerm) {
    builder.append("\u2200 ", forallTerm, RESERVED);
    printWithDelimiter(forallTerm.getVariables(), ", ", forallTerm);
    builder.append(":", forallTerm);
    builder.createBlock();

    builder.increaseIndentationLevel();
    forallTerm.getTerm().accept(this);
    builder.createBlock();
    builder.decreaseIndentationLevel();
    return null;
  }

  @Override
  public Void visit(QuantifiedTerm.ExistsTerm existsTerm) {
    builder.append("\u2203 ", existsTerm, RESERVED);
    printWithDelimiter(existsTerm.getVariables(), ", ", existsTerm);
    builder.append(":", existsTerm, RESERVED);
    builder.createBlock();

    builder.increaseIndentationLevel();
    existsTerm.getTerm().accept(this);
    builder.createBlock();
    builder.decreaseIndentationLevel();

    return null;
  }

  @Override
  public Void visit(LetTerm letTerm) {
    builder.append("let ", letTerm, RESERVED);
    printWithDelimiter(letTerm.getBindings(), ", ", letTerm);
    builder.append(" in", letTerm, RESERVED);
    builder.createBlock();

    builder.increaseIndentationLevel();
    letTerm.getTerm().accept(this);
    builder.createBlock();
    builder.decreaseIndentationLevel();
    return null;
  }

  @Override
  public Void visit(AnnotationTerm annotationTerm) {
    annotationTerm.getTerm().accept(this);
    annotationTerm.getAttributes().forEach(attribute -> {
        builder.append(" ", null);
        builder.setAssociatedExpression(attribute);
        attribute.accept(this);
        builder.resetAssociatedExpression();
      });
    return null;
  }

  @Override
  public Void visit(MatchTerm matchTerm) {
    builder.append("match ", matchTerm, RESERVED);
    matchTerm.getTerm().accept(this);
    builder.createBlock();
    builder.increaseIndentationLevel();

    matchTerm.getMatchCases().forEach((matchCase -> {
          matchCase.accept(this);
          builder.createBlock();
        }));

    builder.decreaseIndentationLevel();
    return null;
  }

  @Override
  public Void visit(ConstantTerm constantTerm) {
    builder.setStyleClass(CONSTANT);
    builder.setAssociatedExpression(constantTerm);
    constantTerm.getConstant().accept(this);
    builder.resetAssociatedExpression();
    builder.resetStyleClass();
    return null;
  }

  @Override
  public Void visit(IdentifierTerm identifierTerm) {
    String identifierName = identifierTerm.getIdentifier().getQualifiedIdentifier().getSymbol().getValue();

    // Handle if-then-else terms
    if (identifierTerm.getIdentifier().equals(ExpressionFactory.ITE)
        && identifierTerm.getIdentifiedTerms().size() == 3) {
      printIte(identifierTerm);
      return null;
    }

    // Use the specified notation if identifier is in config and default notation otherwise
    PrettyPrinterConfig.Notation notation = config.getNotation(identifierName);
    notation = notation != null ? notation : DEFAULT_NOTATION;

    boolean useParentheses = config.useParentheses(identifierName);

    QualIdentifier identifier = identifierTerm.getIdentifier();
    Term[] identifiedTerms = identifierTerm.getIdentifiedTerms().toArray(Term[]::new);

    notation.printInNotation(this, builder, useParentheses, identifierTerm, identifier, identifiedTerms);

    return null;
  }

  @Override
  public Void visit(VariableBinding variableBinding) {
    variableBinding.getVariableName().accept(this);
    builder.append(" := ", variableBinding);
    variableBinding.getBindingTerm().accept(this);
    return null;
  }

  @Override
  public Void visit(SortedVariable sortedVariable) {
    sortedVariable.getVariableName().accept(this);
    builder.append(" \u2208 ", sortedVariable);
    sortedVariable.getVariableType().accept(this);
    return null;
  }

  @Override
  public Void visit(Identifier identifier) {
    // Resolve identifier name and to fetch alias from config
    String identifierName = identifier.getSymbol().getValue();
    String alias = config.getAlias(identifierName);

    // Append alias if configured or name of identifier otherwise
    builder.append(alias != null ? alias : identifierName, identifier);

    // Print indices separated by comma in brackets
    if (!identifier.getIndices().isEmpty()) {
      builder.append("[", identifier);
      for (int i = 0; i < identifier.getIndices().size(); i++) {
        if (i != 0) {
          builder.append(", ", identifier);
        }
        identifier.getIndices().get(i).accept(this);
      }
      builder.append("]", identifier);
    }

    return null;
  }

  @Override
  public Void visit(Index.NumeralIndex index) {
    index.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(Index.SymbolIndex index) {
    index.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(QualIdentifier qualIdentifier) {
    qualIdentifier.getQualifiedIdentifier().accept(this);

    Optional<Sort> identifierSort = qualIdentifier.getIdentifiedSort();
    if (identifierSort.isPresent()) {
      builder.append(" as ", qualIdentifier, RESERVED);
      identifierSort.get().accept(this);
    }
    return null;
  }

  @Override
  public Void visit(AttributeValue.ListAttributeValue value) {
    builder.append("(", value);
    List<Expression> exprs = value.getChildren();
    for (int i = 0; i < exprs.size(); i++) {
      exprs.get(i).accept(this);
      if (i < exprs.size() - 1) {
        builder.append(" ", value);
      }
    }
    builder.append(")", value);
    return null;
  }

  @Override
  public Void visit(AttributeValue.ConstantAttributeValue value) {
    value.getConstant().accept(this);
    return null;
  }

  @Override
  public Void visit(AttributeValue.SymbolAttributeValue value) {
    value.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(Attribute attribute) {
    attribute.getKeyword().accept(this);

    Optional<AttributeValue> attributeValue = attribute.getAttributeValue();
    if (attributeValue.isPresent()) {
      builder.append(" ", null);
      attributeValue.get().accept(this);
    }
    return null;
  }

  @Override
  public Void visit(Token.NumeralToken token) {
    builder.append(token.getValue(), token);
    return null;
  }

  @Override
  public Void visit(Token.StringToken token) {
    builder.append(token.getValue(), token);
    return null;
  }

  @Override
  public Void visit(Token.BinaryToken token) {
    builder.append(String.format("0b%s", token.getValue()), token);
    return null;
  }

  @Override
  public Void visit(Token.DecimalToken token) {
    builder.append(token.getValue(), token);
    return null;
  }

  @Override
  public Void visit(Token.HexadecimalToken token) {
    builder.append(String.format("0x%s", token.getValue()), token);
    return null;
  }

  @Override
  public Void visit(Token.SymbolToken token) {
    builder.append(token.getValue(), token);
    return null;
  }

  @Override
  public Void visit(Token.ReservedWord token) {
    builder.append(token.getValue(), token);
    return null;
  }

  @Override
  public Void visit(Token.Keyword token) {
    builder.append(token.getValue(), token);
    return null;
  }

  @Override
  public Void visit(SpecConstant.NumeralConstant constant) {
    constant.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(SpecConstant.StringConstant constant) {
    constant.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(SpecConstant.DecimalConstant constant) {
    constant.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(SpecConstant.HexadecimalConstant constant) {
    constant.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(SpecConstant.BinaryConstant constant) {
    constant.getToken().accept(this);
    return null;
  }

  @Override
  public Void visit(Sort sort) {
    sort.getSortIdentifier().accept(this);
    sort.getSorts().forEach(s -> {
        builder.append(" ", sort);
        s.accept(this);
      });
    return null;
  }

  @Override
  public Void visit(Pattern pattern) {
    pattern.getSymbol().accept(this);
    pattern.symbolList().forEach((symbol -> symbol.accept(this)));
    return null;
  }

  @Override
  public Void visit(MatchCase matchCase) {
    matchCase.getMatchedPattern().accept(this);
    builder.append(": ", matchCase);
    matchCase.getMatchedTerm().accept(this);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.Assert assertion) {
    assertion.getTerm().accept(this);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.DeclareConstant declaration) {
    builder.append("const ", declaration, RESERVED);
    declaration.getConstName().accept(this);
    builder.append(": ", declaration);
    declaration.getConstType().accept(this);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.CheckSat checkSat) {
    printSimpleCommand(checkSat);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.Pop pop) {
    printSimpleCommand(pop);
    builder.append(" ", pop);
    pop.getMagnitude().accept(this);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.Push push) {
    printSimpleCommand(push);
    builder.append(" ", push);
    push.getMagnitude().accept(this);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.Exit exit) {
    printSimpleCommand(exit);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.Reset reset) {
    printSimpleCommand(reset);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.DeclareFunction declareFunction) {
    builder.append("fun ", declareFunction, RESERVED);
    declareFunction.getConstName().accept(this);
    builder.append(": ", declareFunction, RESERVED);

    int definitionAreaDimension = declareFunction.getDefinitionArea().size();

    if (definitionAreaDimension == 0) {
      builder.append("\u2205", declareFunction);
    } else if (definitionAreaDimension == 1) {
      declareFunction.getDefinitionArea().get(0).accept(this);
    } else if (definitionAreaDimension > 1) {
      builder.append("(", declareFunction);
      printWithDelimiter(declareFunction.getDefinitionArea(), ", ", declareFunction);
      builder.append(")", declareFunction);
    }

    builder.append(" \u2192 ", declareFunction, RESERVED);
    declareFunction.getTargetArea().accept(this);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.GetInfo getInfo) {
    // TODO Implement formatting method for info_flag instances
    builder.append(getInfo.toString(), getInfo, COMMAND);
    return null;
  }

  @Override
  public Void visit(SyntaxCommand.SetOption setOption) {
    builder.append(setOption.toString(), setOption, COMMAND);
    return null;
  }

  /**
   * Print a single command that only consists of its {@link SyntaxCommand#getCommandName()}.
   */
  private void printSimpleCommand(SyntaxCommand command) {
    builder.setAssociatedExpression(command);
    builder.setStyleClass(COMMAND);
    command.getCommandName().accept(this);
    builder.resetAssociatedExpression();
    builder.resetStyleClass();
  }

  private void printIte(IdentifierTerm identifierTerm) {
    builder.append("(", identifierTerm, ITE);
    identifierTerm.getIdentifiedTerms().get(0).accept(this);
    builder.append(" ? ", identifierTerm, ITE);
    identifierTerm.getIdentifiedTerms().get(1).accept(this);
    builder.append(" : ", identifierTerm, ITE);
    identifierTerm.getIdentifiedTerms().get(2).accept(this);
    builder.append(")", identifierTerm, ITE);
  }

  private void printWithDelimiter(List<? extends Expression> expressions, String delimiter, Expression parent) {
    int lastIndex = expressions.size() - 1;

    if (lastIndex < 0) {
      return;
    }

    for (int i = 0; i < lastIndex; i++) {
      expressions.get(i).accept(this);
      builder.append(delimiter, parent);
    }
    expressions.get(lastIndex).accept(this);
  }
}
