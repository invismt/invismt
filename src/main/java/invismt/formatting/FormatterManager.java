/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.util.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Loads {@link Formatter Formatters} registered in META-INF/services and
 * provides clients with instances of them.
 */
public class FormatterManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(FormatterManager.class);
	private static final Class<? extends Formatter> DEFAULT_FORMATTER_CLASS = PrettyPrinter.class;

	private final HashMap<Class<? extends Formatter>, String> installedFormatters;

	/**
	 * Create a new FormatterManager.
	 * Loads installed formatters from META-INF via {@link #loadFormatters()}.
	 */
	public FormatterManager() {
		installedFormatters = new HashMap<>();
		loadFormatters();
	}

	/**
	 * Loads installed formatters from META-INF:
	 *
	 * @see <a href="main/resources/META-INF/services/invismt.formatting.Formatter">Formatter config</a>
	 */
	private void loadFormatters() {
		// Load installed formatters using ServiceLoader
		ServiceLoader.load(Formatter.class)
				.forEach(formatter -> installedFormatters.put(formatter.getClass(), formatter.getName()));
	}

	/**
	 * @return a {@link Set} of {@link Formatter#getName() names}
	 * of installed Formatters {@link #installedFormatters}
	 */
	public Set<Tuple<Class<? extends Formatter>, String>> getInstalledFormatters() {
		Set<Tuple<Class<? extends Formatter>, String>> entries = new HashSet<>();
		for (Map.Entry<Class<? extends Formatter>, String> entry : installedFormatters.entrySet()) {
			Class<? extends Formatter> formatterClass = entry.getKey();
			entries.add(new Tuple<>(formatterClass, entry.getValue()));
		}
		return entries;
	}

	/**
	 * @return the default {@link Formatter} name according to this FormatterManager
	 */
	public Class<? extends Formatter> getDefaultFormatterClass() {
		return DEFAULT_FORMATTER_CLASS;
	}

	/**
	 * Creates and returns an Optional of a new instance of the {@link Formatter}
	 * associated with a given String name in {@link #getInstalledFormatters()}.
	 * If no such {@link Formatter} is present an empty Optional is returned.
	 *
	 * @param formatterClass the class of the {@link Formatter} of which an instance is requested
	 * @return a new instance of the {@link Formatter} with the given name if present, otherwise null
	 */
	public Optional<Formatter> getInstance(Class<? extends Formatter> formatterClass) {
		if (installedFormatters.containsKey(formatterClass)) {
			try {
				return Optional.of(formatterClass.getDeclaredConstructor().newInstance());
			} catch (InstantiationException | IllegalAccessException
					| InvocationTargetException | NoSuchMethodException e) {
				LOGGER.warn(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

}
