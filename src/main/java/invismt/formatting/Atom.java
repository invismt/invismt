/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;

import java.util.Optional;

/**
 * Smallest indivisible piece of a {@link FormattedExpression}.
 * Represents a part of an {@link Expression}.
 * Atoms can be given style class if they are to be associated style class.
 *
 * @author Jonathan Hunz
 */
public class Atom {

	private final String text;
	private final Expression associatedExpression;
	private String styleClass;

	/**
	 * Create an atom without a specified style class by calling its constructor
	 * with the String text representing this atom's content and the expression
	 * this atom is associated with.
	 * That means that an expression's representation consists of
	 * the representations of all the Atoms associated with it, hence
	 * an Atom's String text serves a part of an {@link Expression}.
	 *
	 * @param text                 String that represents the content of this Atom
	 * @param associatedExpression {@link Expression} which is represented by this Atom
	 */
	public Atom(String text, Expression associatedExpression) {
		this.text = text;
		this.associatedExpression = associatedExpression;
	}

	/**
	 * Create an atom with a specified style class by calling its constructor
	 * with the String text representing this atom's content and the expression
	 * this atom is associated with, as well as a style class for the
	 * representation of this Atom.
	 * <p>
	 * How that style class is realized and what exactly it stands for is not part of the Atom class' definition.
	 * The style class String can for example be a colour name defining the colour in which the atom should be printed.
	 *
	 * @param text                 String that represents the textual content of this Atom
	 * @param associatedExpression {@link Expression} this Atom represents a part of
	 * @param styleClass           String that represents the style class of this Atom in some way
	 */
	public Atom(String text, Expression associatedExpression, String styleClass) {
		this(text, associatedExpression);
		this.styleClass = styleClass;
	}

	/**
	 * @return String content of this Atom that represents a part
	 * of the associated {@link Expression}
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return {@link Expression} whose representation this Atom is a part of
	 */
	public Expression getAssociatedExpression() {
		return associatedExpression;
	}

	/**
	 * @return Optional of the String representation of the style class of this Atom
	 * if a style class has been set. Empty Optional otherwise.
	 */
	public Optional<String> getStyleClass() {
		if (styleClass == null) {
			return Optional.empty();
		}
		return Optional.of(styleClass);
	}
}
