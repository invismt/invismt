/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Simplifies the creation of {@link FormattedExpression FormattedExpressions}
 * by providing a mutable representation.
 *
 * @author Jonathan Hunz
 */
public class FormattedExpressionBuilder {

	private final List<Block> blocks;
	private final List<Atom> atomBuffer;
	private final HashMap<Expression, Collection<Atom>> associatedAtoms;
	private int indentationLevel;

	/**
	 * The style class that will be assigned to every Atom that is created
	 * by this FormattedExpressionBuilder if this attribute is not null.
	 */
	private String styleClass;

	/**
	 * The universal Expression that will be associated with every Atom
	 * that is created by this FormattedExpressionBuilder if this attribute
	 * is not null.
	 */
	private Expression associatedExpression;

	/**
	 * Create a new FormattedExpressionBuilder with an empty list of
	 * blocks {@link #blocks}, an empty Atom buffer {@link #atomBuffer}
	 * and an empty Mapping of Expressions onto their associated Atoms
	 * {@link #associatedAtoms} that will be used in the creation
	 * of a new FormattedExpression with this FormattedExpressionBuilder.
	 * <p>
	 * All other attributes are null after construction.
	 */
	public FormattedExpressionBuilder() {
		blocks = new ArrayList<>();
		atomBuffer = new ArrayList<>();
		associatedAtoms = new HashMap<>();
	}

	/**
	 * Create a new {@link Block} from the current {@link #atomBuffer}
	 * of {@link Atom Atoms} and clear the buffer afterwards.
	 */
	public void createBlock() {
		if (atomBuffer.isEmpty()) {
			return;
		}

		Block block = new Block(indentationLevel, new ArrayList<>(atomBuffer));
		blocks.add(block);
		atomBuffer.clear();
	}

	/**
	 * Increase the indentation level that is used when a {@link Block}
	 * is created with the {@link #createBlock()} method by 1.
	 */
	public void increaseIndentationLevel() {
		indentationLevel++;
	}

	/**
	 * Decrease the indentation level that is used when a {@link Block}
	 * is created with the {@link #createBlock()} method by 1.
	 */
	public void decreaseIndentationLevel() {
		indentationLevel--;
	}

	/**
	 * Create a new {@link Atom} with the String content
	 * (textual representation) and associated {@link Expression}
	 * and add the created Atom to the Atom buffer {@link #atomBuffer}.
	 * <p>
	 * If a  has been set with {@link #setStyleClass(String)} it
	 * will be used as the {@link Atom#getStyleClass()} of the created
	 * {@link Atom}.
	 * Otherwise an {@link Atom} of unspecified style class is created.
	 * <p>
	 * If a {@link #styleClass} has been set with {@link #setStyleClass(String)} it
	 * will be used as the {@link Atom#getStyleClass()} of the created
	 * {@link Atom}.
	 * The parameter associatedExpression is overridden by
	 * {@link #associatedExpression} if that attribute is not null.
	 *
	 * @param text                 String content of the created Atom that represents a part
	 *                             of the associatedExpression
	 * @param associatedExpression {@link Expression} whose representation the
	 *                             created Atom is a part of
	 */
	public void append(String text, Expression associatedExpression) {
		Expression expression = this.associatedExpression == null ? associatedExpression : this.associatedExpression;
		Atom atom;
		if (styleClass != null) {
			atom = new Atom(text, expression, styleClass);
		} else {
			atom = new Atom(text, expression);
		}
		atomBuffer.add(atom);

		if (associatedAtoms.containsKey(expression)) {
			associatedAtoms.get(expression).add(atom);
		} else {
			Collection<Atom> atoms = new ArrayList<>();
			atoms.add(atom);
			associatedAtoms.put(expression, atoms);
		}
	}

	/**
	 * Call the {@link #setStyleClass(String)} method before appending
	 * a new {@link Atom} with the {@link #append(String, Expression)} method.
	 *
	 * @param text                 String content of the created Atom that represents a part of the associatedExpression
	 * @param associatedExpression {@link Expression} whose representation the created Atom is a part of
	 * @param styleClass           the {@link #styleClass} of the atom
	 */
	public void append(String text, Expression associatedExpression, String styleClass) {
		String styleClassOld = this.styleClass;
		setStyleClass(styleClass);
		append(text, associatedExpression);
		setStyleClass(styleClassOld);
	}

	/**
	 * Returns a {@link FormattedExpression} which contains the {@link #blocks}
	 * that have been created with the {@link #createBlock()} method until now.
	 * The created {@link FormattedExpression} represents the given {@link Expression}.
	 * <p>
	 * If {#atomBuffer} isn't currently empty, a new {@link Block} is created
	 * with the Atoms in the buffer via {@link #createBlock()} before creating the
	 * FormattedExpression.
	 *
	 * @param expression the expression whose {@link FormattedExpression} will be created
	 * @return a {@link FormattedExpression} containing the current
	 * data of this FormattedExpressionBuilder
	 */
	public FormattedExpression getFormattedExpression(Expression expression) {
		// Create block if atoms where appended but createBlock wasn't called
		if (!atomBuffer.isEmpty()) {
			createBlock();
		}
		FormattedExpression formatted = new FormattedExpression(blocks, expression);
		blocks.clear();
		atomBuffer.clear();
		return formatted;
	}

	/**
	 * Set the {@link #styleClass style class} that is assigned to every {@link Atom}
	 * that is created via {@link #append(String, Expression)} (as long
	 * as this colour is not changed by another call of the
	 * setStyleClass() or the {@link #resetStyleClass()} method).
	 *
	 * @param styleClass String representation of a color in the format specified in
	 *                   {@link javafx.scene.paint.Color#web(String)}.
	 */
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	/**
	 * Reset {@link #styleClass} to null.
	 * Thus, afterwards every created {@link Atom} will be of unspecified style class
	 * until another style class is set via the {@link #setStyleClass(String)} method.
	 */
	public void resetStyleClass() {
		styleClass = null;
	}

	/**
	 * Set the {@link #associatedExpression Expression} that will be
	 * represented by all {@link Atom Atoms} that are created after
	 * the call of this method.
	 * <p>
	 * This overrides the associatedExpression parameter passed in
	 * {@link #append(String, Expression associatedExpression)}
	 * and can be changed by another call of setAssociatedExpression()
	 * or the {@link #resetAssociatedExpression()} method.
	 *
	 * @param associatedExpression {@link Expression} which is represented
	 *                             by the {@link Atom Atoms}
	 *                             appended afterwards
	 */
	public void setAssociatedExpression(Expression associatedExpression) {
		this.associatedExpression = associatedExpression;
	}

	/**
	 * Reset {@link #associatedExpression} to null.
	 * <p>
	 * Hence, {@link #append(String, Expression associatedExpression)}
	 * will use the parameter associatedExpression for Atom creation until
	 * {@link #setAssociatedExpression(Expression)} is called again.
	 */
	public void resetAssociatedExpression() {
		associatedExpression = null;
	}
}
