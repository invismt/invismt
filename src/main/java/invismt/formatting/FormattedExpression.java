/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A representation of an {@link Expression} which serves
 * as a structure in between the expression itself and
 * its visual or textual display.
 * <p>
 * For exemplary use, see {@link Formatter} where FormattedExpressions
 * objects are produced out of {@link Expression} objects in order to create
 * the expressions' representations.
 * <p>
 * A formatted expression consists of {@link Block Blocks}
 * which are made up of indivisible pieces of text ({@link Atom Atoms}).
 *
 * @author Jonathan Hunz
 */
public class FormattedExpression {

	private final List<Block> blocks;
	private final Expression expression;
	// Whether or not the given FormattedExpression should be visual when depicted graphically.
	private boolean isVisible;

	/**
	 * Create a new FormattedExpression object by giving its constructor an ordered
	 * List of {@link Block Blocks} it consists of as well as the Expression that is
	 * represented by this formatted expression.
	 *
	 * @param blocks     List of {@link Block Blocks} that constitute this FormattedExpression
	 * @param expression {@link Expression} represented by the FormattedExpression at hand
	 */
	public FormattedExpression(List<Block> blocks, Expression expression) {
		this.blocks = new ArrayList<>(blocks);
		this.expression = expression;
		isVisible = true;
	}

	/**
	 * @return Ordered List of the {@link Block Blocks} which constitute
	 * this FormattedExpression
	 */
	public List<Block> getBlocks() {
		return new LinkedList<>(blocks);
	}

	/**
	 * Returns whether this FormattedExpression is supposed to be
	 * displayed or hidden when graphically depicted.
	 *
	 * @return the visibility of this FormattedExpression when graphically depicted
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/**
	 * Sets the visibility of this FormattedExpression when it
	 * is graphically depicted.
	 * The value that is set here is returned by {@link #isVisible()}.
	 *
	 * @param visible the desired visibility of this FormattedExpression
	 *                when graphically depicted
	 */
	public void setVisible(boolean visible) {
		isVisible = visible;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (Block block : blocks) {
			for (Atom atom : block.getAtoms()) {
				stringBuilder.append(atom.getText());
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * @return the Expression object that is represented by this FormattedExpression
	 */
	public Expression getExpression() {
		return expression;
	}
}
