/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents an ordered list of {@link Atom Atoms}
 * that visually belong together.
 * <p>
 * A Block can be used to handle multiple Atoms as a single unit
 * visually, e.g. for indentation of all atoms belonging to a Block.
 * The Atoms in a Block don't necessarily have to belong to the
 * same {@link invismt.grammar.Expression}.
 *
 * @author Jonathan Hunz
 */
public class Block {

	private final int indentationLevel;
	private final List<Atom> atoms;

	/**
	 * Create a new Block by calling its constructor with an int value representing
	 * the Block's visual offset when being displayed and a list of Atoms that
	 * constitute this Block in the order of the list.
	 *
	 * @param indentationLevel visual offset that should be used to display this Block.
	 * @param atoms            Ordered List of {@link Atom Atoms} that form this Block
	 */
	public Block(int indentationLevel, List<Atom> atoms) {
		this.indentationLevel = indentationLevel;
		this.atoms = new LinkedList<>(atoms);
	}

	/**
	 * @return Ordered List of Atoms that constitute this Block.
	 */
	public List<Atom> getAtoms() {
		return new LinkedList<>(atoms);
	}

	/**
	 * @return visual offset that should be used to display this Block.
	 */
	public int getIndentationLevel() {
		return indentationLevel;
	}
}
