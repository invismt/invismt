/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.components;

import invismt.tree.PTElement;
import invismt.tree.ProofState;
import invismt.tree.ProofState.CausedBy;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Custom {@link TreeCell} for {@link PTElement} to show a start/stop button for
 * a solver, the {@link ProofState} of the element and the {@link invismt.rule.Rule} it was
 * created from.
 *
 * @author Tim Junginger
 */
public class ProofTreeElementCell extends TreeCell<PTElement> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProofTreeElementCell.class);
	private static Image startImage = new Image("/icons/start.png");
	private static Image stopImage = new Image("/icons/stop.png");
	private final Consumer<PTElement> onClick;
	private final Consumer<PTElement> onStartSolver;
	private final Consumer<PTElement> onStopSolver;
	@FXML
	private HBox container;

	@FXML
	private Button startSolverButton;

	@FXML
	private Label text;

	@FXML
	private Label stateLabel;

	/**
	 * Instantiates a new ProofTreeElementCell.
	 *
	 * @param onClick       the event that should occur once this
	 *                      ProofTreeElementCell gets clicked on
	 * @param onStartSolver the event that should occur once the startSolverButton
	 *                      is fired
	 * @param onStopSolver  the event that should occur once the stopSolverButton is
	 *                      fired
	 */
	public ProofTreeElementCell(Consumer<PTElement> onClick, Consumer<PTElement> onStartSolver,
	                            Consumer<PTElement> onStopSolver) {
		this.onClick = onClick;
		this.onStartSolver = onStartSolver;
		this.onStopSolver = onStopSolver;
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ProofTreeElementCell.fxml"));
		loader.setController(this);
		try {
			container = loader.load();
		} catch (IOException e) {
			LOGGER.warn(e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateItem(PTElement item, boolean empty) {
		super.updateItem(item, empty);
		this.setFocusTraversable(false);
		this.stateLabel.setDisable(true);
		this.text.setDisable(true);
		this.stateLabel.setOpacity(1);
		this.text.setOpacity(1);
		if (item != null && !empty) {
			// set background color
			if (item.getProofTree() != null && item.getProofTree().getActivePTElement() == item) {
				container.setStyle("-fx-background-color: -color-highlight;");
			} else {
				container.setStyle("-fx-background-color: transparent;");
			}
			// set proof state
			if (item.isLocked()) {
				stateLabel.setText(null);
				stateLabel.setGraphic(new ProgressIndicator());
				stopButton(item);
			} else {
				stateLabel.setText(item.getSatisfiable().toString());
				stateLabel.setGraphic(null);
				startButton(item);
			}

			this.text.setText("From " + ((item.getRule() == null) ? "NONE" : item.getRule().getName()));
			setOnMouseClicked(event -> onClick.accept(item));
			this.setGraphic(this.container);
		} else {
			setGraphic(null);
			container.setStyle("-fx-background-color: transparent;");
		}
	}

	private void startButton(PTElement item) {
		startSolverButton.setGraphic(new ImageView(startImage));
		startSolverButton.setOnAction(event -> this.onStartSolver.accept(item));
		startSolverButton.setDisable(true);
		if (item.getSatisfiable() == ProofState.UNKNOWN && !item.isLocked()) {
			startSolverButton.setDisable(false);
		}
		if (item.getSatisfiable().causedBy() == CausedBy.INTERNAL) {
			startSolverButton.setDisable(false);
		}
	}

	private void stopButton(PTElement item) {
		startSolverButton.setGraphic(new ImageView(stopImage));
		startSolverButton.setOnAction(event -> this.onStopSolver.accept(item));
		startSolverButton.setDisable(true);
		if (item.isLocked()) {
			startSolverButton.setDisable(false);
		}
	}

}
