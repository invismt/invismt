/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.components;

import invismt.tree.PTElement;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Custom {@link TreeItem} to allow listening to {@link PTElement} directly.
 *
 * @author Tim Junginger
 */
public class PTTreeItem extends TreeItem<PTElement> implements PropertyChangeListener {

	private TreeView<PTElement> treeView;

	/**
	 * Creates a new TreeItem which listens to the given PTElement.
	 *
	 * @param ptElement Element to listen to
	 * @param treeView  is a JavaFX element to show the Tree
	 */
	public PTTreeItem(PTElement ptElement, TreeView<PTElement> treeView) {
		super(ptElement);
		this.treeView = treeView;
		ptElement.addChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (!evt.getPropertyName().equals(PTElement.class.getName())) {
			return;
		}
		PTElement ptElement = (PTElement) evt.getNewValue();
		List<PTElement> modelList = ptElement.getChildren();
		List<TreeItem<PTElement>> viewList = this.getChildren();

		if (modelList.size() != viewList.size()) {
			for (TreeItem<PTElement> item : viewList) {
				item.getValue().rmChangeListener((PTTreeItem) item);
			}

			this.getChildren().clear();

			for (PTElement element : modelList) {
				PTTreeItem ptTreeItem = new PTTreeItem(element, treeView);
				this.getChildren().add(ptTreeItem);
				ptTreeItem.setExpanded(true);
				element.addChangeListener(ptTreeItem);
			}

		}

		// Update treeView
		treeView.refresh();
		treeView.layout();
	}

	/**
	 * Removes this as listeners and calls this method on children.
	 */
	public void cleanUp() {
		this.getValue().rmChangeListener(this);
		for (TreeItem<PTElement> item : this.getChildren()) {
			((PTTreeItem) item).cleanUp();
		}
	}
}
