/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.components;

import javafx.scene.control.Control;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Skin;
import javafx.scene.control.skin.ScrollPaneSkin;

/**
 * An extension of a ScrollPane that doesn't hide parts of it's content.
 */
public class ListCellScrollPane extends ScrollPane {

	@Override
	protected Skin<?> createDefaultSkin() {
		return new ListCellScrollPaneSkin(this);
	}

	private static class ListCellScrollPaneSkin extends ScrollPaneSkin {

		/**
		 * Creates a new ScrollPaneSkin instance, installing the necessary child nodes
		 * into the Control {@link Control#getChildren() children} list, as well as the
		 * necessary input mappings for handling key, mouse, etc events.
		 *
		 * @param control The control that this skin should be installed onto.
		 */
		ListCellScrollPaneSkin(ScrollPane control) {
			super(control);
		}

		@Override
		protected double computePrefHeight(double width, double topInset, double rightInset, double bottomInset,
		                                   double leftInset) {
			double prefHeight = super.computePrefHeight(width, topInset, rightInset, bottomInset, leftInset);
			if (getSkinnable().getHbarPolicy() == ScrollBarPolicy.AS_NEEDED && getHorizontalScrollBar().isVisible()) {
				prefHeight += getHorizontalScrollBar().prefHeight(-1);
			}
			return prefHeight;
		}
	}
}
