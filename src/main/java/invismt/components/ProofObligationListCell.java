/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.components;

import invismt.tree.ProofObligation;
import javafx.scene.control.ListCell;

import java.util.function.Consumer;

/**
 * @author Tim Junginger
 */
public class ProofObligationListCell extends ListCell<ProofObligation> {

	private final Consumer<ProofObligation> onClick;

	/**
	 * Creates a new ProofObligationListCell.
	 *
	 * @param onClick save teh OnClick event as attribute
	 */
	public ProofObligationListCell(Consumer<ProofObligation> onClick) {
		this.onClick = onClick;
	}

	@Override
	public void updateItem(ProofObligation item, boolean empty) {
		super.updateItem(item, empty);
		if (empty || item == null) {
			setDisable(true);
			setText(null);
			setGraphic(null);
		} else {
			setDisable(false);
			setText(item.getProofTree().getRootPTElement().getScript().getTopLevelExpressions().size()
					+ " statements | " + item.getTreeCommandHistory().getDone().size() + " changes made | " + item
					.getProofTree().getRootPTElement().getSatisfiable());
			setOnMouseClicked(event -> onClick.accept(item));
		}

	}
}
