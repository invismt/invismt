/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.File;

/**
 * An executable {@link javafx.concurrent.Service} implementation
 * which represents the process of automatically solving an SMT-Problem.
 * This implementation does not provide any additional functionality,
 * but it is advised to read the documentation to {@link javafx.concurrent.Service}.
 *
 * @author Tim Junginger
 */
public class SolverService extends Service<SolverAnswer> {

	private final Solver solver;
	private final File file;

	/**
	 * Creates a new SolverService with the given file and solver.
	 *
	 * @param file   file to be proven
	 * @param solver solver to proof the file with
	 */
	public SolverService(File file, Solver solver) {
		this.file = file;
		this.solver = solver;
	}

	@Override
	protected Task<SolverAnswer> createTask() {
		return this.solver.getTask(this.file);
	}

}
