/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import java.util.List;

/**
 * A factory interface to create a list of {@link Solver} from a given configuration file or directory.
 *
 * @author Tim Junginger
 */
public interface SolverProvider {

	/**
	 * Creates a list of {@link Solver} from the given configuration file or directory.
	 * The returned list may be empty or contain only one element.
	 *
	 * @param config a file or directory containing configuration information
	 * @return a list of solvers
	 */
	List<Solver> getSolvers(String config);
}
