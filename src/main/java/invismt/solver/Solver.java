/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import javafx.concurrent.Task;

import java.io.File;
import java.util.List;


/**
 * This interface represents a configuration of a SMT-Solver
 * which works on a file in the SMT-LiB Standard.
 * This interface is designed to provide an executable {@link javafx.concurrent.Task}
 * to allow executing in a different Thread to free main thread resources.
 *
 * @author Tim Junginger
 */
public interface Solver {

	/**
	 * Returns the name of this Solver.
	 * Primarily used to identify different solvers, e.g. for a user.
	 *
	 * @return the name of the solver
	 */
	String getName();

	/**
	 * Creates and returns a new {@link javafx.concurrent.Task} which can then be used inside a {@link SolverService}.
	 * This Task should be designed to be executed inside a different thread than the main application,
	 * as such a process might take a long time.
	 * Therefore this Task should not try to access any outside information,
	 * but rather exclusively work on the given file.
	 * If additional parameters are required consider adding them in the constructor
	 * of an implementation of this interface.
	 *
	 * @param file file which should be solved
	 * @return an executable task that returns the answer of this solver
	 */
	Task<SolverAnswer> getTask(File file);

	/**
	 * Getter for Commands
	 *
	 * @return List of Strings with all the information about, How to start the Solver
	 */
	List<String> getCommand();

}
