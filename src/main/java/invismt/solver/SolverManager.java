/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import invismt.tree.PTElement;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * This class is responsible for managing a collection of {@link Solver}.
 *
 * @author Tim Junginger
 */
public class SolverManager {

	private final List<Solver> solvers;

	/**
	 * Creates a new SolverManager.
	 */
	public SolverManager() {
		this.solvers = new ArrayList<>();
	}

	/**
	 * Initializes this SolverManager.
	 * {@link ServiceLoader} is used to load Instances of the {@link SolverProvider} interface.
	 * The given configuration file is used to load {@link Solver} from each {@link SolverProvider}.
	 *
	 * @param config a file or directory containing configuration information about {@link Solver}
	 */
	public void init(String config) {

		ServiceLoader<SolverProvider> providers = ServiceLoader.load(SolverProvider.class);

		for (SolverProvider provider : providers) {
			List<? extends Solver> solv = provider.getSolvers(config);
			if (solv != null) {
				this.solvers.addAll(solv);
			}
		}

	}

	/**
	 * Uses the given {@link SolverProvider} on the given configuration file
	 * to get a list of {@link Solver} that are then added to the internal collection
	 * of solvers.
	 *
	 * @param provider any implementation of the {@link SolverProvider}
	 * @param config   a file or directory containing configuration files for solvers
	 */
	public void addProvider(SolverProvider provider, String config) {
		if (provider != null) {
			this.solvers.addAll(provider.getSolvers(config));
		}
	}

	/**
	 * Returns a list of all available {@link Solver} that this SolverManager manages.
	 *
	 * @return list of all available solvers
	 */
	public List<Solver> getSolvers() {
		return this.solvers;
	}

	/**
	 * Uses the given {@link Solver} to create an executable {@link javafx.concurrent.Service}
	 * which tries to proof the given {@link PTElement}.
	 * The returned {@link SolverService} is internally completely and correctly configured.
	 * But to get a result from this service one should set event handler to run when this service succeeded or failed.
	 *
	 * @param element the element to be proven
	 * @param solver  the solver configuration with which to do the proving
	 * @return an implementation of the {@link javafx.concurrent.Service} interface
	 */
	public SolverService startSolver(PTElement element, Solver solver) {
		return null;
	}

}
