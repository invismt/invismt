package invismt.solver;

import invismt.solver.z3.Z3Solver;
import invismt.solver.z3.Z3SolverProvider;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.format;

/**
 * This class reads {@link #SOLVER_KEY} from the JAVA property and parse them into SMT solver.
 * Format is {@code <name1>=<cli1>;<name2>=<cli2>;...}
 *
 * @author Alexander Weigl
 * @version 1 (8/3/21)
 */
public class PropertySolverProvider implements SolverProvider {
    private static final Logger LOGGER = Logger.getLogger(PropertySolverProvider.class.getName());

    public static final String SOLVER_KEY = "invismt.solvers";

    @Override
    public List<Solver> getSolvers(String config) {
        var value = System.getProperty(SOLVER_KEY, "");

        if (value.isEmpty() || value.isBlank()) {
            return Collections.emptyList();
        }

        var ret = parseSolvers(value);
        for (Solver s : ret) {
            LOGGER.log(Level.FINER, () -> "Found via invismt.sovlers: " + s.getName() + " with " + s.getCommand());
        }
        return ret;
    }

    List<Solver> parseSolvers(String solverDescription) {
        String[] solvers = solverDescription.split(";");
        List<Solver> ret = new LinkedList<>();
        for (String solver : solvers) {
            if (solver.contains("=")) {
                var v = solver.split("=", 2);
                var name = v[0].trim();
                var args = Z3SolverProvider.getCommand(v[1])
                        .stream().map(String::trim).toList();
                var s = new Z3Solver(name, args);
                ret.add(s);
            }else{
                throw new IllegalArgumentException(
                        format("Solver %s not correctly formatted. Missing »;«.", solver));
            }
        }
        return ret;
    }
}
