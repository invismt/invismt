/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import invismt.tree.PTElement;
import invismt.tree.ProofState;
import invismt.tree.TreeCommand;
import invismt.tree.exception.CommandFailedToExecute;
import invismt.tree.exception.ConflictingStateException;
import invismt.tree.exception.PTElementLocked;

/**
 * SolverSucceededCommand is a Command which is used to give the Tree the ability to create an Undo/Redo History
 *
 * @author Tim Junginger
 */
public class SolverSucceededCommand implements TreeCommand {

	private final SolverAnswer answer;
	private final PTElement element;
	private ProofState newSatisfiability;
	private ProofState previousSatisfiability;

	/**
	 * Constructor for the Command
	 *
	 * @param answer  is the answer which is return by the Solver
	 * @param element is the PLElement on which the Solver was Run
	 */
	public SolverSucceededCommand(SolverAnswer answer, PTElement element) {
		this.answer = answer;
		this.element = element;
	}

	@Override
	public void execute() throws CommandFailedToExecute {
		previousSatisfiability = element.getSatisfiable();
		try {
			element.unlock(this.answer);
		} catch (ConflictingStateException e) {
			throw new CommandFailedToExecute(e.getMessage());
		}
		this.newSatisfiability = element.getSatisfiable();
	}

	@Override
	public void redo() throws CommandFailedToExecute {
		try {
			element.setSatisfiableNoMatterConflicts(newSatisfiability);
		} catch (PTElementLocked e) {
			throw new CommandFailedToExecute(e.getMessage());
		}
	}

	@Override
	public void undo() throws CommandFailedToExecute {
		try {
			element.setSatisfiableNoMatterConflicts(previousSatisfiability);
		} catch (PTElementLocked e) {
			throw new CommandFailedToExecute(e.getMessage());
		}
	}

	@Override
	public String toString() {
		if (this.element.getRule() != null) {
			return "Solver: From " + this.element.getRule().getName() + " is " + this.answer.getType();
		}
		return "Solver: From Unknown is " + this.answer.getType();
	}

}
