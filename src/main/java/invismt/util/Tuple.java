/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.util;

/**
 * Represents a simple Tuple of two values.
 *
 * @param <U>
 * @param <T>
 * @author Alicia
 */
public final class Tuple<U, T> {

	private final U first;
	private final T second;

	/**
	 * is a Tuple
	 *
	 * @param first  is the first element of the Tuple
	 * @param second is the second element of the Tuple
	 */
	public Tuple(U first, T second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Getter
	 *
	 * @return first element of the Tuple
	 */
	public U getFirst() {
		return first;
	}

	/**
	 * Getter
	 *
	 * @return second element of the Tuple
	 */
	public T getSecond() {
		return second;
	}

}
