/*
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 */
package invismt.util;

import invismt.formatting.FormattedExpression;
import invismt.formatting.SMTLIBFormatter;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Utility class for printing a {@link Script} to {@link String} or {@link File}.
 *
 * @author Jonathan Hunz, Tim Junginger
 */
public class ScriptPrinter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ScriptPrinter.class);
	private final SMTLIBFormatter formatter;

	/**
	 * Creates a new ScriptPrinter
	 */
	public ScriptPrinter() {
		formatter = new SMTLIBFormatter();
	}

	/**
	 * Creates a temp file containing the commands from the given script
	 *
	 * @param script is a List of {@link Expression} which makeup an SMT-File
	 * @return a temp file
	 */
	public static File createTempSolverFile(Script script) {
		File file;
		// create temp file
		try {
			file = File.createTempFile("proof_tree_element", ".smt2");
			file.deleteOnExit();
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR, "Could not create file for solver.");
			alert.show();
			LOGGER.warn(e.getMessage(), e);
			return null;
		}
		ScriptPrinter printer = new ScriptPrinter();
		String content = printer.print(script);
		try {
			Files.writeString(Paths.get(file.getAbsolutePath()), content);
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR, "Could not write to file for solver.");
			alert.show();
			LOGGER.warn(e.getMessage(), e);
			return null;
		}
		return file;
	}

	/**
	 * Prints the given {@link Script} to {@link String}.
	 *
	 * @param script is the Script to Print
	 * @return String representing the script in SMT-LIB-Standard
	 */
	public String print(Script script) {
		StringBuilder stringBuilder = new StringBuilder();

		for (Expression expression : script.getTopLevelExpressions()) {
			FormattedExpression formattedExpression = formatter.print(expression);
			stringBuilder.append(formattedExpression.toString());
			stringBuilder.append("\n");
		}

		return stringBuilder.toString();
	}
}
