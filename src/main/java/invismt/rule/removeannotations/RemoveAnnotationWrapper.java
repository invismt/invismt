/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.removeannotations;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.AnnotationTerm;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;

/**
 * {@link RuleWrapper} for the {@link RemoveAnnotationRule}.
 *
 * @author Alicia
 */
public class RemoveAnnotationWrapper implements RuleWrapper {

	@Override
	public String getName() {
		return RemoveAnnotationRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException {
		if (!mayApply(script, expr)) {
			throw new IllegalArgumentException();
		}
		AnnotationTerm term = (AnnotationTerm) expr;
		return new RemoveAnnotationRule(script, term);
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<AnnotationTerm> termCheck = new CheckConstruct<>() {
			@Override
			public AnnotationTerm visit(AnnotationTerm term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}
				return term;
			}
		};
		if (expr.accept(termCheck) == null) {
			return false;
		}
		Expression top = script.getTopLevel(expr);
		SyntaxCommand assertion = top.accept(new CheckConstruct<Assert>() {
			@Override
			public SyntaxCommand.Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		return assertion != null;
	}

}
