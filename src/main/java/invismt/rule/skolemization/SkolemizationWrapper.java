/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.skolemization;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.SortedVariable;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;

/**
 * {@link RuleWrapper} for the {@link SkolemizationRule}.
 *
 * @author Tim Junginger
 */
public class SkolemizationWrapper implements RuleWrapper {

	private static final String DIALOG_HEADER_TEXT = "Skolemization";
	private static final String DIALOG_1_CONTENT_TEXT = "Select which variable to skolemize:";
	private static final String DIALOG_2_CONTENT_TEXT = "Enter new constant name:";

	@Override
	public String getName() {
		return SkolemizationRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException {

		// check for exists term
		ExistsTerm term = expr.accept(new CheckConstruct<>() {
			@Override
			public ExistsTerm visit(ExistsTerm term) {
				return term;
			}
		});
		if (term == null) {
			throw new IllegalArgumentException();
		}
		// create first choice dialog to select variable to skolemize
		ChoiceDialog<SortedVariable> dialog1 = new ChoiceDialog<>(term.getVariables().get(0), term.getVariables());
		dialog1.setContentText(DIALOG_1_CONTENT_TEXT);
		dialog1.setHeaderText(DIALOG_HEADER_TEXT);
		Optional<SortedVariable> sortedVar = dialog1.showAndWait();
		if (sortedVar.isEmpty()) {
			throw new MissingUserInputException();
		}
		// create second dialog to name new constant
		TextInputDialog dialog2 = new TextInputDialog("skolem_" + sortedVar.get().getVariableName().getValue());
		dialog2.setContentText(DIALOG_2_CONTENT_TEXT);
		dialog2.setHeaderText(DIALOG_HEADER_TEXT);
		Optional<String> con = dialog2.showAndWait();
		if (con.isEmpty()) {
			throw new MissingUserInputException();
		}
		Token.SymbolToken symbol = new Token.SymbolToken(con.get());
		// create rule
		return new SkolemizationRule(script, term, sortedVar.get(), symbol);
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<QuantifiedTerm.ExistsTerm> termCheck = new CheckConstruct<>() {
			@Override
			public QuantifiedTerm.ExistsTerm visit(QuantifiedTerm.ExistsTerm term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}
				if (!script.getTopLevel(term).equals(new SyntaxCommand.Assert(term))) {
					return null;
				}
				return term;
			}
		};
		return expr.accept(termCheck) != null;
	}

}
