/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.term.Term;

/**
 * @author Alicia
 */
public interface TermReplacer {

	/**
	 * @param term the term which is to be replaced
	 * @return true if the term can be replaced, false if not
	 */
	boolean applicable(Term term);

	/**
	 * Returns the given term itself if {@link #applicable(Term)} is false.
	 *
	 * @param term the term to be replaced
	 * @return the replacement term of the term to be replaced
	 */
	Term substitute(Term term);
}
