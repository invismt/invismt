/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.splitor;

import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.Rule;

import java.util.List;

/**
 * @author Tim Jungigner
 */
public class SplitOrRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "SplitOr";
	private static final boolean CONFLUENT = true;
	private static final String NOTORTERM = " is not an or-term.";

	private final Term splitTerm;

	/**
	 * Constructor.
	 *
	 * @param script    the {@link Script} on which this rule is supposed to be applied on.
	 * @param orTerm    the {@link IdentifierTerm} that represents an or-operation.
	 * @param splitTerm the {@link Term} that should be split.
	 */
	public SplitOrRule(Script script, IdentifierTerm orTerm, Term splitTerm) {
		super(RULE_NAME, CONFLUENT, script, orTerm);
		this.splitTerm = splitTerm;
		checkParams();
		generateNewScripts();
	}

	private void checkParams() {
		IdentifierTerm term = (IdentifierTerm) expression;

		// Check whether term is top level in an assertion
		if (!script.getTopLevel(term).equals(new SyntaxCommand.Assert(term))) {
			throw CannotApplyRuleException.notTopLevelTerm(this, term);
		}

		// Check whether identified terms contain splitTerm
		if (!term.getIdentifiedTerms().contains(splitTerm)) {
			throw CannotApplyRuleException.notContained(this, splitTerm, term);
		}

		// Check whether term has at least 2 identified terms
		if (term.getIdentifiedTerms().size() < 2) {
			throw CannotApplyRuleException.tooFewIdentifiedTerms(this, term);
		}

		// Check whether term is an or-term
		if (!term.getIdentifier().equals(ExpressionFactory.OR)) {
			throw new CannotApplyRuleException(term + NOTORTERM);
		}
	}

	private void generateNewScripts() {
		IdentifierTerm term = (IdentifierTerm) expression;
		List<Term> remainder = term.getIdentifiedTerms();
		remainder.remove(splitTerm);
		Term remainderTerm;
		if (remainder.size() > 1) {
			remainderTerm = new IdentifierTerm(term.getIdentifier(), remainder);
		} else {
			remainderTerm = remainder.get(0);
		}
		createdScripts.add(script
				.addTopLevelListInstead(script.getTopLevel(term), List.of(new SyntaxCommand.Assert(remainderTerm))));
		createdScripts.add(script
				.addTopLevelListInstead(script.getTopLevel(term), List.of(new SyntaxCommand.Assert(splitTerm))));
	}

}
