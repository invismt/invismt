/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.splitor;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.Term;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;
import javafx.scene.control.ChoiceDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * {@link RuleWrapper} for the {@link SplitOrRule}.
 *
 * @author Tim Junginger
 */
public class SplitOrWrapper implements RuleWrapper {

	private static final String CHOOSESPLITTERM = "Choose the term to be split from the or-term: ";
	private static final String TERMCHOICE = "Term Choice";
	private static final String NOCHOSENSPLITTERM = "No term to split off has been chosen.";
	private static final QualIdentifier OR = new QualIdentifier(
			new Identifier(new Token.SymbolToken("or"), new ArrayList<>()), Optional.empty());

	@Override
	public String getName() {
		return SplitOrRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException {
		if (!mayApply(script, expr)) {
			throw new IllegalArgumentException();
		}
		IdentifierTerm term = (IdentifierTerm) expr;
		List<Term> orTerms = term.getIdentifiedTerms();
		if (orTerms.size() == 2) {
			return new SplitOrRule(script, term, orTerms.get(1));
		}
		ChoiceDialog<Term> splitTermChoice = new ChoiceDialog<>(orTerms.get(0), orTerms);
		splitTermChoice.setContentText(CHOOSESPLITTERM);
		splitTermChoice.setHeaderText(TERMCHOICE);
		final Optional<Term> splitTerm = splitTermChoice.showAndWait();
		if (splitTerm.isEmpty()) {
			throw new MissingUserInputException(NOCHOSENSPLITTERM);
		}
		return new SplitOrRule(script, term, splitTerm.get());
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<IdentifierTerm> termCheck = new CheckConstruct<>() {
			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				if (!script.existsIdenticallySomewhere(term)) {
					return null;
				}
				if (!script.getTopLevel(term).equals(new SyntaxCommand.Assert(term))) {
					return null;
				}
				if (term.getIdentifiedTerms().size() < 2) {
					return null;
				}
				if (!term.getIdentifier().equals(OR)) {
					return null;
				}
				return term;
			}
		};
		return expr.accept(termCheck) != null;
	}
}
