/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.Script;

/**
 * Dummy implementation for the creation of a new {@link invismt.tree.ProofObligation}.
 *
 * @author Tim Junginger
 */
public class CreationRule extends Rule {

	private static final String RULE_NAME = "Creation";

	/**
	 * @param script     the script of the new proof obligation
	 * @param expression any expression in the script of the new proof obligation
	 */
	public CreationRule(Script script, Expression expression) {
		super(RULE_NAME, false, script, expression);
		createdScripts.add(script);
	}

}
