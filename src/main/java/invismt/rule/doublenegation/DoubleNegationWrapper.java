/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.doublenegation;

import invismt.exception.MissingUserInputException;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.IdentifierTerm;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;

/**
 * {@link RuleWrapper} for the {@link DoubleNegationRule}.
 *
 * @author Alicia
 */
public class DoubleNegationWrapper implements RuleWrapper {

	@Override
	public String getName() {
		return DoubleNegationRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) throws IllegalArgumentException, MissingUserInputException {
		if (!mayApply(script, expr)) {
			throw new IllegalArgumentException();
		}
		IdentifierTerm term = (IdentifierTerm) expr;
		return new DoubleNegationRule(script, term);
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		CheckConstruct<IdentifierTerm> termCheck = new CheckConstruct<>() {
			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				IdentifierTerm innerTerm = null;
				if (term.getIdentifier().equals(ExpressionFactory.NOT) && term.getIdentifiedTerms().size() == 1) {
					innerTerm = term.getIdentifiedTerms().get(0).accept(new CheckConstruct<>() {
						@Override
						public IdentifierTerm visit(IdentifierTerm term) {
							if (term.getIdentifier().equals(ExpressionFactory.NOT)
									&& term.getIdentifiedTerms().size() == 1) {
								return term;
							}
							return null;
						}
					});
				}
				if (innerTerm == null) {
					return null;
				}
				return term;
			}
		};
		if (expr.accept(termCheck) == null) {
			return false;
		}
		Expression top = script.getTopLevel(expr);
		SyntaxCommand assertion = top.accept(new CheckConstruct<Assert>() {
			@Override
			public SyntaxCommand.Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		return assertion != null;
	}

}
