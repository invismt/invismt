/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.splitand;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.Term;
import invismt.rule.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Tim Junginger
 */
public class SplitAndRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "SplitAnd";
	private static final boolean CONFLUENT = true;
	private static final String NOTANDTERM = " is not an and-term.";

	private static final QualIdentifier AND = new QualIdentifier(
			new Identifier(new SymbolToken("and"), new ArrayList<>()), Optional.empty());

	private final Term splitTerm;

	/**
	 * Constructor.
	 *
	 * @param script    the {@link Script} on which this rule is supposed to be applied on.
	 * @param andTerm   the {@link IdentifierTerm} that represents an and-operation.
	 * @param splitTerm the {@link Term} that should be split.
	 */
	public SplitAndRule(Script script, IdentifierTerm andTerm, Term splitTerm) {
		super(RULE_NAME, CONFLUENT, script, andTerm);
		this.splitTerm = splitTerm;
		checkParams();
		generateNewStates();
	}

	private void checkParams() {
		IdentifierTerm andTerm = (IdentifierTerm) expression;

		// Check whether term is an and-term
		if (!andTerm.getIdentifier().equals(AND)) {
			throw new CannotApplyRuleException(andTerm + NOTANDTERM);
		}

		// Check whether term has at least 2 identified terms
		if (andTerm.getIdentifiedTerms().size() < 2) {
			throw CannotApplyRuleException.tooFewIdentifiedTerms(this, andTerm);
		}

		// Check whether identified terms contain splitTerm
		if (!andTerm.getIdentifiedTerms().contains(splitTerm)) {
			throw CannotApplyRuleException.notContained(this, splitTerm, andTerm);
		}

		// Check whether term is top level in an assertion
		if (!script.getTopLevel(andTerm).equals(new SyntaxCommand.Assert(andTerm))) {
			throw CannotApplyRuleException.notTopLevelTerm(this, andTerm);
		}
	}

	private void generateNewStates() {
		IdentifierTerm andTerm = (IdentifierTerm) expression;
		List<Term> remainder = andTerm.getIdentifiedTerms();
		remainder.remove(splitTerm);
		Term remainderTerm;
		if (remainder.size() > 1) {
			remainderTerm = new IdentifierTerm(andTerm.getIdentifier(), remainder);
		} else {
			remainderTerm = remainder.get(0);
		}
		TermReplacer s = new NoBoundVariableReplacer(andTerm, splitTerm,
				new ExpressionComparator.IdenticalComparator());
		Expression splitTop = new Assert(s.substitute(andTerm));
		TermReplacer r = new NoBoundVariableReplacer(andTerm, remainderTerm,
				new ExpressionComparator.IdenticalComparator());
		Expression remainderTop = new Assert(r.substitute(andTerm));
		createdScripts.add(script.addTopLevelListInstead(andTerm, List.of(splitTop, remainderTop)));
	}

}
