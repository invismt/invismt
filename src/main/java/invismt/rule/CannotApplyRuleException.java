/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;

import java.util.ResourceBundle;

/**
 * Exceptions thrown to indicate that a {@link Rule}} could not be applied.
 *
 * @author Alicia, Tim Jungigner
 */
public class CannotApplyRuleException extends IllegalArgumentException {

	private static final ResourceBundle ERROR_MESSAGES = ResourceBundle.getBundle("i18n.ErrorMessages");

	/**
	 * Constructs a CannotApplyRuleException with the given message.
	 *
	 * @param message the error message.
	 */
	public CannotApplyRuleException(String message) {
		super(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because the {@link Expression} it
	 * was supposed to be applied on is not top level.
	 *
	 * @param rule the {@link Rule} that could not be applied.
	 * @param term the term the {@link Rule} was supposed to be applied to.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException notTopLevelTerm(Rule rule, Term term) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.notTopLevel");
		String message = String.format(messageFormat, rule.getName(), term.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because an enclosing
	 * {@link Expression} does not contain the other given {@link Expression}.
	 *
	 * @param rule      the {@link Rule} that could not be applied.
	 * @param enclosing the enclosing Expression.
	 * @param contained the Expression which was not contained in the enclosing Expression.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException notContained(Rule rule, Expression enclosing, Expression contained) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.notContained");
		String message = String.format(messageFormat, rule.getName(), contained.toString(), enclosing.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because an {@link Expression} has
	 * too few {@link IdentifierTerm identified terms}.
	 *
	 * @param rule the {@link Rule} that could not be applied.
	 * @param term the term the {@link Rule} was supposed to be applied to.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException tooFewIdentifiedTerms(Rule rule, IdentifierTerm term) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.toFewIdentifiedTerms");
		String message = String.format(messageFormat, rule.getName(), term.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because a {@link SortedVariable} is
	 * of wrong {@link Sort}.
	 *
	 * @param rule         the {@link Rule} that could not be applied.
	 * @param variable     the {@link SortedVariable} which is not of the expected {@link Sort}.
	 * @param searchedSort the expected {@link Sort}.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException wrongSort(Rule rule, SortedVariable variable, Sort searchedSort) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.wrongSort");
		String message = String.format(messageFormat, rule.getName(), variable.toString(), searchedSort.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because a {@link Expression} is not
	 * contained in a {@link Script}.
	 *
	 * @param rule       the {@link Rule}} that could not be applied.
	 * @param expression the Expression which was not found in the script.
	 * @param script     the script on which the {@link Rule} was supposed to be applied on.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException notInScript(Rule rule, Expression expression, Script script) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.notInScript");
		String message = String.format(messageFormat, rule.getName(), expression.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because a {@link Term} could not be
	 * replaced by another {@link Term}.
	 *
	 * @param term    the {@link Rule} that could not be applied.
	 * @param oldTerm the term that was supposed to be replaced.
	 * @param newTerm the term that was supposed to replace to old term.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException cannotReplace(Term term, Term oldTerm, Term newTerm) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.cannotReplace");
		String message = String.format(messageFormat, oldTerm.toString(), newTerm.toString(), term.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because a {@link Script} already
	 * contains a {@link Token.SymbolToken}.
	 *
	 * @param script    the script on which a {@link Rule}} was supposed to be applied on.
	 * @param newSymbol the {@link Token.SymbolToken} which was already contained in the script.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException scriptAlreadyContains(Script script, Token.SymbolToken newSymbol) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.scriptAlreadyContains");
		String message = String.format(messageFormat, newSymbol.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because a {@link Term} was not
	 * contained in an assertion.
	 *
	 * @param rule the {@link Rule}} that could not be applied.
	 * @param term the term which was not contained in an assertion.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException notInAssertion(Rule rule, Term term) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.notInAssertion");
		String message = String.format(messageFormat, rule.getName(), term.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating a {@link Rule} could not be applied because a {@link Term} contains
	 * annotations.
	 *
	 * @param rule the {@link Rule}} that could not be applied.
	 * @param term the term which contained an annotation.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException annotationTerm(Rule rule, Term term) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.containsAnnotation");
		String message = String.format(messageFormat, rule.getName(), term.toString());
		return new CannotApplyRuleException(message);
	}

	/**
	 * CannotApplyRuleException indicating that a {@link Rule} could not be applied because it would produce a script
	 * with less than one assertion.
	 *
	 * @param rule the {@link Rule}} that could not be applied.
	 * @return a CannotApplyRuleException with a respective error message.
	 */
	public static CannotApplyRuleException moreThanOneAssertion(Rule rule) {
		String messageFormat = ERROR_MESSAGES.getString("cannotApplyRule.scriptNeedMoreThanOneAssertion");
		String message = String.format(messageFormat, rule.getName());
		return new CannotApplyRuleException(message);
	}
}
