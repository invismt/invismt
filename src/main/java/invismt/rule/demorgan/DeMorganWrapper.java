/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.demorgan;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;
import invismt.rule.CheckConstruct;
import invismt.rule.Rule;
import invismt.rule.RuleWrapper;

import java.util.List;

/**
 * {@link RuleWrapper} for the {@link DeMorganRule}.
 *
 * @author Alicia
 */
public class DeMorganWrapper implements RuleWrapper {

	private static final List<QualIdentifier> ALLOWED_IDENTIFIERS = List
			.of(ExpressionFactory.AND, ExpressionFactory.OR);

	@Override
	public String getName() {
		return DeMorganRule.RULE_NAME;
	}

	@Override
	public Rule createRule(Script script, Expression expr) {
		if (!mayApply(script, expr)) {
			throw new IllegalArgumentException();
		}
		IdentifierTerm term = expr.accept(new CheckConstruct<>() {
			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				return term;
			}
		});
		if (term == null) {
			return new DeMorganRule(script, (QuantifiedTerm) expr);
		}
		return new DeMorganRule(script, term);
	}

	@Override
	public boolean mayApply(Script script, Expression expr) {
		if (!script.existsIdenticallySomewhere(expr)) {
			return false;
		}
		Assert topLevel = script.getTopLevel(expr).accept(new CheckConstruct<>() {
			@Override
			public Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		if (topLevel == null) {
			return false;
		}
		CheckConstruct<QuantifiedTerm> quantifiedCheck = new CheckConstruct<>() {
			@Override
			public ForAllTerm visit(ForAllTerm term) {
				return term;
			}

			@Override
			public ExistsTerm visit(ExistsTerm term) {
				return term;
			}
		};
		CheckConstruct<IdentifierTerm> identifierCheck = new CheckConstruct<>() {
			@Override
			public IdentifierTerm visit(IdentifierTerm term) {
				IdentifierTerm innerTerm = term;
				if (term.getIdentifier().equals(ExpressionFactory.NOT) && term.getIdentifiedTerms().size() == 1) {
					innerTerm = term.getIdentifiedTerms().get(0).accept(new CheckConstruct<>() {
						@Override
						public IdentifierTerm visit(IdentifierTerm term) {
							return term;
						}
					});
				}

				if (innerTerm == null) {
					if (term.getIdentifiedTerms().get(0).accept(quantifiedCheck) != null) {
						return term;
					}
					return null;
				}

				if (!ALLOWED_IDENTIFIERS.contains(innerTerm.getIdentifier())) {
					return null;
				}
				return term;
			}
		};
		return expr.accept(identifierCheck) != null || expr.accept(quantifiedCheck) != null;
	}

}
