/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.Script;

import java.util.ArrayList;
import java.util.List;

/**
 * An inference rule that can be applied to an {@link Expression} in a {@link Script}.
 * The expression must be contained identically in the script.
 * <p>
 * An inference rule logically transforms the SMT problem represented by the script
 * into one or more new SMT problems.
 * Every inference rule is expected to be correct in the sense that it is logically sound.
 *
 * @author Alicia
 */
public abstract class Rule {

	protected final Script script;
	protected final Expression expression;
	protected final String name;
	protected final List<Script> createdScripts;
	private final boolean isConfluent;

	/**
	 * Every rule object is expected to be created iff it is applicable to the given arguments.
	 * If it is not applicable, a CannotApplyRuleException is thrown.
	 *
	 * @param name        the name of the rule at hand
	 * @param isConfluent whether or not the rule at hand is confluent
	 * @param script      {@link Script} the rule at hand is applied to
	 * @param expression  {@link Expression} the rule at hand is applied to, must be contained in the script
	 * @throws CannotApplyRuleException if the rule cannot be applied to the given arguments
	 */
	protected Rule(String name, boolean isConfluent, Script script, Expression expression) {
		this.script = script;
		this.expression = expression;
		this.name = name;
		this.isConfluent = isConfluent;
		createdScripts = new ArrayList<>();
		if (!script.existsIdenticallySomewhere(expression)) {
			throw CannotApplyRuleException.notInScript(this, expression, script);
		}
	}

	/**
	 * @return the name of the rule at hand
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return whether or not the rule at hand is confluent
	 */
	public final boolean isConfluent() {
		return isConfluent;
	}

	/**
	 * Returns the new scripts created by the rule at hand as soon as the rule object
	 * has been created.
	 *
	 * @return one or more proof states created by applying this rule on the given state
	 */
	public List<Script> getCreatedScripts() {
		return new ArrayList<>(createdScripts);
	}

}
