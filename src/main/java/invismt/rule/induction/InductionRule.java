/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.induction;

import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.*;
import invismt.rule.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * An inference rule that changes an {@link QuantifiedTerm.ForAllTerm} of the type "forall x in Z: phi(x)"
 * to a term of the type "phi(0) and forall x in Z: ( phi(x) -> ( phi(x+1) and phi(x-1) ) )".
 * Can only be applied to top level formulas directly after a {@link SyntaxCommand.Assert} command
 * in a {@link Script}.
 *
 * @author Alicia
 */
public class InductionRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "Induction";
	private static final String AND = "and";
	private static final String IMPLIES = "=>";
	private static final String PLUS = "+";
	private static final String MINUS = "-";
	private static final String ONE = "1";
	private static final String ZERO = "0";
	private static final boolean CONFLUENT = true;
	private final SortedVariable variable;
	private final Term variableTerm;
	private Assert topLevel;

	/**
	 * Constructor.
	 *
	 * @param script   the {@link Script} on which this rule is supposed to be applied on.
	 * @param term     the {@link QuantifiedTerm.ForAllTerm} that contains the variable that should be induced over.
	 * @param variable the {@link SortedVariable} that should be induced over.
	 */
	public InductionRule(Script script, QuantifiedTerm.ForAllTerm term, SortedVariable variable) {
		super(RULE_NAME, CONFLUENT, script, term);
		this.variable = variable;
		variableTerm = new IdentifierTerm(
				new QualIdentifier(new Identifier(variable.getVariableName(), new ArrayList<>()), Optional.empty()),
				new ArrayList<>());
		checkParams();
		generateScripts();
	}

	private void checkParams() {
		QuantifiedTerm.ForAllTerm term = (QuantifiedTerm.ForAllTerm) expression;

		// Check whether the term is contained somewhere in an assertion
		topLevel = script.getTopLevel(term).accept(new CheckConstruct<>() {
			@Override
			public Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		if (topLevel == null) {
			throw CannotApplyRuleException.notInAssertion(this, term);
		}

		// Check whether the chosen induction variable is contained in the term's sorted variables
		if (!term.getVariables().contains(variable)) {
			throw CannotApplyRuleException.notContained(this, variable, term);
		}

		// Check whether the chosen induction variable is of the sort Integer
		if (!variable.getVariableType().equals(ExpressionFactory.INT)) {
			throw CannotApplyRuleException.wrongSort(this, variable, ExpressionFactory.INT);
		}
	}

	/*
	Create the script's status after the induction rule has been performed:
	Add (assert phi(0)) and (assert (forall (x Int) (=> phi(x) (and phi(x+1) phi(x-1))))).
	 */
	private void generateScripts() {
		QuantifiedTerm.ForAllTerm term = (QuantifiedTerm.ForAllTerm) expression;
		List<SortedVariable> variables = new ArrayList<>(term.getVariables());
		variables.remove(variable);

		// Term for "phi(0) and (forall x in Z: phi(x) -> (phi(x+1) and phi(x-1)))"
		Term newTerm = generateIdentifierTerm(new SymbolToken(AND),
				List.of(getZeroTerm(term, variables), getInductionStepTerm(term)));
		BoundVariableReplacer replacer = new BoundVariableReplacer(term, newTerm,
				new ExpressionComparator.IdenticalComparator());
		Term replacedTerm = replacer.substitute(topLevel.getTerm());
		Assert newTop = new Assert(replacedTerm);
		createdScripts.add(script.addTopLevelListInstead(topLevel, List.of(newTop)));
	}

	/*
	Helper method so the {@link #generateNewScripts()} method is not too long.
	Creates the Term representing (forall (x Int) (=> phi(x) (and phi(x+1) phi(x-1))))
	in the induction rule.
	 */
	private Term getInductionStepTerm(QuantifiedTerm.ForAllTerm term) {
		// ConstantTerm representing the number 1
		Term oneTerm = new ConstantTerm(new SpecConstant.NumeralConstant(new Token.NumeralToken(ONE)));
		List<Term> plusMinusOneList = List.of(variableTerm, oneTerm);

		// Terms for x+1, x-1
		Term variablePlusOne = generateIdentifierTerm(new Token.SymbolToken(PLUS), plusMinusOneList);
		Term variableMinusOne = generateIdentifierTerm(new Token.SymbolToken(MINUS), plusMinusOneList);

		// Terms for phi(x+1), phi(x-1)
		TermReplacer plusOneReplacer = new NoBoundVariableReplacer(variableTerm, variablePlusOne,
				new ExpressionComparator.EqualComparator());
		TermReplacer minusOneReplacer = new NoBoundVariableReplacer(variableTerm, variableMinusOne,
				new ExpressionComparator.EqualComparator());
		if (!plusOneReplacer.applicable(term.getTerm())) {
			throw CannotApplyRuleException.cannotReplace(term.getTerm(), variableTerm, variablePlusOne);
		}
		if (!plusOneReplacer.applicable(term.getTerm())) {
			throw CannotApplyRuleException.cannotReplace(term.getTerm(), variableTerm, variableMinusOne);
		}
		Term plusOneTerm = plusOneReplacer.substitute(term.getTerm());
		Term minusOneTerm = minusOneReplacer.substitute(term.getTerm());
		List<Term> termList = List.of(plusOneTerm, minusOneTerm);

		// Term for "phi(x+1) and phi(x-1)"
		Term andTerm = generateIdentifierTerm(new Token.SymbolToken(AND), termList);
		List<Term> implyList = List.of(term.getTerm(), andTerm);

		// Term for "phi(x) -> (phi(x+1) and phi(x-1))"
		Term implication = generateIdentifierTerm(new Token.SymbolToken(IMPLIES), implyList);

		// Term for "forall x in Z: phi(x) -> (phi(x+1) and phi(x-1))"
		return new QuantifiedTerm.ForAllTerm(term.getVariables(), implication);
	}

	/*
	Helper method so the {@link #generateNewScripts()} method is not too long.
	Creates the Term representing phi(0) in the induction rule.
	 */
	private Term getZeroTerm(QuantifiedTerm.ForAllTerm term, List<SortedVariable> variables) {
		ConstantTerm zero = new ConstantTerm(new SpecConstant.NumeralConstant(new Token.NumeralToken(ZERO)));
		NoBoundVariableReplacer replaceByZero = new NoBoundVariableReplacer(variableTerm, zero,
				new ExpressionComparator.EqualComparator());

		if (!replaceByZero.applicable(term.getTerm())) {
			throw CannotApplyRuleException.cannotReplace(term.getTerm(), variableTerm, zero);
		}

		Term zeroTerm = replaceByZero.substitute(term.getTerm());
		if (variables.isEmpty()) {
			return zeroTerm;
		} else {
			return new QuantifiedTerm.ForAllTerm(variables, zeroTerm);
		}
	}

	private IdentifierTerm generateIdentifierTerm(Token.SymbolToken symbol, List<Term> terms) {
		return new IdentifierTerm(new QualIdentifier(new Identifier(symbol, new ArrayList<>()), Optional.empty()),
				terms);
	}

}
