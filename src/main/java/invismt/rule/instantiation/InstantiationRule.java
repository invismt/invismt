/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.instantiation;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.rule.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * An inference rule that changes a formula of the type "forall x: phi(x)" to "(forall x: phi(x)) and phi(t)" where t is
 * any term. The rule can only be applied to a {@link Term} directly after a {@link SyntaxCommand.Assert} command in a
 * {@link Script}. It creates one new script with the additional assertion "(assert phi(t))".
 * <p>
 * The rule does not check the script for the existence of declarations and definitions of variables and function names
 * used in t. The rule also does not check t for well-sortedness according to the SMT-LIB standard 2.6 or for having the
 * same type as x.
 *
 * @author Alicia, Tim Junginger
 */
public class InstantiationRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "Instantiation";
	private static final String AND = "and";
	private static final boolean CONFLUENT = true;

	private final SortedVariable sortedVar;
	private final Term instance;
	Term varTerm;
	private Assert topLevel;

	/**
	 * Constructor.
	 *
	 * @param script    the {@link Script} on which this rule is supposed to be applied on.
	 * @param forall    the {@link ForAllTerm} that contains the variable that is supposed to be instantiated.
	 * @param sortedVar the {@link SortedVariable} that should be instantiated.
	 * @param instance  an instance of the variable.
	 */
	public InstantiationRule(Script script, ForAllTerm forall, SortedVariable sortedVar, Term instance) {
		super(RULE_NAME, CONFLUENT, script, forall);
		this.sortedVar = sortedVar;
		this.instance = instance;
		checkParams();
		varTerm = new IdentifierTerm(
				new QualIdentifier(new Identifier(sortedVar.getVariableName(), new ArrayList<>()), Optional.empty()),
				new ArrayList<>());
		generateNewStates();
	}

	private void checkParams() {
		ForAllTerm term = (ForAllTerm) expression;

		// Check whether term is contained in a top level assertion
		topLevel = script.getTopLevel(term).accept(new CheckConstruct<>() {
			@Override
			public Assert visit(SyntaxCommand.Assert assertion) {
				return assertion;
			}
		});
		if (topLevel == null) {
			throw CannotApplyRuleException.notInAssertion(this, term);
		}

		// Check whether the variable to be instantiated is contained in the sorted variables of the term
		if (!term.getVariables().contains(sortedVar)) {
			throw CannotApplyRuleException.notContained(this, sortedVar, term);
		}

	}

	private void generateNewStates() {
		List<SortedVariable> newVariables;
		ForAllTerm forall = (ForAllTerm) expression;

		// Remove the instantiated variable from the term's sorted variables
		newVariables = new ArrayList<>();
		for (SortedVariable sortedVariable : forall.getVariables()) {
			newVariables.add(sortedVariable.deepCopy());
		}
		newVariables.remove(sortedVar);

		// Replace each free occurence of the variable to be instantiated by the instance in the term
		TermReplacer replacer = new NoBoundVariableReplacer(varTerm, instance,
				new ExpressionComparator.EqualComparator());
		Term instantiated = replacer.substitute(forall.getTerm());

		// If other variables were bound in the forall term, keep them
		Term newTerm = instantiated;
		if (!newVariables.isEmpty()) {
			newTerm = new ForAllTerm(newVariables, instantiated);
		}

		// Replace the forall term that was instantiated by "instanceTerm and forallTerm"
		Term andTerm = new IdentifierTerm(
				new QualIdentifier(new Identifier(new SymbolToken(AND), new ArrayList<>()), Optional.empty()),
				List.of(newTerm, forall));
		TermReplacer boundReplacer = new BoundVariableReplacer(forall, andTerm,
				new ExpressionComparator.IdenticalComparator());
		Term replacedTerm = boundReplacer.substitute(topLevel.getTerm());
		Expression newTop = new Assert(replacedTerm);
		createdScripts.add(script.addTopLevelListInstead(topLevel, List.of(newTop)));
	}

}
