/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.cut;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.Rule;

import java.util.List;

/**
 * An inference rule that changes a formula A to "(A and B) or (A and (not B))".
 * The rule can only be applied to a {@link Term} directly after a
 * {@link SyntaxCommand.Assert} command in a {@link Script}.
 * It creates two new scripts, one with the additional assertion (assert B) and
 * one with the additional assertion "(assert (not B))" where B is any formula.
 * <p>
 * The rule does not check the script for the existence of declarations and definitions
 * of variables and function names used in B.
 * The rule also does not check B for well-sortedness according to the
 * SMT-LIB standard 2.6.
 */
public class CutRule extends Rule {

	/**
	 * The name of this rule.
	 */
	public static final String RULE_NAME = "Cut";
	private static final boolean CONFLUENT = true;

	private final Term cutTerm;
	private final Expression top;

	/**
	 * Creates a new cut rule with the script and the term in the script
	 * that the rule is to be applied to as well as the new term to be
	 * added to the created scripts according to the rule:
	 * A is transformed to (A and B) or (A and (not B)).
	 *
	 * @param script     the script to which the rule will be applied
	 * @param commonTerm the term (A) this rule is to be applied to
	 * @param cutTerm    the term (B) that will be added to the script
	 * @throws CannotApplyRuleException if the term A is not top-level in an assertion
	 */
	public CutRule(Script script, Term commonTerm, Term cutTerm) {
		super(RULE_NAME, CONFLUENT, script, commonTerm);
		this.cutTerm = cutTerm;
		top = script.getTopLevel(commonTerm);
		checkParams();
		generateNewScripts();
	}

	/*
	Checks whether the commonTerm is top-level in an assertion.
	 */
	private void checkParams() {
		if (!top.equals(new Assert((Term) expression))) {
			throw CannotApplyRuleException.notTopLevelTerm(this, (Term) expression);
		}
	}

	/*
	Creates two new scripts out of the current one where one has the additional
	assertion (assert (not cutTerm)) and the other one has the additional assertion
	(assert cutTerm).
	 */
	private void generateNewScripts() {
		Term negTerm = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(), List.of(cutTerm));
		Assert posTop = new Assert(cutTerm);
		Assert negTop = new Assert(negTerm);
		createdScripts.add(script.addTopLevelListInstead(top, List.of(top, posTop)));
		createdScripts.add(script.addTopLevelListInstead(top, List.of(top, negTop)));
	}

}
