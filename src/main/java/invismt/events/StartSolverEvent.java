/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.solver.Solver;
import invismt.tree.PTElement;

/**
 * An event signalling that a {@link PTElement PTElement's}
 * {@link invismt.grammar.Script} is to be proven.
 * <p>
 * For example, a StartSolverEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then be handled by any
 * class that has access to the EventBus to start a
 * {@link invismt.solver.Solver} on the PTElement's script.
 * <p>
 * See
 * {@link invismt.controller.MasterController#handleStartSolver(StartSolverEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class StartSolverEvent {

	private final PTElement element;
	private Solver solver;

	/**
	 * Create a new StartSolverEvent by calling its constructor with the
	 * {@link PTElement} which is to be proven.
	 *
	 * @param element PTElement whose {@link invismt.grammar.Script} is to be proven
	 */
	public StartSolverEvent(PTElement element) {
		this.element = element;
		this.solver = null;
	}

	/**
	 * Create a new StartSolverEvent with the {@link PTElement} which is to be
	 * proven and the {@link Solver} to prove it with.
	 *
	 * @param element PTElement whose {@link invismt.grammar.Script} is to be proven
	 * @param solver  Solver to prove the element with
	 */
	public StartSolverEvent(PTElement element, Solver solver) {
		this.element = element;
		this.solver = solver;
	}

	/**
	 * @return PTElement to be proven
	 */
	public PTElement getElement() {
		return element;
	}

	/**
	 * @return Solver to prove with
	 */
	public Solver getSolver() {
		return solver;
	}

	/**
	 * Sets the solver to prove the {@link PTElement} with.
	 *
	 * @param solver is the Solver for PTElement
	 * @throws IllegalStateException when the solver has already been set
	 */
	public void setSolver(Solver solver) {
		if (this.solver == null) {
			this.solver = solver;
		} else {
			throw new IllegalStateException("Solver has already been set.");
		}
	}
}
