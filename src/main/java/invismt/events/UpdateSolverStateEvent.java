/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.tree.PTElement;
import invismt.tree.ProofState;

import static invismt.tree.ProofState.CausedBy;

/**
 * An event signalling a possible change to a proof process on a {@link PTElement}.
 * <p>
 * For example, an UpdateSolverStateElement object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then be handled by any
 * class that has access to the EventBus to signal that a {@link invismt.solver.Solver}
 * has been started on the PTElement.
 * <p>
 * See
 * {@link invismt.controller.MasterController#handleUpdateSolverStateEvent(UpdateSolverStateEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class UpdateSolverStateEvent {

	/**
	 * Whether a solver has been running on the PTElement given to the constructor
	 * when this event was created.
	 */
	private final boolean running;
	/**
	 * Whether any solver process has been disabled (the element has already been proven/disproven)
	 * on the PTElement given to the constructor when this event was created.
	 */
	private final boolean disabled;

	/**
	 * Creates a new UpdateSolverStateEvent that is a snapshot of the
	 * solver state (disabled/running) on the given PTElement.
	 *
	 * @param element is the {@link PTElement} whose solver state is of interest
	 */
	public UpdateSolverStateEvent(PTElement element) {
		//Start the Solver
		this.running = element.isLocked();
		this.disabled = (element.getSatisfiable() != ProofState.UNKNOWN
				                 && element.getSatisfiable().causedBy() == CausedBy.EXTERNAL);
	}

	/**
	 * Returns whether a solver has been running on the PTElement given to the constructor.
	 *
	 * @return true when a solver is running
	 */
	public boolean getRunning() {
		return running;
	}

	/**
	 * @return true when the button should be disabled
	 */
	public boolean getDisabled() {
		return disabled;
	}

}
