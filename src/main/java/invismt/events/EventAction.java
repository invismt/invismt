package invismt.events;

/**
 * This enum represent a type of action that events can take.
 * @author Tim Junginger
 *
 */
public enum EventAction {
	
	SET,
	ADD,
	GET,
	POST;

}
