/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.grammar.Script;
import invismt.tree.PTElement;

import java.io.File;

/**
 * An event signalling that a {@link Script} is to be saved to a {@link File}.
 * <p>
 * For example, a SaveToFileEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then
 * be handled by any class that has access to the EventBus.
 * <p>
 * See {@link invismt.controller.ScriptController#handleSaveToFileEvent(SaveToFileEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class SaveToFileEvent {

	private File file;
	private Script script;

	/**
	 * Creates a new event with the given {@link File} and {@link Script}.
	 *
	 * @param file   is the File to save to
	 * @param script is the Script which will be saved
	 */
	public SaveToFileEvent(File file, Script script) {
		if (file == null) {
			throw new IllegalArgumentException("File can not be null.");
		}
		this.file = file;
		this.script = script;
	}

	/**
	 * Returns the file to which a {@link PTElement} or {@link Script} should be
	 * saved.
	 *
	 * @return file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * Returns the script to be saved to file. If this returns null, then the script
	 * of the currently active {@link PTElement} should be saved.
	 *
	 * @return script
	 */
	public Script getScript() {
		return script;
	}
}
