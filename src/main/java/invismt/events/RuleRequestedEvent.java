/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.grammar.Expression;
import invismt.rule.RuleWrapper;
import invismt.tree.PTElement;

/**
 * An event signalling that a {@link invismt.rule.Rule} represented
 * by a {@link RuleWrapper} is requested to be applied to an
 * {@link Expression} in the {@link invismt.grammar.Script} of a
 * {@link PTElement}.
 * <p>
 * For example, a RuleRequestedEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then
 * be handled by any class that has access to the EventBus
 * to carry out the rule application.
 * <p>
 * See {@link invismt.controller.ProofTreeController#handleRuleRequest(RuleRequestedEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class RuleRequestedEvent {

	private final RuleWrapper wrapper;
	private final PTElement element;
	private final Expression expr;

	/**
	 * Create a new RuleRequestedEvent by calling its constructor with the RuleWrapper
	 * representing the Rule to be applied as well as the Expression and the PTElement
	 * which the Rule is requested to be applied to.
	 *
	 * @param wrapper {@link RuleWrapper} representing the Rule to be applied
	 * @param element {@link PTElement} whose Script the Rule will be applied to
	 * @param expr    {@link Expression} to which the Rule will be applied
	 */
	public RuleRequestedEvent(RuleWrapper wrapper, PTElement element, Expression expr) {
		this.wrapper = wrapper;
		this.element = element;
		this.expr = expr;
	}

	/**
	 * Returns the RuleWrapper that can be used to create the Rule object that will be applied by
	 * giving the PTElement's {@link #element} Script as well as the Expression {@link #expr}
	 * as parameters to its {@link RuleWrapper#createRule(invismt.grammar.Script, Expression)}
	 * method.
	 *
	 * @return {@link RuleWrapper} that represents the Rule to be applied and can be used to create it
	 */
	public RuleWrapper getRuleWrapper() {
		return this.wrapper;
	}

	/**
	 * @return {@link PTElement} on which the rule is requested to be applied
	 */
	public PTElement getPTElement() {
		return this.element;
	}

	/**
	 * @return {@link Expression} that the rule is requested to be applied to
	 */
	public Expression getExpression() {
		return this.expr;
	}

}
