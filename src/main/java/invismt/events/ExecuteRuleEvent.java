/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.rule.RuleApplyCommand;
import invismt.tree.ProofObligation;
import invismt.tree.TreeCommand;

/**
 * An event signalling that an existing {@link RuleApplyCommand}
 * is to be executed on a {@link ProofObligation}.
 * <p>
 * For example, an ExecuteRuleEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then
 * be handled by any class that has access to the EventBus
 * to execute the RuleApplyCommand on the ProofObligation with
 * the {@link ProofObligation#executeCommand(TreeCommand)} method.
 * <p>
 * See {@link invismt.controller.MasterController#verifyRule(ExecuteRuleEvent)}
 * for exemplary use.
 *
 * @author Tim Junginger
 */
public class ExecuteRuleEvent {

	private final RuleApplyCommand command;
	private final ProofObligation obligation;

	/**
	 * Create a new ExecuteRuleEvent by calling its constructor with the
	 * {@link RuleApplyCommand} to be executed and the {@link ProofObligation}
	 * the command is to be executed on.
	 *
	 * @param command    {@link RuleApplyCommand} to be executed on the obligation
	 * @param obligation {@link ProofObligation} inside of which the PTElement lies
	 */
	public ExecuteRuleEvent(RuleApplyCommand command, ProofObligation obligation) {
		this.command = command;
		this.obligation = obligation;
	}

	/**
	 * @return {@link RuleApplyCommand} to be executed
	 */
	public RuleApplyCommand getCommand() {
		return command;
	}

	/**
	 * @return {@link ProofObligation} on which the command will be executed
	 */
	public ProofObligation getObligation() {
		return obligation;
	}
}
