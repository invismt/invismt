/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.tree.ProofObligation;
import invismt.tree.TreeCommand;

/**
 * An event signalling that a change has been made to the history of activities
 * of a {@link ProofObligation}.
 * <p>
 * For example, a StartSolverEvent object can be posted on an
 * {@link com.google.common.eventbus.EventBus} and then be handled by any
 * class that has access to the EventBus to cause a change to a
 * {@link invismt.tree.TreeCommandHistory} of a {@link ProofObligation}.
 * <p>
 * See {@link invismt.controller.MasterController#undo()} for exemplary use.
 *
 * @author Tim Junginger
 */
public class ChangeHistoryEvent {

	private final ProofObligation obl;
	private final Type type;
	private final TreeCommand command;

	/**
	 * Creates a new event to change the history of the given
	 * {@link ProofObligation} in the way described by {@link Type}.
	 *
	 * @param obl  is the ProofObligation
	 * @param type is the Type of Chang (Undo, Redo)
	 */
	public ChangeHistoryEvent(ProofObligation obl, Type type) {
		this.obl = obl;
		this.type = type;
		this.command = null;
	}

	/**
	 * Creates a new event to change the history of the given
	 * {@link ProofObligation} in the way described by {@link Type} and possibly
	 * with the given {@link TreeCommand}.
	 *
	 * @param obl     is the ProofObligation
	 * @param type    is the {@link Type} of change that is done to the history
	 * @param command is the {@link TreeCommand} which is undone/redone or otherwise changes the history
	 */
	public ChangeHistoryEvent(ProofObligation obl, Type type, TreeCommand command) {
		this.obl = obl;
		this.type = type;
		this.command = command;
	}

	/**
	 * Returns the {@link ProofObligation} on which the history should be changed.
	 * If this returns null the currently active ProofObligation should be changed.
	 *
	 * @return proofObligation the identical {@link #obl} of this event
	 */
	public ProofObligation getProofObligation() {
		return this.obl;
	}

	/**
	 * @return type the identical {@link #type} of this history change
	 */
	public Type getType() {
		return this.type;
	}

	/**
	 * @return command the identical {@link #command} applied in this event
	 */
	public TreeCommand getCommand() {
		return this.command;
	}

	/**
	 * Types of possible history change events.
	 *
	 * @author Tim Junginger
	 */
	public enum Type {
		/**
		 * Used to execute a new step in the history.
		 */
		EXECUTE,
		/**
		 * Used to undo a step in the history.
		 */
		UNDO,
		/**
		 * Used to redo a previously undone step in the history.
		 */
		REDO;
	}

}
