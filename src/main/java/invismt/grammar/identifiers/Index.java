/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.identifiers;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * An index in version 2.6 of the SMT-LIB standard.
 * In that version, an index can be derived into either
 * a {@link NumeralIndex} or a {@link SymbolIndex}.
 * These two kinds of index are represented by subclasses
 * of the abstract Index class.
 */
public abstract class Index implements Expression {

	protected final Token token;

	/**
	 * Create an abstract index with the token it is derived into.
	 *
	 * @param token {@link Token} this index is derived into
	 */
	protected Index(Token token) {
		this.token = token;
	}

	/**
	 * Returns the {@link #token} of this Index identically.
	 *
	 * @return the {@link Token} this Index is derived into
	 */
	public abstract Token getToken();

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(1);
		children.add(token);
		return children;
	}

	@Override
	public abstract Index copy();

	@Override
	public abstract Index deepCopy();

	@Override
	public int hashCode() {
		return Objects.hashCode(token);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		Index o = (Index) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		return token.toString();
	}

	/**
	 * An index that is derived into a {@link invismt.grammar.Token.NumeralToken}.
	 */
	public static class NumeralIndex extends Index {

		/**
		 * Create a new numeral index with the token it is derived into.
		 *
		 * @param token {@link Token.NumeralToken} this index is derived into
		 */
		public NumeralIndex(Token.NumeralToken token) {
			super(token);
		}

		@Override
		public Token.NumeralToken getToken() {
			return (Token.NumeralToken) token;
		}

		@Override
		public NumeralIndex copy() {
			return new NumeralIndex(getToken());
		}

		@Override
		public NumeralIndex deepCopy() {
			return new NumeralIndex(getToken().deepCopy());
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

	/**
	 * An index that is derived into a {@link invismt.grammar.Token.SymbolToken}.
	 */
	public static class SymbolIndex extends Index {

		/**
		 * Create a new symbol index with the token it is derived into.
		 *
		 * @param token {@link Token.SymbolToken} this index is derived into
		 */
		public SymbolIndex(Token.SymbolToken token) {
			super(token);
		}

		@Override
		public Token.SymbolToken getToken() {
			return (Token.SymbolToken) token;
		}

		@Override
		public SymbolIndex copy() {
			return new SymbolIndex(getToken());
		}

		@Override
		public SymbolIndex deepCopy() {
			return new SymbolIndex(getToken().deepCopy());
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

	}

}

