/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;

/**
 * A generic base visitor implementing the generic {@link ExpressionVisitor} interface.
 * Its visit methods return null by default.
 * <p>
 * For an exemplary use of this class {@see invismt.rule.BoundVariableReplacer.ReplacementVisitor}.
 *
 * @param <T>
 */
public class ExpressionBaseVisitor<T> implements ExpressionVisitor<T> {

  @Override
  public T visit(GeneralExpression expression) {
    return null;
  }

  @Override
  public T visit(ForAllTerm forallTerm) {
    return null;
  }

  @Override
  public T visit(ExistsTerm existsTerm) {
    return null;
  }

  @Override
  public T visit(LetTerm letTerm) {
    return null;
  }

  @Override
  public T visit(AnnotationTerm annotationTerm) {
    return null;
  }

  @Override
  public T visit(MatchTerm matchTerm) {
    return null;
  }

  @Override
  public T visit(ConstantTerm constantTerm) {
    return null;
  }

  @Override
  public T visit(IdentifierTerm identifierTerm) {
    return null;
  }

  @Override
  public T visit(VariableBinding variableBinding) {
    return null;
  }

  @Override
  public T visit(SortedVariable sortedVariable) {
    return null;
  }

  @Override
  public T visit(Identifier identifier) {
    return null;
  }

  @Override
  public T visit(Index.NumeralIndex index) {
    return null;
  }

  @Override
  public T visit(Index.SymbolIndex index) {
    return null;
  }

  @Override
  public T visit(QualIdentifier qualIdentifier) {
    return null;
  }

  @Override
  public T visit(AttributeValue.ListAttributeValue value) {
    return null;
  }

  @Override
  public T visit(AttributeValue.ConstantAttributeValue value) {
    return null;
  }

  @Override
  public T visit(AttributeValue.SymbolAttributeValue value) {
    return null;
  }

  @Override
  public T visit(Attribute attribute) {
    return null;
  }

  @Override
  public T visit(Token.NumeralToken token) {
    return null;
  }

  @Override
  public T visit(Token.StringToken token) {
    return null;
  }

  @Override
  public T visit(Token.BinaryToken token) {
    return null;
  }

  @Override
  public T visit(Token.DecimalToken token) {
    return null;
  }

  @Override
  public T visit(Token.HexadecimalToken token) {
    return null;
  }

  @Override
  public T visit(Token.SymbolToken token) {
    return null;
  }

  @Override
  public T visit(Token.ReservedWord token) {
    return null;
  }

  @Override
  public T visit(Token.Keyword token) {
    return null;
  }

  @Override
  public T visit(SpecConstant.NumeralConstant constant) {
    return null;
  }

  @Override
  public T visit(SpecConstant.StringConstant constant) {
    return null;
  }

  @Override
  public T visit(SpecConstant.DecimalConstant constant) {
    return null;
  }

  @Override
  public T visit(SpecConstant.HexadecimalConstant constant) {
    return null;
  }

  @Override
  public T visit(SpecConstant.BinaryConstant constant) {
    return null;
  }

  @Override
  public T visit(Sort sort) {
    return null;
  }

  @Override
  public T visit(Pattern pattern) {
    return null;
  }

  @Override
  public T visit(MatchCase matchCase) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.Assert assertion) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.DeclareConstant declaration) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.CheckSat checkSat) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.Pop pop) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.Push push) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.Exit exit) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.Reset reset) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.DeclareFunction declareFunction) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.GetInfo getInfo) {
    return null;
  }

  @Override
  public T visit(SyntaxCommand.SetOption setOption) {
    return null;
  }
}
