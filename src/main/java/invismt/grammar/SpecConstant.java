/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import com.google.common.base.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * An SpecConstant in version 2.6 of the SMT-LIB standard.
 */
public abstract class SpecConstant implements Expression {

	protected final Token token;

	/**
	 * Create an abstract SpecConstant with the token it is derived into.
	 *
	 * @param token {@link Token} this SpecConstant is derived into
	 */
	protected SpecConstant(Token token) {
		this.token = token;
	}

	/**
	 * Returns the {@link #token} of this Index identically.
	 *
	 * @return the {@link Token} this Index is derived into
	 */
	public abstract Token getToken();

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(1);
		children.add(token);
		return children;
	}

	@Override
	public abstract SpecConstant copy();

	@Override
	public abstract SpecConstant deepCopy();

	@Override
	public int hashCode() {
		return Objects.hashCode(token);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		SpecConstant o = (SpecConstant) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		return token.toString();
	}

	/**
	 * An SpecConstant that is derived into a {@link Token.StringToken}.
	 */
	public static class StringConstant extends SpecConstant {

		/**
		 * Create a new StringConstant SpecConstant with the token it is derived into.
		 *
		 * @param token {@link Token.StringToken} this index is derived into
		 */
		public StringConstant(Token.StringToken token) {
			super(token);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public Token.StringToken getToken() {
			return (Token.StringToken) token;
		}

		@Override
		public StringConstant copy() {
			return new StringConstant(getToken());
		}

		@Override
		public StringConstant deepCopy() {
			return new StringConstant(getToken().deepCopy());
		}

	}

	/**
	 * An index that is derived into a {@link Token.NumeralToken}.
	 */
	public static class NumeralConstant extends SpecConstant {

		/**
		 * Create a new symbol SpecConstant with the token it is derived into.
		 *
		 * @param token {@link Token.NumeralToken} this SpecConstant is derived into
		 */
		public NumeralConstant(Token.NumeralToken token) {
			super(token);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public Token.NumeralToken getToken() {
			return (Token.NumeralToken) token;
		}

		@Override
		public NumeralConstant copy() {
			return new NumeralConstant(getToken());
		}

		@Override
		public NumeralConstant deepCopy() {
			return new NumeralConstant(getToken().deepCopy());
		}

	}

	/**
	 * An SpecConstant that is derived into a {@link Token.DecimalToken}.
	 */
	public static class DecimalConstant extends SpecConstant {
		/**
		 * Create a new symbol SpecConstant with the token it is derived into.
		 *
		 * @param token {@link Token.DecimalToken} this {@link SpecConstant} is derived into
		 */
		public DecimalConstant(Token.DecimalToken token) {
			super(token);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public Token.DecimalToken getToken() {
			return (Token.DecimalToken) token;
		}

		@Override
		public DecimalConstant copy() {
			return new DecimalConstant(getToken());
		}

		@Override
		public DecimalConstant deepCopy() {
			return new DecimalConstant(getToken().deepCopy());
		}

	}

	/**
	 * An {@link SpecConstant} that is derived into a {@link Token.HexadecimalToken}.
	 */
	public static class HexadecimalConstant extends SpecConstant {
		/**
		 * Create a new symbol {@link SpecConstant} with the token it is derived into.
		 *
		 * @param token {@link Token.HexadecimalToken} this {@link SpecConstant} is derived into
		 */
		public HexadecimalConstant(Token.HexadecimalToken token) {
			super(token);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public Token.HexadecimalToken getToken() {
			return (Token.HexadecimalToken) token;
		}

		@Override
		public HexadecimalConstant copy() {
			return new HexadecimalConstant(getToken());
		}

		@Override
		public HexadecimalConstant deepCopy() {
			return new HexadecimalConstant(getToken().deepCopy());
		}

	}

	/**
	 * An {@link SpecConstant} that is derived into a {@link Token.BinaryToken}.
	 */
	public static class BinaryConstant extends SpecConstant {
		/**
		 * Create a new symbol {@link SpecConstant} with the token it is derived into.
		 *
		 * @param token {@link Token.BinaryToken} this {@link SpecConstant} is derived into
		 */
		public BinaryConstant(Token.BinaryToken token) {
			super(token);
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return visitor.visit(this);
		}

		@Override
		public Token.BinaryToken getToken() {
			return (Token.BinaryToken) token;
		}

		@Override
		public BinaryConstant copy() {
			return new BinaryConstant(getToken());
		}

		@Override
		public BinaryConstant deepCopy() {
			return new BinaryConstant(getToken().deepCopy());
		}

	}

}
