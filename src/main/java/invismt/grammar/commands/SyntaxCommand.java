/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.commands;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.Term;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A command in version 2.6 of the SMT-LIB standard. In that version, a command
 * can be derived into many forms, some of which are represented as subclasses
 * of the SyntaxCommand class.
 * <p>
 * Commands from the SMT-LIB standard that are not implemented as subclasses of
 * the SyntaxCommand class can either be added or handled as
 * {@link invismt.grammar.GeneralExpression}.
 * <p>
 * Every command starts with a {@link Token.ReservedWord} representing the
 * command's name and is afterwards derived into a defined amount of child
 * expressions of special types: (name ...).
 */
public abstract class SyntaxCommand implements Expression {

  /**
   * The name of the command at hand.
   */
  protected final Token.ReservedWord name;

  /**
   * The child expressions containing the name that the command at hand is derived
   * into from left to right in the order of the list.
   * <p>
   * Returns the child expression identically to the attributes of the command
   * object at hand.
   */
  protected final List<Expression> children;

  /**
   * Create an abstract SyntaxCommand with the given name and a list of
   * expressions that only contains the command's name.
   *
   * @param name {@link Token.ReservedWord} representing the command at hand's
   *             name
   */
  protected SyntaxCommand(Token.ReservedWord name) {
    this.name = name;
    this.children = new ArrayList<>(1);
    children.add(name);
  }

  /**
   * Returns the identical {@link #name} name of the command at hand.
   *
   * @return the {@link Token.ReservedWord} representing the command at hand's
   *         name
   */
  public Token.ReservedWord getCommandName() {
    return name;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(children);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!other.getClass().equals(this.getClass())) {
      return false;
    }
    SyntaxCommand o = (SyntaxCommand) other;
    return o.getChildren().equals(this.getChildren());
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
    for (Expression child : getChildren()) {
      joiner.add(child.toString());
    }
    return joiner.toString();
  }

  @Override
  public List<Expression> getChildren() {
    return new ArrayList<>(children);
  }

  /**
   * The exit command in the SMT-LIB standard version 2.6: (exit).
   */
  public static class Exit extends SyntaxCommand {

    /**
     * Create a new Exit command object whose {@link #name} is created anew when
     * this constructor is called.
     */
    public Exit() {
      super(new Token.ReservedWord(Token.ReservedWordName.EXIT));
    }

    @Override
    public Exit copy() {
      return new Exit();
    }

    @Override
    public Exit deepCopy() {
      return new Exit();
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

  }

  /**
   * The push command in the SMT-LIB standard version 2.6: (push n) where n is a
   * {@link Token.NumeralToken} representing the magnitude of the push command.
   */
  public static class Push extends SyntaxCommand {

    private final Token.NumeralToken magnitude;

    /**
     * Create a new Push command object whose {@link #name} is created anew when
     * this constructor is called, with the given magnitude.
     *
     * @param mag {@link Token.NumeralToken} representing the push command at hand's
     *            magnitude
     */
    public Push(Token.NumeralToken mag) {
      super(new Token.ReservedWord(Token.ReservedWordName.PUSH));
      this.magnitude = mag;
      children.add(magnitude);
    }

    /**
     * Returns the identical {@link #magnitude} of this push command object
     * identically.
     *
     * @return the {@link Token.NumeralToken} representing the push command at
     *         hand's magnitude
     */
    public Token.NumeralToken getMagnitude() {
      return magnitude;
    }

    @Override
    public Push copy() {
      return new Push(this.magnitude);
    }

    @Override
    public Push deepCopy() {
      return new Push(this.magnitude.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

  }

  /**
   * The pop command in the SMT-LIB standard version 2.6: (pop n) where n is a
   * {@link Token.NumeralToken} representing the magnitude of the pop command.
   */
  public static class Pop extends SyntaxCommand {

    private final Token.NumeralToken magnitude;

    /**
     * Create a new Pop command object whose {@link #name} is created anew when this
     * constructor is called, with the given magnitude.
     *
     * @param mag {@link Token.NumeralToken} representing the pop command at hand's
     *            magnitude
     */
    public Pop(Token.NumeralToken mag) {
      super(new Token.ReservedWord(Token.ReservedWordName.POP));
      this.magnitude = mag;
      children.add(magnitude);
    }

    /**
     * Returns the identical {@link #magnitude} of this pop command object
     * identically.
     *
     * @return the {@link Token.NumeralToken} representing the pop command at hand's
     *         magnitude
     */
    public Token.NumeralToken getMagnitude() {
      return magnitude;
    }

    @Override
    public Pop copy() {
      return new Pop(this.magnitude);
    }

    @Override
    public Pop deepCopy() {
      return new Pop(this.magnitude.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

  }

  /**
   * This class corresponds to a "family" of commands; check-sat and
   * check-sat-assuming We seperate between the individual commands through
   * homogenous constructors
   */
  public static class CheckSat extends SyntaxCommand {

    /**
     * Create a new CheckSat command object whose {@link #name} is created anew when
     * this constructor is called.
     */
    public CheckSat() {
      super(new Token.ReservedWord(Token.ReservedWordName.CHECKSAT));
    }

    // private CheckSat(List<ProperLiteral> assumptions) { ... }
    // See issue #16

    @Override
    public CheckSat copy() {
      return new CheckSat();
    }

    @Override
    public CheckSat deepCopy() {
      return new CheckSat();
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

  }

  /**
   * The set-option command in the SMT-LIB standard version 2.6: (set-option attribute).
   */
  public static class SetOption extends SyntaxCommand {

    private final Attribute option;

    /**
     * Create a new set-option command with the given  {@link Attribute}.
     *
     * @param option the Attribute that is the option to be set, such as ":global-declarations true".
     */
    public SetOption(Attribute option) {
      super(new Token.ReservedWord(Token.ReservedWordName.SETOPTION));
      this.option = option;
      children.add(option);
    }

    /**
     *
     * @return the identical {@link #option} of this command
     */
    public Attribute getOption() {
      return this.option;
    }

    @Override
    public SetOption copy() {
      return new SetOption(this.option);
    }

    @Override
    public SetOption deepCopy() {
      return new SetOption(this.option.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }
  }

  /**
   * The get-info command in the SMT-LIB standard version 2.6: (get-info keyword).
   */
  public static class GetInfo extends SyntaxCommand {

    private final Token.Keyword parameter;

    /**
     * Create a new get-info command with the given  {@link Token.Keyword}.
     *
     * @param parameter the Keyword info-parameter, e.g. :authors.
     */
    public GetInfo(Token.Keyword parameter) {
      super(new Token.ReservedWord(Token.ReservedWordName.GETINFO));
      this.parameter = parameter;
      children.add(parameter);
    }

    /**
     *
     * @return the identical {@link #parameter} of this command
     */
    public Token.Keyword getParameter() {
      return this.parameter;
    }

    @Override
    public GetInfo copy() {
      return new GetInfo(this.parameter);
    }

    @Override
    public GetInfo deepCopy() {
      return new GetInfo(this.parameter.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }
  }

  /**
   * The reset command in the SMT-LIB standard version 2.6: (reset).
   */
  public static class Reset extends SyntaxCommand {

    /**
     * Create a new Reset command object whose {@link #name} is created anew when
     * this constructor is called.
     */
    public Reset() {
      super(new Token.ReservedWord(Token.ReservedWordName.RESET));
    }

    @Override
    public Reset copy() {
      return new Reset();
    }

    @Override
    public Reset deepCopy() {
      return new Reset();
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

  }

  /**
   * The assert command in the SMT-LIB standard version 2.6: (assert term) where
   * term is any {@link Term}.
   * <p>
   * (no semantic type-checks for well-sorted terms according to the SMT-LIB
   * standard are taking place inside of the {@link Expression} hierarchy)
   */
  public static class Assert extends SyntaxCommand {

    private final Term term;

    /**
     * Create a new Assert command object whose {@link #name} is created anew when
     * this constructor is called, with the given {@link Term}.
     *
     * @param term the {@link Term} this assert command object is derived into
     */
    public Assert(Term term) {
      super(new Token.ReservedWord(Token.ReservedWordName.ASSERT));
      this.term = term;
      children.add(term);
    }

    /**
     * Returns the {@link #term} of this assert command object identically.
     *
     * @return {@link Term} term of this assert command
     */
    public Term getTerm() {
      return term;
    }

    @Override
    public Assert copy() {
      return new Assert(term);
    }

    @Override
    public Assert deepCopy() {
      return new Assert(term.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

  }

  /**
   * The declare-const command in the SMT-LIB standard version 2.6: (declare-const
   * x sort) where x is a {@link Token.SymbolToken} representing the name of the
   * declared variable and sort is a {@link Sort} representing the variable's
   * type.
   */
  public static class DeclareConstant extends SyntaxCommand {

    private final Sort constType;
    private final Token.SymbolToken constName;

    /**
     * Create a new declare-const command whose {@link #name} is created anew when
     * this constructor is called, with the given {@link Token.SymbolToken} and
     * {@link Sort}.
     *
     * @param constName {@link Token.SymbolToken} representing the variable declared
     *                  by this command
     * @param constType {@link Sort} representing the sort of the variable
     *                  represented by constName
     */
    public DeclareConstant(Token.SymbolToken constName, Sort constType) {
      super(new Token.ReservedWord(Token.ReservedWordName.DECLARECONST));
      this.constName = constName;
      this.constType = constType;
      children.add(constName);
      children.add(constType);
    }

    /**
     * Returns the {@link #constType} of this command identically.
     *
     * @return the {@link #constType} of this command
     */
    public Sort getConstType() {
      return constType;
    }

    /**
     * Returns the {@link #constName} of this command identically.
     *
     * @return the {@link #constName} of this command
     */
    public Token.SymbolToken getConstName() {
      return constName;
    }

    @Override
    public DeclareConstant copy() {
      return new DeclareConstant(constName, constType);
    }

    @Override
    public DeclareConstant deepCopy() {
      return new DeclareConstant(constName.deepCopy(), constType.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

    @Override
    public String toString() {
      StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
      joiner.add(this.name.toString());
      joiner.add(this.constName.toString());
      joiner.add(this.constType.toString());
      return joiner.toString();
    }
  }

  /**
   * The declare-fun command in the SMT-LIB standard version 2.6: (declare-fun f
   * (sort*) sort) where f is a {@link Token.SymbolToken} representing the name of
   * the declared function and sort are {@link Sort}s representing the functions
   * input and output types.
   */
  public static class DeclareFunction extends SyntaxCommand {

    private final Sort targetArea;
    private final List<Sort> definitionArea;
    private final Token.SymbolToken funName;

    /**
     * Create a new declare-fun command whose {@link #name} is created anew when
     * this constructor is called, with the given {@link Token.SymbolToken} as
     * function symbol and list of {@link Sort Sorts} as definition area as well as
     * the {@link Sort} as target area.
     *
     * @param funName        {@link Token.SymbolToken} representing the function
     *                       declared by this command
     * @param definitionArea list of {@link Sort Sorts} representing the definition
     *                       area of the function declared by this command
     * @param targetArea     {@link Sort} representing the target area of the
     *                       function declared by this command
     */
    public DeclareFunction(Token.SymbolToken funName, List<Sort> definitionArea, Sort targetArea) {
      super(new Token.ReservedWord(Token.ReservedWordName.DECLAREFUN));
      this.funName = funName;
      this.definitionArea = new ArrayList<>(definitionArea);
      this.targetArea = targetArea;
      children.add(funName);
      children.addAll(definitionArea);
      children.add(targetArea);
    }

    /**
     * Returns the {@link #targetArea} of this command identically.
     *
     * @return the {@link #targetArea} of this command
     */
    public Sort getTargetArea() {
      return targetArea;
    }

    /**
     * Returns the contents of {@link #definitionArea} of this command identically
     * inside of a new list. Mind that the definition area might be empty.
     *
     * @return the {@link #definitionArea} of this command
     */
    public List<Sort> getDefinitionArea() {
      return new ArrayList<>(definitionArea);
    }

    /**
     * Returns the {@link #funName} of this command identically.
     *
     * @return the {@link #funName} of this command
     */
    public Token.SymbolToken getConstName() {
      return funName;
    }

    @Override
    public DeclareFunction copy() {
      return new DeclareFunction(funName, definitionArea, targetArea);
    }

    @Override
    public DeclareFunction deepCopy() {
      List<Sort> definitionAreaCopy = definitionArea.stream().map(Sort::deepCopy).toList();
      return new DeclareFunction(funName.deepCopy(), definitionAreaCopy, targetArea.deepCopy());
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
      return visitor.visit(this);
    }

    @Override
    public String toString() {
      StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
      joiner.add(this.name.toString());
      joiner.add(this.funName.toString());
      joiner.add(PAR_OPEN);
      for (Sort inputType : definitionArea) {
        joiner.add(inputType.toString());
      }
      joiner.add(PAR_CLOSED);
      joiner.add(targetArea.toString());
      return joiner.toString();
    }
  }

}
