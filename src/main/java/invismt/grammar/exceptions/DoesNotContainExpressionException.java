/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.exceptions;

import invismt.grammar.Expression;
import invismt.grammar.Script;

/**
 * Exception that can be thrown in a {@link Script} when
 * an {@link Expression} is searched and not found in that script.
 */
@SuppressWarnings("serial")
public class DoesNotContainExpressionException extends IllegalArgumentException {

	/**
	 * Create a new DoesNotContainExpressionException with the desired
	 * expression and the script that does not contain that expression.
	 *
	 * @param expr   the expression to be looked for in the script
	 * @param script the script the expression where the expression is searched
	 */
	public DoesNotContainExpressionException(Expression expr, Script script) {
		super("Script does not contain " + expr.toString());
	}

}