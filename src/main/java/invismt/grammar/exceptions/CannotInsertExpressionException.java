/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.exceptions;

import invismt.grammar.Expression;
import invismt.grammar.Script;

/**
 * Exception that can be thrown in a {@link Script} when an {@link Expression} is
 * to be inserted into the script that cannot be inserted for some reason.
 */
@SuppressWarnings("serial")
public class CannotInsertExpressionException extends IllegalArgumentException {

	/**
	 * Create a new CannotInsertExpressionException with the problematic
	 * expression and the script it is to be inserted into.
	 *
	 * @param expr   the expression to be inserted in to the script
	 * @param script the script the expression is to be inserted into
	 */
	public CannotInsertExpressionException(Expression expr, Script script) {
		super("Cannot insert " + expr.toString() + " into script.");
	}

}
