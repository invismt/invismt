/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.commands.SyntaxCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * An S-expression in the SMT-LIB standard.
 * Implementing classes and extending interfaces represent
 * subcategories of the SMT-LIB syntax.
 * <p>
 * Note that implementations of this interface should be immutable in their attributes,
 * which is why there are only getter-methods - and no setter-methods - to be implemented.
 */
public interface Expression {
	/**
	 * PAR OPEN
	 */
	String PAR_OPEN = "(";
	/**
	 * PAR CLOSED
	 */
	String PAR_CLOSED = ")";
	/**
	 * SPACE
	 */
	String SPACE = " ";

	/**
	 * Getter
	 *
	 * @return List of Children
	 */
	List<Expression> getChildren();
	// Do not return cloned children, otherwise searching for expressions won't work.

	/**
	 * Checks whether an expression equal to the given one is contained
	 * in the expression hierarchy that starts with the expression at hand
	 * as a root.
	 *
	 * @param expr the expression to be searched for in this expression's hierarchy
	 * @return true if an expression equal to the given one is contained in the hierarchy
	 */
	default boolean containsEqualExpressionSomewhere(Expression expr) {
		Expression current = this;
		if (current.equals(expr)) {
			return true;
		}
		List<Expression> currentChildren = current.getChildren();
		for (Expression child : currentChildren) {
			current = child;
			if (current.containsEqualExpressionSomewhere(expr)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether the given expression is contained identically (same object)
	 * in the expression hierarchy that starts with the expression at hand as a root.
	 *
	 * @param expr the expression to be searched for in this expression's hierarchy
	 * @return true if the given expression is identically contained in the hierarchy
	 */
	default boolean containsIdenticallySomewhere(Expression expr) {
		Expression current = this;
		if (current == expr) {
			return true;
		}
		List<Expression> currentChildren = current.getChildren();
		for (Expression child : currentChildren) {
			current = child;
			if (current.containsIdenticallySomewhere(expr)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the list of parents of the given expression in the expression at hand.
	 * <p>
	 * Returns an empty list iff the given expression is not identically contained in
	 * the expression at hand according to {@link #containsIdenticallySomewhere(Expression)}.
	 * If the given expression is contained identically more than once, the list of parents
	 * of the first occurrence of the given expression is returned.
	 * The children are searched in the order given by the list returned by
	 * {@link #getChildren()}.
	 *
	 * @param expr the expression whose parent list is to be returned
	 * @return the list of parents of the first identical occurrence of the given expression
	 */
	default List<Expression> getParentChain(Expression expr) {
		List<Expression> parentChain = new ArrayList<>();
		if (!containsIdenticallySomewhere(expr)) {
			return parentChain;
		}
		parentChain.add(this);
		if (this == expr) {
			return parentChain;
		}
		for (Expression child : getChildren()) {
			if (child.containsIdenticallySomewhere(expr)) {
				parentChain.addAll(child.getParentChain(expr));
				break;
			}
		}
		return parentChain;
	}

	/**
	 * Returns a list of all expressions that are contained in the expression at hand
	 * somewhere according to {@link #containsIdenticallySomewhere(Expression)}.
	 * <p>
	 * The returned list contains the expression at hand first and adds all expressions
	 * contained in the children returned by {@link #getChildren()} in that order.
	 *
	 * @return a list of all expressions contained somewhere in the expression at hand
	 */
	default List<Expression> getChildrenOfChildren() {
		List<Expression> childrenOfChildren = new ArrayList<>();
		childrenOfChildren.add(this);
		for (Expression expr : getChildren()) {
			childrenOfChildren.addAll(expr.getChildrenOfChildren());
		}
		return childrenOfChildren;
	}

	/**
	 * Clones an expression by creating a new expression with the same attributes
	 * that are needed by the constructor as the one at hand.
	 * <p>
	 * Mind that other attributes may not be the same, e.g. the
	 * {@link SyntaxCommand#getCommandName()}
	 * is generally created anew for each new command instance.
	 * <p>
	 * The returned expression is not identical to the one at hand but their children are identical.
	 * The returned expression is equal according to {@link #equals(Object)}.
	 *
	 * @return a new expression with the same children as the one at hand
	 */
	Expression copy();

	/**
	 * Clones an expression by creating a new expression where the children are also deep-copied.
	 * <p>
	 * The returned expression and it's children are not identical to the one at hand.
	 * The returned expression is equal according to {@link #equals(Object)}.
	 *
	 * @return a new expression with deep-copied children
	 */
	Expression deepCopy();

	/**
	 * Accepts a visitor to the expression class hierarchy according to
	 * the visitor design pattern.
	 *
	 * @param visitor the visitor that visits the expression at hand
	 * @param <T>     the generic return type of the given visitor's visit method
	 * @return an object of the given visitor's return type
	 */
	<T> T accept(ExpressionVisitor<T> visitor);

	/**
	 * Checks whether two expressions are equal.
	 * <p>
	 * That is generally the case iff their children are identical or equal
	 * and the two expressions are of the same class.
	 * If expressions have other attributes, those need to be the same or equal as well.
	 *
	 * @param other the expression which the one at hand is compared to
	 * @return whether the expression at hand is equal to the given one
	 */
	@Override
	boolean equals(Object other);

	/**
	 * @return the String representation of the expression at hand according to the SMT-LIB standard version 2.6
	 */
	@Override
	String toString();

}
