/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * An identifier term in the SMT-LIB standard version 2.6:
 * "identifier" or "( identifier term+ )"
 * <p>
 * An identifier term without identified terms generally represents a single variable,
 * an identifier term with identified terms generally represents a function application.
 */
public class IdentifierTerm implements Term {

	private final QualIdentifier identifier;
	private final List<Term> identifiedTerms;

	/**
	 * Create a new identifier term with a qualified identifier
	 * and a possibly empty list of identified terms.
	 * If the list is empty, the term is derived into "identifier" and generally
	 * represents a variable.
	 * If the list is not empty, the term is derived into "(identifier term+)" and
	 * generally represents a function application.
	 *
	 * @param identifier      {@link QualIdentifier} of the created term representing either
	 *                        a variable or a function
	 * @param identifiedTerms list of {@link Term terms} that the function represented
	 *                        by the given qualified identifier is applied to
	 */
	public IdentifierTerm(QualIdentifier identifier, List<Term> identifiedTerms) {
		this.identifier = identifier;
		this.identifiedTerms = new ArrayList<>(identifiedTerms); // clone list so it cannot be manipulated
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>();
		children.add(identifier);
		if (identifiedTerms.isEmpty()) {
			return children;
		}
		children.addAll(identifiedTerms);
		return children;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	/**
	 * Returns the {@link #identifiedTerms identified terms} (the "function arguments")
	 * of this term identically.
	 * Creates a new list containing these identified terms.
	 *
	 * @return the {@link #identifiedTerms} of this term
	 */
	public List<Term> getIdentifiedTerms() {
		return new ArrayList<>(identifiedTerms);
	}

	/**
	 * Returns the {@link #identifiedTerms} of this term identically.
	 *
	 * @return the {@link #identifier} of this term
	 */
	public QualIdentifier getIdentifier() {
		return identifier;
	}

	/**
	 * An identifier term does not bind any variables so this method returns an empty list.
	 */
	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		return new ArrayList<>();
	}

	/**
	 * If an identifier term consists only of its identifier, it represents a variable.
	 * The variable itself is a free occurrence, it only becomes bound if it is part of
	 * another term such as a {@link QuantifiedTerm}.
	 * <p>
	 * If an identifier term has at least one item in its {@link #identifiedTerms}, it
	 * represents a function application of the function named by the identifier of the
	 * term. Then, the free variable occurrences of the identifier term are the entirety
	 * of the free variable occurrences in its identified terms.
	 */
	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		List<Token.SymbolToken> freeVariables = new ArrayList<>();
		if (identifiedTerms.isEmpty()) {
			freeVariables.add(identifier.getQualifiedIdentifier().getSymbol());
		}
		for (Term term : identifiedTerms) {
			freeVariables.addAll(term.getFreeVariables());
		}
		return freeVariables;
	}

	@Override
	public IdentifierTerm copy() {
		return new IdentifierTerm(identifier, identifiedTerms);
	}

	@Override
	public IdentifierTerm deepCopy() {
		List<Term> newTerms = new ArrayList<>();
		for (Term term : identifiedTerms) {
			newTerms.add(term.deepCopy());
		}
		return new IdentifierTerm(identifier.deepCopy(), newTerms);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(identifiedTerms, identifier);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		IdentifierTerm o = (IdentifierTerm) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner;
		if (identifiedTerms.isEmpty()) {
			joiner = new StringJoiner(SPACE);
		} else {
			joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		}
		joiner.add(identifier.toString());
		for (Term term : identifiedTerms) {
			joiner.add(term.toString());
		}
		return joiner.toString();
	}

}
