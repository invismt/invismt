/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A match term in the SMT-LIB standard version 2.6:
 * "( match term ( (match case)+ ) )"
 */
public class MatchTerm implements Term {

	private static final String CANNOT_CREATE = "Cannot create match term without match cases.";
	private final Term term;
	private final List<MatchCase> matchCases;

	/**
	 * Create a new match term with the term to which the match cases are
	 * applied and a non-empty list of match cases that will be matched
	 * according to the SMT-LIB standard inside of the given term.
	 *
	 * @param matchedTerm {@link Term} to which the match cases are applied
	 * @param matchCases  list of {@link MatchCase match cases} that are matched inside of the term
	 * @throws IllegalArgumentException if the list of match cases is empty
	 */
	public MatchTerm(Term matchedTerm, List<MatchCase> matchCases) {
		if (matchCases.isEmpty()) {
			throw new IllegalArgumentException(CANNOT_CREATE);
		}
		this.matchCases = new ArrayList<>(matchCases);
		this.term = matchedTerm;
	}

	/**
	 * Returns the {@link #term matched term} identically.
	 *
	 * @return the {@link #term matched term} of this term
	 */
	public Term getTerm() {
		return term;
	}

	/**
	 * Returns a new list of the identical {@link #matchCases match cases} of this term.
	 *
	 * @return a new list of the identical {@link #matchCases} of this term
	 */
	public List<MatchCase> getMatchCases() {
		return new ArrayList<>(matchCases);
	}

	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		return new ArrayList<>();
	}

	/**
	 * According to the SMT-LIB standard, a variable occurrence in a match term of the
	 * form "match t (matchcases)" is free
	 * " if it occurs free in t or it
	 * occurs free in some t_i (1 ≤ i ≤ n) and does not occur in the corresponding p_i "
	 *
	 * @return the free variables in a LetTerm
	 */
	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		List<Token.SymbolToken> freeVariables = new ArrayList<>();
		for (MatchCase mCase : matchCases) {
			for (Token.SymbolToken varName : mCase.getMatchedTerm().getFreeVariables()) {
				if (!mCase.getMatchedPattern().containsEqualExpressionSomewhere(varName)) {
					freeVariables.add(varName);
				}
			}
		}
		freeVariables.addAll(term.getFreeVariables());
		return freeVariables;
	}

	@Override
	public MatchTerm copy() {
		return new MatchTerm(term, matchCases);
	}

	@Override
	public MatchTerm deepCopy() {
		List<MatchCase> newMatchCases = new ArrayList<>();
		for (MatchCase matchCase : matchCases) {
			newMatchCases.add(matchCase.deepCopy());
		}
		return new MatchTerm(term.deepCopy(), newMatchCases);
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(3);
		children.add(new Token.ReservedWord(Token.ReservedWordName.MATCH));
		children.add(term);
		children.addAll(matchCases);
		return children;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(matchCases, term);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		MatchTerm o = (MatchTerm) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		joiner.add(term.toString());
		joiner.add(PAR_OPEN);
		for (MatchCase matchCase : matchCases) {
			joiner.add(matchCase.toString());
		}
		joiner.add(PAR_CLOSED);
		return joiner.toString();
	}

}
