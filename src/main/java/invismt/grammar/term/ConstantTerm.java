/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * A constant term in the SMT-LIB standard version 2.6:
 * "constant"
 */
public class ConstantTerm implements Term {

	private final SpecConstant constant;

	/**
	 * Create a new constant term representing a given constant.
	 *
	 * @param constant {@link SpecConstant} represented by the created term
	 */
	public ConstantTerm(SpecConstant constant) {
		this.constant = constant;
	}

	/**
	 * Returns the represented {@link #constant} identically.
	 *
	 * @return the {@link #constant} of this term
	 */
	public SpecConstant getConstant() {
		return constant;
	}

	/**
	 * A constant term contains no bound variable occurrences as it does not
	 * contain variables at all.
	 */
	@Override
	public List<Token.SymbolToken> getBoundVariables() {
		return new ArrayList<>();
	}

	/**
	 * A constant term contains no free variable occurrences as it does not
	 * contain variables at all.
	 */
	@Override
	public List<Token.SymbolToken> getFreeVariables() {
		return new ArrayList<>();
	}

	@Override
	public ConstantTerm copy() {
		return new ConstantTerm(constant);
	}

	@Override
	public ConstantTerm deepCopy() {
		return new ConstantTerm(constant.deepCopy());
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(1);
		children.add(constant);
		return children;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(constant);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		ConstantTerm o = (ConstantTerm) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		return constant.toString();
	}

}
