/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.term;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A match case in the SMT-LIB standard version 2.6.
 * In that version, a sort can be derived into "(pattern term)"
 * where pattern is a {@link Pattern} and term is a {@link Term}.
 */
public class MatchCase implements Expression {

	private final Pattern pattern;
	private final Term term;

	/**
	 * Create a new match case with a pattern and a term
	 * that will be matched with the pattern.
	 *
	 * @param pattern {@link Pattern} of this match case
	 * @param term    {@link Term} of this match case
	 */
	public MatchCase(Pattern pattern, Term term) {
		this.term = term;
		this.pattern = pattern;
	}

	/**
	 * Returns the {@link #term matched term} of this match case identically.
	 *
	 * @return the {@link #term matched term} of this match case
	 */
	public Term getMatchedTerm() {
		return term;
	}

	/**
	 * Returns the {@link #pattern} of this match case identically.
	 *
	 * @return the {@link #pattern} of this match case
	 */
	public Pattern getMatchedPattern() {
		return pattern;
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(2);
		children.add(pattern);
		children.add(term);
		return children;
	}

	@Override
	public MatchCase copy() {
		return new MatchCase(pattern, term);
	}

	@Override
	public MatchCase deepCopy() {
		return new MatchCase(pattern.deepCopy(), term.deepCopy());
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(pattern, term);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		MatchCase o = (MatchCase) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		for (Expression child : getChildren()) {
			joiner.add(child.toString());
		}
		return joiner.toString();
	}

}
