/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;

/**
 * This interface offers the base constraint(s) for a complete
 * expression visitor; namely the fact that it should be able to visit
 * any and all subclasses of {@link Expression}.
 *
 * Please add new visit methods for new {@link Expression} subtypes if
 * the package {@link invismt.grammar} is extended.
 *
 * @param <T> The type of the object that shall be returned upon
 * visiting an Expression subtype
 */
public interface ExpressionVisitor<T> {
  /**
   * Visit the given {@link GeneralExpression}.
   *
   * GeneralExpressions represent expressions that cannot be
   * articulated further with any other Expression subclasses.
   *
   * @param expression GeneralExpression instance to be visited
   * @return an object of type T
   */
  T visit(GeneralExpression expression);

  //// Terms

  /**
   * Visit the given {@link ForAllTerm}.
   *
   * @param forallTerm ForallTerm instance to be visited
   * @return an object of type T
   */
  T visit(ForAllTerm forallTerm);

  /**
   * Visit the given {@link ExistsTerm}.
   *
   * @param existsTerm ExistsTerm instance to be visited
   * @return an object of type T
   */
  T visit(ExistsTerm existsTerm);

  /**
   * Visit the given {@link LetTerm}.
   *
   * @param letTerm LetTerm instance to be visited
   * @return an object of type T
   */
  T visit(LetTerm letTerm);

  /**
   * Visit the given {@link AnnotationTerm}
   *
   * @param annotationTerm AnnotationTerm instance to be visited
   * @return an object of type T
   */
  T visit(AnnotationTerm annotationTerm);

  /**
   * Visit the given {@link MatchTerm}.
   *
   * @param matchTerm MatchTerm instance to be visited
   * @return an object of type T
   */
  T visit(MatchTerm matchTerm);

  /**
   * Visit the given {@link ConstantTerm}.
   *
   * @param constantTerm ConstantTerm instance to be visited
   * @return an object of type T
   */
  T visit(ConstantTerm constantTerm);

  /**
   * Visit the given {@link IdentifierTerm}.
   *
   * @param identifierTerm IdentifierTerm instance to be visited
   * @return an object of type T
   */
  T visit(IdentifierTerm identifierTerm);

  /**
   * Visit the given {@link VariableBinding}.
   *
   * @param variableBinding VariableBinding instance to be visited
   * @return an object of type T
   */
  T visit(VariableBinding variableBinding);

  //// Variables

  /**
   * Visit the given {@link SortedVariable}.
   *
   * @param sortedVariable SortedVariable instance to be visited
   * @return an object of type T
   */
  T visit(SortedVariable sortedVariable);

  //// Identifiers and indexes

  /**
   * Visit the given {@link Identifier}.
   *
   * @param identifier Identifier instance to be visited
   * @return an object of type T
   */
  T visit(Identifier identifier);

  /**
   * Visit the given {@link Index.NumeralIndex}.
   *
   * @param index NumeralIndex instance to be visited
   * @return an object of type T
   */
  T visit(Index.NumeralIndex index);

  /**
   * Visit the given {@link Index.SymbolIndex}.
   *
   * @param index SymbolIndex instance to be visited
   * @return an object of type T
   */
  T visit(Index.SymbolIndex index);

  /**
   * Visit the given {@link QualIdentifier}.
   *
   * @param qualIdentifier QualIdentifier instance to be visited
   * @return an object of type T
   */
  T visit(QualIdentifier qualIdentifier);

  //// Attributes

  /**
   * Visit the given {@link AttributeValue.ListAttributeValue}.
   *
   * @param value ListAttributeValue instance to be visited
   * @return an object of type T
   */
  T visit(AttributeValue.ListAttributeValue value);

  /**
   * Visit the given {@link AttributeValue.ConstantAttributeValue}.
   *
   * @param value ConstantAttributeValue instance to be visited
   * @return an object of type T
   */
  T visit(AttributeValue.ConstantAttributeValue value);

  /**
   * Visit the given {@link AttributeValue.SymbolAttributeValue}.
   *
   * @param value SymbolAttributeValue instance to be visited
   * @return an object of type T
   */
  T visit(AttributeValue.SymbolAttributeValue value);

  /**
   * Visit the given {@link Attribute}.
   *
   * @param attribute Attribute instance to be visited
   * @return an object of type T
   */
  T visit(Attribute attribute);

  //// Tokens

  /**
   * Visit the given {@link Token.NumeralToken}.
   *
   * @param token NumeralToken instance to be visited
   * @return an object of type T
   */
  T visit(Token.NumeralToken token);

  /**
   * Visit the given {@link Token.StringToken}.
   *
   * @param token StringToken instance to be visited
   * @return an object of type T
   */
  T visit(Token.StringToken token);

  /**
   * Visit the given {@link Token.BinaryToken}.
   *
   * @param token BinaryToken instance to be visited
   * @return an object of type T
   */
  T visit(Token.BinaryToken token);

  /**
   * Visit the given {@link Token.DecimalToken}.
   *
   * @param token DecimalToken instance to be visited
   * @return an object of type T
   */
  T visit(Token.DecimalToken token);

  /**
   * Visit the given {@link Token.HexadecimalToken}.
   *
   * @param token HexadecimalToken instance to be visited
   * @return an object of type T
   */
  T visit(Token.HexadecimalToken token);

  /**
   * Visit the given {@link Token.SymbolToken}.
   *
   * @param token SymbolToken instance to be visited
   * @return an object of type T
   */
  T visit(Token.SymbolToken token);

  /**
   * Visit the given {@link Token.ReservedWord}.
   *
   * @param token ReservedWord instance to be visited
   * @return an object of type T
   */
  T visit(Token.ReservedWord token);

  /**
   * Visit the given {@link Token.Keyword}.
   *
   * @param token Keyword instance to be visited
   * @return an object of type T
   */
  T visit(Token.Keyword token);

  //// Constants

  /**
   * Visit the given {@link SpecConstant.NumeralConstant}.
   *
   * @param constant NumeralConstant instance to be visited
   * @return an object of type T
   */
  T visit(SpecConstant.NumeralConstant constant);

  /**
   * Visit the given {@link SpecConstant.StringConstant}.
   *
   * @param constant StringConstant instance to be visited
   * @return an object of type T
   */
  T visit(SpecConstant.StringConstant constant);

  /**
   * Visit the given {@link SpecConstant.DecimalConstant}.
   *
   * @param constant DecimalConstant instance to be visited
   * @return an object of type T
   */
  T visit(SpecConstant.DecimalConstant constant);

  /**
   * Visit the given {@link SpecConstant.HexadecimalConstant}.
   *
   * @param constant HexadecimalConstant instance to be visited
   * @return an object of type T
   */
  T visit(SpecConstant.HexadecimalConstant constant);

  /**
   * Visit the given {@link SpecConstant.BinaryConstant}.
   *
   * @param constant BinaryConstant instance to be visited
   * @return an object of type T
   */
  T visit(SpecConstant.BinaryConstant constant);

  //// Sort

  /**
   * Visit the given {@link Sort}.
   *
   * @param sort Sort instance to be visited
   * @return an object of type T
   */
  T visit(Sort sort);

  //// Pattern matching

  /**
   * Visit the given {@link Pattern}.
   *
   * @param pattern Pattern instance to be visited
   * @return an object of type T
   */
  T visit(Pattern pattern);

  /**
   * Visit the given {@link MatchCase}.
   *
   * @param matchCase MatchCase instance to be visited
   * @return an object of type T
   */
  T visit(MatchCase matchCase);

  //// Commands and command subtypes

  /**
   * Visit the given {@link SyntaxCommand.Assert}.
   *
   * @param assertion Assert instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.Assert assertion);

  /**
   * Visit the given {@link SyntaxCommand.DeclareConstant}.
   *
   * @param declaration DeclareConstant instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.DeclareConstant declaration);

  /**
   * Visit the given {@link SyntaxCommand.CheckSat}.
   *
   * @param checkSat CheckSat instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.CheckSat checkSat);

  /**
   * Visit the given {@link SyntaxCommand.Pop}.
   *
   * @param pop Pop instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.Pop pop);

  /**
   * Visit the given {@link SyntaxCommand.Push}.
   *
   * @param push Push instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.Push push);

  /**
   * Visit the given {@link SyntaxCommand.Exit}.
   *
   * @param exit Exit instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.Exit exit);

  /**
   * Visit the given {@link SyntaxCommand.Reset}.
   *
   * @param reset Reset instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.Reset reset);

  /**
   * Visit the given {@link SyntaxCommand.DeclareFunction}.
   *
   * @param declareFunction DeclareFunction instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.DeclareFunction declareFunction);

  /**
   * Visit the given {@link SyntaxCommand.GetInfo}.
   *
   * @param getInfo GetInfo instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.GetInfo getInfo);

  /**
   * Visit the given {@link SyntaxCommand.SetOption}.
   *
   * @param setOption SetOption instance to be visited
   * @return an object of type T
   */
  T visit(SyntaxCommand.SetOption setOption);
}
