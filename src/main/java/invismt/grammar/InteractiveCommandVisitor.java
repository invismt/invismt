package invismt.grammar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import invismt.grammar.commands.SyntaxCommand;
import invismt.parser.SMTLIBProblemBuilder;
import invismt.parser.ScriptParserDispatcher;
import invismt.parser.exceptions.SMTLIBParsingException;
import invismt.tree.ListOfProofObligation;
import invismt.tree.ProofObligation;

public class InteractiveCommandVisitor extends ExpressionBaseVisitor<Void> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InteractiveCommandVisitor.class);
	private BufferedReader reader;
	private List<Expression> interactiveScript;
	private StringBuilder tempBuffer;
	private ScriptParserDispatcher parser;
	private Runnable onExit;
	
	// flags
	private boolean printSuccess = false;
	
	private boolean hasNext = false;
	
	public InteractiveCommandVisitor(Runnable onExit) {
			this.reader = new BufferedReader(new InputStreamReader(System.in));
			this.interactiveScript = new ArrayList<>();
			this.parser = new ScriptParserDispatcher();
			this.onExit = onExit;
	}

	public ListOfProofObligation nextObligation(ProofObligation lastObligation) {
		
		// print result of last obligation if possible
		if (lastObligation != null) {
			System.out.println(lastObligation.getProofTree().getRootPTElement().getSatisfiable().toSimpleString() + "\n");
		}
		
		while (!hasNext) {
			String line = null;
			try {
				line = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line == null) return null;
			LOGGER.info(line, "Read line: {0}");
			
			// create new tempBuffer
			if (this.tempBuffer == null) {
				this.tempBuffer = new StringBuilder();
			}
			// append line to tempBuffer
			this.tempBuffer.append(line + "\n");
			// try to parse
			List<Expression> exprs;
			try {
				exprs = parser.parseInto(this.tempBuffer.toString());
			} catch (SMTLIBParsingException parsingE) {
				// optimistically assume we are just missing part of input
				// do not throw away buffer but continue writing to it
				continue;
			}
			// successful parsed
			// throw away buffer
			this.tempBuffer = null;
			
			for (Expression expr : exprs) {
				if (this.printSuccess) System.out.println("success");
				expr.accept(this);
			}
		}
		this.hasNext = false;
		return this.getLastObligation();
	}

	private ListOfProofObligation getLastObligation() {
		// return the last proof obligation that we can build
		ListOfProofObligation next = (new SMTLIBProblemBuilder()).buildProblems(this.interactiveScript);
		return new ListOfProofObligation(List.of(next.getProofObligations().get(next.getProofObligations().size()-1)));
	}
	
	@Override
	public Void visit(SyntaxCommand.Assert assertC) {
		this.interactiveScript.add(assertC);
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.DeclareConstant decconst) {
		this.interactiveScript.add(decconst);
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.DeclareFunction decfun) {
		this.interactiveScript.add(decfun);
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.Pop pop) {
		this.interactiveScript.add(pop);
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.Push push) {
		this.interactiveScript.add(push);
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.Reset reset) {
		this.interactiveScript.add(reset);
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.CheckSat checksat) {
		this.interactiveScript.add(checksat);
		this.hasNext = true;
		return null;
	}
	
	@Override
	public Void visit(SyntaxCommand.Exit exit) {
		this.onExit.run();
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.GetInfo getInfo) {
		if (getInfo.getParameter().equals(ExpressionFactory.VERSION)) {
			System.out.println("(:version \"4.8.13\")");
			return null;
		}
		if (getInfo.getParameter().equals(ExpressionFactory.NAME)) {
			System.out.println("(:name \"Z3\")");
			return null;
		}
		return null;
	}

	@Override
	public Void visit(SyntaxCommand.SetOption setOption) {
		if (setOption.getOption().equals(ExpressionFactory.PRINTSUCCESS)) {
			this.printSuccess = true;
		}
		return null;
	}

	@Override
	public Void visit(GeneralExpression expr) {
		this.interactiveScript.add(expr);
		return null;
	}
}
