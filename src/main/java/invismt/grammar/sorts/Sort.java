/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar.sorts;

import com.google.common.base.Objects;
import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.identifiers.Identifier;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * A sort in the SMT-LIB standard version 2.6.
 * In that version, a sort can be derived into either
 * "identifier" or "( identifier sort+ )" where identifier
 * is a {@link Identifier} and sort is another sort.
 */
public class Sort implements Expression {

	private final Identifier sortName;
	private final List<Sort> sorts;

	/**
	 * Create a new sort with the token representing the identifier's
	 * name as well as a possibly empty list of indices.
	 * If the list is empty, the sort is derived into "identifier".
	 * If it is not empty, the sort is derived into "( identifier sorts+ )".
	 *
	 * @param sortName {@link Identifier} that represents the name of this sort
	 * @param sorts    list of {@link Sort sorts} this sort is derived into, may be empty
	 */
	public Sort(Identifier sortName, List<Sort> sorts) {
		this.sortName = sortName;
		this.sorts = new ArrayList<>(sorts);
	}

	/**
	 * Returns the {@link #sortName} identically.
	 *
	 * @return the {@link #sortName} of this term
	 */
	public Identifier getSortIdentifier() {
		return sortName;
	}

	/**
	 * Returns a new list of the identical {@link #sorts} of this sort.
	 * This also creates and returns a new empty list if {@link #sorts} is empty.
	 *
	 * @return a new list of the identical {@link #sorts} of this identifier
	 */
	public List<Sort> getSorts() {
		return new ArrayList<>(sorts);
	}

	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<>(1);
		children.add(sortName);
		if (sorts.isEmpty()) {
			return children;
		}
		children.addAll(sorts);
		return children;
	}

	@Override
	public Sort copy() {
		return new Sort(sortName, sorts);
	}

	@Override
	public Sort deepCopy() {
		List<Sort> newSorts = new ArrayList<>();
		for (Sort sort : sorts) {
			newSorts.add(sort.deepCopy());
		}
		return new Sort(sortName.deepCopy(), newSorts);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(sortName, sorts);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!other.getClass().equals(this.getClass())) {
			return false;
		}
		Sort o = (Sort) other;
		return o.getChildren().equals(this.getChildren());
	}

	@Override
	public String toString() {
		StringJoiner joiner;
		if (sorts.isEmpty()) {
			joiner = new StringJoiner(SPACE);
		} else {
			joiner = new StringJoiner(SPACE, PAR_OPEN, PAR_CLOSED);
		}
		joiner.add(sortName.toString());
		for (Sort sort : sorts) {
			joiner.add(sort.toString());
		}
		return joiner.toString();
	}

}
