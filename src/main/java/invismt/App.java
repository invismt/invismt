/*
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 */
package invismt;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.eventbus.EventBus;
import invismt.controller.MasterController;
import invismt.events.CleanUpEvent;
import invismt.grammar.InteractiveCommandVisitor;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class of InViSMT.
 */
public class App extends Application {

	private static final EventBus EVENTBUS = new EventBus();
	private static Stage primaryStage;

	private static final AppArgs arguments = new AppArgs();
	
	private InteractiveCommandVisitor commandVisitor;

	/**
	 * Returns the current build version, specified in the JAR manifest.
	 */
	public static String getBuildVersion() {
		return App.class.getPackage().getImplementationVersion();
	}

	/**
	 * Returns the {@link EventBus} for this application.
	 *
	 * @return EventBus for this application.
	 */
	public static EventBus getEventBus() {
		return EVENTBUS;
	}

	/**
	 * Entry point of InViSMT.
	 *
	 * @param args array of commandline arguments
	 */
	public static void main(String[] args) {
		JCommander.newBuilder().addObject(arguments).build().parse(args);
		launch(args);
	}

	/**
	 * Returns the primary stage thus the top level JavaFX container of the
	 * application.
	 *
	 * @return the applications primary stage.
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	@Override
	public void start(Stage stage) throws Exception {
		if (arguments.version) {
			System.out.println("Z3 version 0.0.0 - 64 bit (actually InViSMT " + getBuildVersion() + ")");
			System.exit(0);
		}

		if (arguments.help) {
			JCommander.newBuilder().addObject(arguments).build().usage();
			System.exit(0);
		}

		if (arguments.files.size() > 1) {
			System.err.println("Wrong number of parameters");
			System.exit(1);
		}

		if (arguments.files.size() == 1 && arguments.files.get(0).equals("-")) {
			arguments.readFromStdin = true;
		}

		primaryStage = stage;
		primaryStage.setMaximized(true);
		primaryStage.setTitle("InViSMT");

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MasterView.fxml"));
		Parent root = fxmlLoader.load();

		MasterController mc = fxmlLoader.getController();

		
		if (arguments.readFromStdin) {
			this.commandVisitor = new InteractiveCommandVisitor(this::onExit);
			mc.setInteractive(this.commandVisitor::nextObligation);
		} else if (!arguments.files.isEmpty()) {
			mc.openFile(new File(arguments.files.get(0)));
		}

		primaryStage.setScene(new Scene(root));
		primaryStage.setOnCloseRequest(event -> onExit());
		primaryStage.show();
	}
	
	private void onExit() {
		EVENTBUS.post(new CleanUpEvent());
		Platform.exit();
		System.exit(0);
	}

	public static class AppArgs {
		@Parameter
		private List<String> files = new ArrayList<>(4);

		@Parameter(names = { "-log", "-verbose" }, description = "Level of verbosity")
		private Integer verbose = 1;

		@Parameter(names = { "--in", "-in" }, description = "Read from stdin")
		private boolean readFromStdin;

		@Parameter(names = "--debug", description = "Debug mode")
		private boolean debug = false;

		@Parameter(names = { "--version", "-version" }, description = "Show version information")
		private boolean version = false;

		@Parameter(names = "--help", help = true)
		private boolean help;
	}
}