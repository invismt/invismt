/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser.exceptions;

import org.antlr.v4.runtime.Token;

/**
 * Is the Class for Parsing Exception
 */
@SuppressWarnings("serial")
public class SMTLIBParsingException extends IllegalArgumentException {

	/**
	 * Parsing Exception
	 *
	 * @param message is the message for the Exception
	 */
	public SMTLIBParsingException(String message) {
		super(message);
	}

	/**
	 * Exception if the File can not be parsed
	 *
	 * @param token is Token of Failure
	 * @return SMTLIBParsingException with message pos of Token and Text
	 */
	public static SMTLIBParsingException couldNotParse(Token token) {
		return new SMTLIBParsingException(position(token) + " could not be parsed.");
	}

	/**
	 * Exception for unexpected Exception
	 *
	 * @param token is Token of Failure
	 * @return SMTLIBParsingException with message pos of Token and Text
	 */
	public static SMTLIBParsingException notExpected(Token token) {
		return new SMTLIBParsingException(position(token) + " was not expected.");
	}

	private static String position(Token token) {
		return "\"" + token.getText() + "\"" + " in line " + token.getLine() + " at position " + token
				.getCharPositionInLine();
	}

}
