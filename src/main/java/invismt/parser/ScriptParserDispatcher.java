/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.Expression;
import invismt.parser.antlr.SMTLIBv2Lexer;
import invismt.parser.antlr.SMTLIBv2Parser;
import invismt.parser.antlr.SMTLIBv2Parser.ScriptContext;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements {@link ParserDispatcher} to parse a string to a list of {@link Expression}.
 *
 * @author Kaan Berk Yaman
 */
public class ScriptParserDispatcher implements ParserDispatcher<List<Expression>> {

	private final SMTLIBv2Parser toDispatch;

	/**
	 * Creates a new ScriptParserDispatcher.
	 */
	public ScriptParserDispatcher() {
		this.toDispatch = new SMTLIBv2Parser(null);
		toDispatch.setErrorHandler(new SMTLIBDescriptiveBailErrorStrategy());
		toDispatch.removeErrorListeners();
	}

	@Override
	public List<Expression> parseInto(String content) {
		ANTLRInputStream contentStream = new ANTLRInputStream(content);
		SMTLIBv2Lexer lexer = new SMTLIBv2Lexer(contentStream);
		CommonTokenStream contentTokens = new CommonTokenStream(lexer);
		toDispatch.setTokenStream(contentTokens);
		List<Expression> parsedCommands = new ArrayList<>();
		ScriptContext root = toDispatch.script();
		// Feed logger entire parse tree before converting to AST
		SMTLIBExpressionProductionVisitor visitor = new SMTLIBExpressionProductionVisitor();
		for (int i = 0; i < root.getChildCount() - 1; i++) {
			parsedCommands.add(root.getChild(i).accept(visitor));
			// script -> command* EOF is deterministic
		}
		return parsedCommands; // Return list of parsed commands
	}

}
