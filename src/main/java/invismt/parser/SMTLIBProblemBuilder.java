/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionVisitor;
import invismt.grammar.GeneralExpression;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant.*;
import invismt.grammar.Token.*;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue.ConstantAttributeValue;
import invismt.grammar.attributes.AttributeValue.ListAttributeValue;
import invismt.grammar.attributes.AttributeValue.SymbolAttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.*;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index.NumeralIndex;
import invismt.grammar.identifiers.Index.SymbolIndex;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;
import invismt.grammar.term.QuantifiedTerm.ExistsTerm;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;
import invismt.parser.exceptions.SMTLIBParsingException;
import invismt.tree.ListOfProofObligation;
import invismt.tree.ProofObligation;

import java.util.*;

/**
 * Creates a {@link ListOfProofObligation} from a list of {@link Expression}'s.
 *
 * @author Kaan Berk Yaman
 * @see #buildProblems(List)
 */
public class SMTLIBProblemBuilder implements ExpressionVisitor<Boolean> {

  private static final String INVALIDVISIT = "ProblemBuilder can only visit first-level expressions!";
  private final LinkedList<ProofObligation> problems;
  private final Deque<Deque<Expression>> currentContext;

  /**
   * Creates a new SMTLIBProblemBuilder.
   */
  public SMTLIBProblemBuilder() {
    problems = new LinkedList<>();
    var initialContext = new LinkedList<Deque<Expression>>();
    initialContext.add(new LinkedList<>()); // Add assertion level 1
    currentContext = initialContext;
  }

  /**
   * Builds one or multiple SMT problems that are represented by the
   * {@link invismt.tree.PTElement} class and encapsulated in the
   * {@link ProofObligation} class. The problems are built out of a list of
   * {@link Expression} objects, splitting the list up into different problems
   * according to the SMT-LIB Version 2.6 commands push, pop, check-sat, reset and
   * exit.
   * <p>
   * If one of the created problems would have an empty {@link Script} without
   * even a check-sat command it is not returned in the list of ProofObligations
   * that is created.
   *
   * @param commands list of expressions out of which the proof obligations are
   *                 built
   * @return list of ProofObligation objects
   */
  public ListOfProofObligation buildProblems(List<Expression> commands) {
    for (Expression exp : commands) {
      if (!Boolean.TRUE.equals(exp.accept(this))) {
        break;
      }
    }
    if (problems.isEmpty()) {
      throw new SMTLIBParsingException(
                                       "File contains no (check-sat) expressions!" + " " + "No ProofObligations could have been generated.");
    }
    return new ListOfProofObligation(problems);
  }

  @Override
  public Boolean visit(GeneralExpression expression) {
    if (expression.isGlobal()) {
      currentContext.getLast().addLast(expression);
    } else {
      currentContext.peek().addLast(expression);
    }
    return true;
  }

  @Override
  public Boolean visit(ForAllTerm forallTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(ExistsTerm existsTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(LetTerm letTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(AnnotationTerm annotationTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(MatchTerm matchTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(ConstantTerm constantTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(IdentifierTerm identifierTerm) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(VariableBinding variableBinding) {
    throw new SMTLIBParsingException(INVALIDVISIT);

  }

  @Override
  public Boolean visit(SortedVariable sortedVariable) {
    throw new SMTLIBParsingException(INVALIDVISIT);

  }

  @Override
  public Boolean visit(Identifier identifier) {
    throw new SMTLIBParsingException(INVALIDVISIT);

  }

  @Override
  public Boolean visit(NumeralIndex index) {
    throw new SMTLIBParsingException(INVALIDVISIT);

  }

  @Override
  public Boolean visit(SymbolIndex index) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(QualIdentifier qualIdentifier) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(ListAttributeValue value) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(ConstantAttributeValue value) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(SymbolAttributeValue value) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(Attribute attribute) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(NumeralToken token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(StringToken token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(BinaryToken token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(DecimalToken token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(HexadecimalToken token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(SymbolToken token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(ReservedWord token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(Keyword token) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(NumeralConstant constant) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(StringConstant constant) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(DecimalConstant constant) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(HexadecimalConstant constant) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(BinaryConstant constant) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(Sort sort) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(Pattern pattern) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(MatchCase matchCase) {
    throw new SMTLIBParsingException(INVALIDVISIT);
  }

  @Override
  public Boolean visit(Assert assertion) {
    currentContext.peek().addLast(assertion);
    return true;
  }

  @Override
  public Boolean visit(DeclareConstant declaration) {
    currentContext.peek().addLast(declaration);
    return true;
  }

  @Override
  public Boolean visit(CheckSat checkSat) {
    var allExpressions = new ArrayList<Expression>();
    // Check if Lists.reverse(...) is actually required
    for (Iterator<Deque<Expression>> iter = currentContext.descendingIterator(); iter.hasNext();) {
      allExpressions.addAll(iter.next());
    }
    var problemContent = new Script(allExpressions);
    if (!problemContent.getTopLevelExpressions().isEmpty()) {
      problems.add(new ProofObligation(problemContent));
    }
    return true;
  }

  @Override
  public Boolean visit(Pop pop) {
    for (int i = 0; i < pop.getMagnitude().getInteger().intValue(); i++) {
      // See comment in next method
      if (currentContext.size() == 1) {
        // Don't let pop remove the 0th assertion level
        break;
      }
      currentContext.pop();
    }
    return true;
  }

  @Override
  public Boolean visit(Push push) {
    for (int i = 0; i < push.getMagnitude().getInteger().intValue(); i++) {
      // Kinda insecure, casting a BigInteger into an int value might cause trouble
      currentContext.push(new LinkedList<>());
    }
    return true;
  }

  @Override
  public Boolean visit(Exit exit) {
    return false;
  }

  @Override
  public Boolean visit(Reset reset) {
    currentContext.clear();
    currentContext.add(new LinkedList<>());
    return true;
  }

  @Override
  public Boolean visit(SyntaxCommand.DeclareFunction declareFunction) {
    currentContext.peek().addLast(declareFunction);
    return true;
  }

  @Override
  public Boolean visit(SyntaxCommand.GetInfo getInfo) {
    // Add GetInfo as a global expression
    currentContext.getLast().addLast(getInfo);
    return true;
  }

  @Override
  public Boolean visit(SetOption setOption) {
    currentContext.getLast().addLast(setOption);
    return true;
  }
}
