/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.term.Term;
import invismt.parser.antlr.SMTLIBv2Lexer;
import invismt.parser.antlr.SMTLIBv2Parser;
import invismt.parser.antlr.SMTLIBv2Parser.Single_termContext;
import invismt.parser.exceptions.SMTLIBParsingException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 * Parses a string into a {@link Term}.
 *
 * @author Kaan Berk Yaman
 */
public class TermParserDispatcher implements ParserDispatcher<Term> {

	private final SMTLIBv2Parser toDispatch;

	/**
	 * Creates a new TermPraserDispatcher.
	 */
	public TermParserDispatcher() {
		this.toDispatch = new SMTLIBv2Parser(null);
		toDispatch.setErrorHandler(new SMTLIBDescriptiveBailErrorStrategy());
		// Ensure that the parsing process is cancelled if ANTLR encounters an error
	}

	@Override
	public Term parseInto(String content) {
		ANTLRInputStream contentStream = new ANTLRInputStream(content);
		SMTLIBv2Lexer lexer = new SMTLIBv2Lexer(contentStream);
		CommonTokenStream contentTokens = new CommonTokenStream(lexer);
		toDispatch.setTokenStream(contentTokens);
		Single_termContext root;
		try {
			root = toDispatch.single_term();
		} catch (SMTLIBParsingException e) {
			throw new SMTLIBParsingException(content + " is not a valid term.");
		}
		SMTLIBExpressionProductionVisitor visitor = new SMTLIBExpressionProductionVisitor();
		return (Term) root.getChild(0).accept(visitor);
	}
}
