/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;
import invismt.grammar.Token.*;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;
import invismt.parser.antlr.SMTLIBv2BaseVisitor;
import invismt.parser.antlr.SMTLIBv2Parser.*;
import invismt.parser.exceptions.SMTLIBParsingException;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Creates elements from {@link invismt.grammar} based on the given context from
 * {@link invismt.parser.antlr.SMTLIBv2Parser}.
 *
 * @author uynuq/Kaan Berk Yaman
 */
public class SMTLIBExpressionProductionVisitor extends SMTLIBv2BaseVisitor<Expression> {

  private static final String PAROPEN = "(";
  private static final String PARCLOSED = ")";

  private GeneralExpression convertToGeneralExpression(ParserRuleContext ctx, boolean isGlobal, boolean brackets) {
    StringBuilder str = new StringBuilder();
    String spaces = ctx.start.getInputStream()
      .getText(new Interval(ctx.start.getStartIndex(), ctx.stop.getStopIndex()));
    str.append(spaces);
    if (brackets) {
      str.insert(0, PAROPEN);
      str.append(PARCLOSED);
    }
    return new GeneralExpression(str.toString(), isGlobal);
  }

  @Override
  public Expression visitGeneralReservedWord(GeneralReservedWordContext ctx) {
    // generalReservedWord -> GRW_* is deterministic
    return ctx.getChild(0).accept(this);
  }

  @Override
  public Expression visitSimpleSymbol(SimpleSymbolContext ctx) {
    return new Token.Keyword(ctx.getText());
  }

  @Override
  public Expression visitQuotedSymbol(QuotedSymbolContext ctx) {
    return new Token.SymbolToken(ctx.getText());
    // quotedSymbol directly matches to SymbolToken
  }

  @Override
  public Expression visitSymbol(SymbolContext ctx) {
    return new Token.SymbolToken(ctx.getText());
    // symbol matches both simpleSymbol and quotedSymbol (which directly match with
    // predefinedSymbol and UndefinedSymbol, so no need to invoke a further rule)
  }

  @Override
  public Expression visitNumeral(NumeralContext ctx) {
    return new Token.NumeralToken(ctx.Numeral().getText());
    // numeral directly matches to NumeralToken
  }

  @Override
  public Expression visitDecimal(DecimalContext ctx) {
    return new Token.DecimalToken(ctx.Decimal().getText());
    // decimal directly matches to DecimalToken
  }

  @Override
  public Expression visitHexadecimal(HexadecimalContext ctx) {
    return new Token.HexadecimalToken(ctx.HexDecimal().getText());
    // hexadecimal directly matches to HexademicalToken
  }

  @Override
  public Expression visitBinary(BinaryContext ctx) {
    return new Token.BinaryToken(ctx.Binary().getText());
    // binary directly matches to BinaryToken
  }

  @Override
  public Expression visitString(StringContext ctx) {
    return new Token.StringToken(ctx.String().getText());
    // string directly matches to StringToken
  }

  @Override
  public Expression visitKeyword(KeywordContext ctx) {
    return new Token.Keyword(ctx.getText());
    // Both predefinedKeyword and UndefinedKeyword match to Keyword
  }

  @Override
  public Expression visitInfo_flag(Info_flagContext ctx) {
    return new Token.Keyword(ctx.getText());
    // Treat info flags like keywords
  }

  @Override
  public Expression visitSpec_constant(Spec_constantContext ctx) {
    // TODO Replace switch expression with some arbitary Visitor structure
    if (ctx.getChildCount() == 1) {
      if (ctx.numeral() != null) {
        return new SpecConstant.NumeralConstant((NumeralToken) ctx.numeral().accept(this));
      } else if (ctx.string() != null) {
        return new SpecConstant.StringConstant((StringToken) ctx.string().accept(this));
      } else if (ctx.hexadecimal() != null) {
        return new SpecConstant.HexadecimalConstant((HexadecimalToken) ctx.hexadecimal().accept(this));
      } else if (ctx.binary() != null) {
        return new SpecConstant.BinaryConstant((BinaryToken) ctx.binary().accept(this));
      } else if (ctx.decimal() != null) {
        return new SpecConstant.DecimalConstant((DecimalToken) ctx.decimal().accept(this));
      }
    }
    return null;
    // TODO Decide what to do if parser wonks
  }

  @Override
  public Expression visitS_expr(S_exprContext ctx) {
    return convertToGeneralExpression(ctx, false, false);
  }

  @Override
  public Expression visitIndex(IndexContext ctx) {
    // TODO Replace switch expression with some arbitrary Visitor structure
    if (ctx.getChildCount() == 1) {
      if (ctx.numeral() != null) {
        return new Index.NumeralIndex((NumeralToken) ctx.numeral().accept(this));
      } else if (ctx.symbol() != null) {
        return new Index.SymbolIndex((SymbolToken) ctx.symbol().accept(this));
      }
    }
    return null;
    // Deterministic rule, this line should have zero coverage
  }

  @Override
  public Expression visitIdentifier(IdentifierContext ctx) {
    if (ctx.getChildCount() == 1) {
      return new Identifier((SymbolToken) ctx.symbol().accept(this), List.of());
    } else {
      // Collect further sorts in a list and call this visitor recursively on
      // individual sort nodes
      List<Index> index = new ArrayList<>();
      for (IndexContext e : ctx.index()) {
        index.add((Index) e.accept(this));
      }
      return new Identifier((SymbolToken) ctx.symbol().accept(this), index);
    }
    // TODO Implement error handling
  }

  @Override
  public Expression visitAttribute_value(Attribute_valueContext ctx) {
    if (ctx.getChildCount() == 1) {
      if (ctx.spec_constant() != null) {
        return new AttributeValue.ConstantAttributeValue((SpecConstant) ctx.spec_constant().accept(this));
      } else if (ctx.symbol() != null) {
        return new AttributeValue.SymbolAttributeValue((SymbolToken) ctx.symbol().accept(this));
      }
      return null;
    } else {
      // Deterministic rule: attribute_value -> ParOpen s_expr* ParClose
      List<Expression> sexprs = new ArrayList<>();
      for (S_exprContext sexpr : ctx.s_expr()) {
        sexprs.add(sexpr.accept(this));
      }
      return new AttributeValue.ListAttributeValue(sexprs);
    }
  }

  @Override
  public Expression visitAttribute(AttributeContext ctx) {
    if (ctx.getChildCount() == 1) {
      return new Attribute((Keyword) ctx.keyword().accept(this), Optional.empty());
    } else {
      return new Attribute((Keyword) ctx.keyword().accept(this),
                           Optional.of((AttributeValue) ctx.attribute_value().accept(this)));
    }
  }

  // TODO Decide how the parser should handle unsupported expressions
  @Override
  public Expression visitSort(SortContext ctx) {
    if (ctx.getChildCount() == 1) {
      return new Sort((Identifier) ctx.identifier().accept(this), List.of());
    } else {
      // Call recursively and collect
      List<Sort> sorts = new ArrayList<>();
      for (SortContext sort : ctx.sort()) {
        sorts.add((Sort) sort.accept(this));
      }
      return new Sort((Identifier) ctx.identifier().accept(this), sorts);
    }
  }

  @Override
  public Expression visitQual_identifier(Qual_identifierContext ctx) {
    // Positions chosen according to grammar
    if (ctx.getChildCount() == 1) {
      return new QualIdentifier((Identifier) ctx.identifier().accept(this), Optional.empty());
    } else {
      return new QualIdentifier((Identifier) ctx.identifier().accept(this),
                                Optional.of((Sort) ctx.sort().accept(this)));
    }
  }

  @Override
  public Expression visitVar_binding(Var_bindingContext ctx) {
    return new VariableBinding((SymbolToken) ctx.symbol().accept(this), (Term) ctx.term().accept(this));
  }

  @Override
  public Expression visitSorted_var(Sorted_varContext ctx) {
    return new SortedVariable((SymbolToken) ctx.symbol().accept(this), (Sort) ctx.sort().accept(this));
  }

  @Override
  public Expression visitPattern(PatternContext ctx) {
    List<SymbolToken> symbols = new ArrayList<>();
    if (ctx.getChildCount() == 1) {
      return new Pattern((SymbolToken) ctx.getChild(0).accept(this), symbols);
    } else {
      for (int i = 2; i < ctx.getChildCount() - 1; i++) {
        symbols.add((SymbolToken) ctx.getChild(i).accept(this));
      }
      return new Pattern((SymbolToken) ctx.getChild(1).accept(this), symbols);
    }
  }

  @Override
  public Expression visitMatch_case(Match_caseContext ctx) {
    return new MatchCase((Pattern) ctx.pattern().accept(this), (Term) ctx.term().accept(this));
  }

  // TODO Implement visiting of bind nodes
  @Override
  public Expression visitTerm(TermContext ctx) {
    // TODO Replace switch expression with something more OOP-friendly
    if (ctx.getChildCount() == 1) {
      if (ctx.spec_constant() != null) {
        return new ConstantTerm((SpecConstant) ctx.spec_constant().accept(this));
      } else if (ctx.qual_identifier() != null) {
        return new IdentifierTerm((QualIdentifier) ctx.qual_identifier().accept(this), List.of());
      }
    } else {
      // Deterministic rule: term -> ParOpen *_term ParClose
      return ctx.getChild(1).accept(this);
    }
    // Should have zero coverage
    return null;
  }

  @Override
  public Expression visitSingle_term(Single_termContext ctx) {
    return ctx.getChild(0).accept(this);
    // TODO Remove this unused method
  }

  @Override
  public Expression visitQual_term(Qual_termContext ctx) {
    // Call recursively and collect
    List<Term> terms = new ArrayList<>();
    for (TermContext term : ctx.term()) {
      terms.add((Term) term.accept(this));
    }
    return new IdentifierTerm((QualIdentifier) ctx.qual_identifier().accept(this), terms);
  }

  @Override
  public Expression visitLet_term(Let_termContext ctx) {
    List<VariableBinding> binds = new ArrayList<>();
    for (Var_bindingContext bind : ctx.var_binding()) {
      binds.add((VariableBinding) bind.accept(this));
    }
    return new LetTerm(binds, (Term) ctx.term().accept(this));
  }

  @Override
  public Expression visitForall_term(Forall_termContext ctx) {
    List<SortedVariable> svars = new ArrayList<>();
    for (Sorted_varContext svar : ctx.sorted_var()) {
      svars.add((SortedVariable) svar.accept(this));
    }
    return new QuantifiedTerm.ForAllTerm(svars, (Term) ctx.term().accept(this));
  }

  @Override
  public Expression visitExists_term(Exists_termContext ctx) {
    List<SortedVariable> svars = new ArrayList<>();
    for (Sorted_varContext svar : ctx.sorted_var()) {
      svars.add((SortedVariable) svar.accept(this));
    }
    return new QuantifiedTerm.ExistsTerm(svars, (Term) ctx.term().accept(this));
  }

  @Override
  public Expression visitMatch_term(Match_termContext ctx) {
    List<MatchCase> matches = new ArrayList<>();
    for (Match_caseContext match : ctx.match_case()) {
      matches.add((MatchCase) match.accept(this));
    }
    return new MatchTerm((Term) ctx.term().accept(this), matches);
  }

  @Override
  public Expression visitExclamation_term(Exclamation_termContext ctx) {
    List<Attribute> attributes = new ArrayList<>();
    for (AttributeContext attribute : ctx.attribute()) {
      attributes.add((Attribute) attribute.accept(this));
    }
    return new AnnotationTerm((Term) ctx.term().accept(this), attributes);
  }

  @Override
  public Expression visitScript(ScriptContext ctx) {
    // Ensure ANTLRExpressionProductionVisitor never visits a ScriptContext
    throw new SMTLIBParsingException(
                                     "SMTLIBExpressionProductionVisitor tried to visit a ScriptContext! Parsing failed.");
  }

  @Override
  public Expression visitCmd_declareConst(Cmd_declareConstContext ctx) {
    return new SyntaxCommand.DeclareConstant((SymbolToken) ctx.symbol().accept(this), (Sort) ctx.sort().accept(this));
  }

  @Override
  public Expression visitCmd_declareDatatype(Cmd_declareDatatypeContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_declareDatatypes(Cmd_declareDatatypesContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_declareSort(Cmd_declareSortContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_defineFun(Cmd_defineFunContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_defineFunRec(Cmd_defineFunRecContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_defineFunsRec(Cmd_defineFunsRecContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_defineSort(Cmd_defineSortContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  // TODO Implement visiting of individual command types
  @Override
  public Expression visitCmd_assert(Cmd_assertContext ctx) {
    return new SyntaxCommand.Assert((Term) ctx.term().accept(this));
  }

  @Override
  public Expression visitCmd_checkSat(Cmd_checkSatContext ctx) {
    return new SyntaxCommand.CheckSat();
  }

  @Override
  public Expression visitCmd_checkSatAssuming(Cmd_checkSatAssumingContext ctx) {
    return new SyntaxCommand.CheckSat();
    // TODO Extend CheckSat to support assumptions (see issue #15)
  }

  @Override
  public Expression visitCmd_declareFun(Cmd_declareFunContext ctx) {
    List<Sort> definitionArea = new ArrayList<>();
    for (SortContext sortCtx : ctx.sort()) {
      definitionArea.add((Sort) sortCtx.accept(this));
    }
    Sort targetArea = definitionArea.get(definitionArea.size() - 1);
    definitionArea.remove(definitionArea.size() - 1);
    return new SyntaxCommand.DeclareFunction((SymbolToken) ctx.symbol().accept(this), definitionArea, targetArea);
  }

  @Override
  public Expression visitCmd_echo(Cmd_echoContext ctx) {
    return convertToGeneralExpression(ctx, false, true);
  }

  @Override
  public Expression visitCmd_exit(Cmd_exitContext ctx) {
    return new SyntaxCommand.Exit();
  }

  @Override
  public Expression visitCmd_pop(Cmd_popContext ctx) {
    return new SyntaxCommand.Pop((NumeralToken) ctx.numeral().accept(this));
  }

  @Override
  public Expression visitCmd_push(Cmd_pushContext ctx) {
    return new SyntaxCommand.Push((NumeralToken) ctx.numeral().accept(this));
  }

  @Override
  public Expression visitCmd_reset(Cmd_resetContext ctx) {
    return new SyntaxCommand.Reset();
  }

  @Override
  public Expression visitCmd_setInfo(Cmd_setInfoContext ctx) {
    return convertToGeneralExpression(ctx, true, true);
  }

  @Override
  public Expression visitCmd_getInfo(Cmd_getInfoContext ctx) {
    return new SyntaxCommand.GetInfo((Keyword) ctx.info_flag().accept(this));
  }

  @Override
  public Expression visitCmd_setOption(Cmd_setOptionContext ctx) {
    return new SyntaxCommand.SetOption((Attribute) ctx.attribute().accept(this));
  }

  @Override
  public Expression visitCmd_getOption(Cmd_getOptionContext ctx) {
    return convertToGeneralExpression(ctx, true, true);
  }

  @Override
  public Expression visitCmd_setLogic(Cmd_setLogicContext ctx) {
    return convertToGeneralExpression(ctx, true, true);
  }

  @Override
  public Expression visitCmd_getProof(Cmd_getProofContext ctx) {
    return convertToGeneralExpression(ctx, true, true);
  }

  @Override
  public Expression visitCmd_getModel(Cmd_getModelContext ctx) {
    return convertToGeneralExpression(ctx, true, true);
  }

  @Override
  public Expression visitCommand(CommandContext ctx) {
    // Position 1 always contains command node: ParOpen cmd_* ParClose
    return ctx.getChild(1).accept(this);
  }
}
