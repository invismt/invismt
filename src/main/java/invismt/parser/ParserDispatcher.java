/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

/**
 * This interface provides a method to parse a String into another Object.
 *
 * @param <T>
 * @author Kaan Berk Yaman
 */
public interface ParserDispatcher<T> {

	/**
	 * Parses the content into another object.
	 *
	 * @param content is the content of the other object
	 * @return the parsed object
	 */
	T parseInto(String content);
}
