/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.exception;

/**
 * This exception signals that an operation could not be completed due to missing user input or user abort.
 *
 * @author Tim Junginger
 */
@SuppressWarnings("serial")
public class MissingUserInputException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with null as its detail message. The cause is not initialized,
	 * and may subsequently be initialized by a call to initCause.
	 */
	public MissingUserInputException() {
		super();
	}

	/**
	 * @param message - the detail message. The detail message is saved for later retrieval by the getMessage() method
	 */
	public MissingUserInputException(String message) {
		super(message);
	}

}
