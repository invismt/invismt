package invismt.tree.exception;

import invismt.tree.ProofState;

/**
 * An exception signalling conflicting {@link ProofState} proof states.
 */
public class ConflictingStateException extends Exception {

	/**
	 * Calls {@link Exception#Exception()} whose message contains the new
	 * and the old proof state that are conflicting.
	 *
	 * @param newState the new {@link ProofState}
	 * @param oldState the old {@link ProofState}
	 */
	public ConflictingStateException(ProofState newState, ProofState oldState) {
		super("Proof states " + oldState + " and " + newState + " are conflicting.");
	}

}
