/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import invismt.grammar.Script;
import invismt.rule.Rule;
import invismt.solver.SolverAnswer;
import invismt.solver.SolverService;
import invismt.tree.exception.ConflictingStateException;
import invismt.tree.exception.PTElementLocked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * PTElement is a node in the ProofTree. This node is used to represent a proof
 * state which consists of a single set (list) of assertions and the
 * {@link ProofState} of this proof state.
 *
 * @see ProofTree
 * @see Script
 */
public class PTElement {

	private static final Logger LOGGER = LoggerFactory.getLogger(PTElement.class);

	private final List<PTElement> children;
	private final Rule rule;
	private final List<PropertyChangeListener> listeners = new ArrayList<>();
	private boolean locked;
	private PTElement parent;
	private ProofState proofState;
	private SolverService solverService;
	private Script script;
	private ProofTree proofTree;

	/**
	 * Creates a node with the given parameters.
	 *
	 * @param parent    Parent for the node (set to null if root)
	 * @param children  list of children (set to empty list if not Children exists)
	 * @param script    holds the Script for the node
	 * @param rule      holds the Rule which created the node
	 * @param proofTree is the ProofTree in which the PTElement is a node
	 */
	public PTElement(PTElement parent, List<PTElement> children, Script script, Rule rule, ProofTree proofTree) {
		this.script = script;
		this.parent = parent;
		this.children = children;
		for (PTElement child : children) {
			child.setParent(this);
		}
		this.rule = rule;
		locked = false;
		proofState = ProofState.UNKNOWN;
		solverService = null;
		this.proofTree = proofTree;
	}

	/**
	 * Creates a node with no children.
	 *
	 * @param parent    Parent for the node (set to null if root)
	 * @param script    Script for the node
	 * @param rule      Rule which created the node
	 * @param proofTree ProofTree this element is a part of if any
	 */
	public PTElement(PTElement parent, Script script, Rule rule, ProofTree proofTree) {
		this(parent, new ArrayList<>(), script, rule, proofTree);
	}

	/**
	 * Notifies all listeners.
	 */
	public void notifyListeners() {
		for (PropertyChangeListener listener : listeners) {
			listener.propertyChange(new PropertyChangeEvent(this, PTElement.class.getName(), null, this));
		}
	}

	/**
	 * Sets the {@link ProofState proof state} of this PTElement according
	 * to its children's proof states and the rule(s) the children were created with,
	 * if there are children.
	 *
	 * @throws PTElementLocked if the PTElement is currently locked and hence the proof
	 *                         state cannot be changed
	 * @throws ConflictingStateException if the evaluated state conflicts with the current proofState
	 */
	public synchronized void evaluateSatisfiableFromChildren() throws PTElementLocked, ConflictingStateException {
		boolean allUnsat = true;
		// Don't do anything if there are no children.
		if (children.isEmpty()) {
			setSatisfiable(ProofState.UNKNOWN);
			return;
		}
		for (PTElement child : children) {
			// If the child has been created by a confluent Rule and is SAT, the PTElement at hand is also SAT.
			if (child.getRule().isConfluent() && ProofState.sat.contains(child.getSatisfiable())) {
				setSatisfiable(ProofState.INTERNAL_SAT);
				return;
			}
			// Check whether all children are UNSAT.
			if (ProofState.sat.contains(child.getSatisfiable()) || child.proofState == ProofState.UNKNOWN) {
				allUnsat = false;
			}
		}
		// If all children are UNSAT, the PTElement at hand is also UNSAT,
		// assuming the Rule that created the children is correct.
		if (allUnsat) {
			setSatisfiable(ProofState.INTERNAL_UNSAT);
			return;
		}
		setSatisfiable(ProofState.UNKNOWN);
	}

	/**
	 * Adds a listener to listen when this obligation undergoes a change.
	 *
	 * @param newListener Listener to add
	 */
	public void addChangeListener(PropertyChangeListener newListener) {
		listeners.add(newListener);
	}

	/**
	 * Removes the given listener.
	 *
	 * @param rmListener Listener to remove
	 */
	public void rmChangeListener(PropertyChangeListener rmListener) {
		listeners.remove(rmListener);
	}

	/**
	 * Gets whether or not this node is locked.
	 *
	 * @return true if this is locked, else false
	 * @see #lock
	 */
	public synchronized boolean isLocked() {
		return locked;
	}

	/**
	 * Locks this node and starts the given service. The service should use
	 * {@link #unlock(SolverAnswer)} if it was successful and {@link #unlockAbort()}
	 * if the service failed or was aborted.
	 *
	 * @param solverService service to start
	 * @throws PTElementLocked when this node is already locked. Note that this
	 *                         method does not block until it is successful, rather
	 *                         it will throw an exception if this node is already
	 *                         locked.
	 */
	public synchronized void lock(SolverService solverService) throws PTElementLocked {
		if (!locked) {
			locked = true;
			this.solverService = solverService;
			this.solverService.start();
		} else {
			throw new PTElementLocked();
		}
		notifyListeners();
	}

	/**
	 * Unlocks this node and saves the given answer (if possible) to the proofState
	 * state of this node.
	 * Evaluates the proof state afterwards in case something has changed while
	 * the PTElement was locked.
	 *
	 * @param solverAnswer answer to save
	 * @see #lock
	 *
	 * @throws ConflictingStateException if the evaluated state conflicts with the current proofState
	 */
	public synchronized void unlock(SolverAnswer solverAnswer) throws ConflictingStateException {
		if (!locked) {
			return;
		}
		locked = false;
		for (ProofState state : ProofState.values()) {
			if (state.toString().equals(solverAnswer.getType().toString())) {
				try {
					setSatisfiable(state);
				} catch (PTElementLocked e) {
					return;
				}
			}
		}
		solverService = null;
		notifyListeners();
		// Try to calculate proof state.
		// Won't set the proof state to UNKNOWN if it is EXTERNAL because of the priority.
		evaluateNewState();
	}

	/**
	 * Calculate the new proof state if that should have happened while this element was locked.
	 */
	private synchronized void evaluateNewState() throws ConflictingStateException {
		try {
			evaluateSatisfiableFromChildren();
		} catch (PTElementLocked e) {
			LOGGER.error(e.getMessage());
		}
	}

	/**
	 * Unlocks node and evaluates the proof state afterwards in case something has changed while
	 * the PTElement was locked.
	 *
	 * @see #lock
	 *
	 * @throws ConflictingStateException if the evaluated proof state conflicts with the current proofState
	 */
	public synchronized void unlockAbort() throws ConflictingStateException {
		if (solverService != null) {
			solverService.cancel();
		}
		locked = false;
		solverService = null;
		notifyListeners();
		// Try to calculate proof state.
		// Won't set the proof state to UNKNOWN if it is EXTERNAL because of the priority.
		evaluateNewState();
	}

	/**
	 * Add a new children to this node.
	 *
	 * @param newChild Child to add
	 */
	public void addChildren(PTElement newChild) {
		children.add(newChild);
		newChild.setParent(this);
		newChild.setProofTree(proofTree);
		if (proofTree != null) {
			proofTree.setActivePTElement(newChild);
		}
		notifyListeners();
	}

	/**
	 * Removes children from this node if it exists.
	 *
	 * @param rmChild Child to remove
	 */
	public void rmChildren(PTElement rmChild) {
		rmChild.setProofTree(null);
		children.remove(rmChild);
		if (proofTree != null) {
			proofTree.checkActive();
		}
		notifyListeners();
	}

	/**
	 * Gets all children of this node.
	 *
	 * @return List of children
	 */
	public List<PTElement> getChildren() {
		return children;
	}

	/**
	 * Gets the {@link ProofState} state of this node.
	 *
	 * @return proofState state
	 */
	public synchronized ProofState getSatisfiable() {
		return proofState;
	}


	/**
	 * Sets the {@link ProofState} state of this node and check for
	 * conflicts between the new and old proof state according to
	 * {@link PTElement#handleConflicts(ProofState)}.
	 *
	 * If the PTElement is locked, the proof state is not changed.
	 *
	 * @param proofState {@link ProofState} the new proof state
	 *
	 * @throws PTElementLocked if this PTElement
	 * @throws ConflictingStateException if the new state conflicts with the current proofState
	 */
	public synchronized void setSatisfiable(ProofState proofState)
			throws PTElementLocked, ConflictingStateException {
		setSatisfiable(proofState, true);
	}

	private synchronized void setSatisfiable(ProofState proofState, boolean checkConflicts)
			throws PTElementLocked, ConflictingStateException {
		if (checkConflicts && !handleConflicts(proofState)) {
			return;
		}
		if (!locked) {
			ProofState tempState = this.proofState;
			this.proofState = proofState;
			// Let parent evaluate its new proof state if this element's state
			// changed and the parent is not currently locked.
			if (tempState != proofState && parent != null && !parent.isLocked()) {
				parent.evaluateSatisfiableFromChildren();
			}
		} else {
			throw new PTElementLocked();
		}
		notifyListeners();
	}

	/**
	 * Set the new {@link PTElement#proofState} without checking for conflicts
	 * between the new and old proof state.
	 *
	 * If the PTElement is currently locked, the proof state is not changed.
	 *
	 * @param proofState {@link ProofState} the new proof state
	 * @throws PTElementLocked if the element is currently locked
	 */
	public synchronized void setSatisfiableNoMatterConflicts(ProofState proofState)
			throws PTElementLocked {
		try {
			this.setSatisfiable(proofState, false);
		} catch (ConflictingStateException e) {
			LOGGER.error(e.getMessage());
		}
	}

	/**
	 * @param newState the new proof state
	 * @return whether the proof state should be changed
	 * @throws ConflictingStateException if the states are conflicting (different values)
	 */
	private boolean handleConflicts(ProofState newState) throws ConflictingStateException {
		// Always change if the current proof state is unknown.
		if (proofState == ProofState.UNKNOWN) {
			return true;
		}
		// Throw if new state conflicts with proofState
		if (newState.conflicts(proofState)) {
			throw new ConflictingStateException(newState, proofState);
		}
		// Change if the states are the same but the new state has been caused externally:
		// Prioritize external change over internal change.
		return newState.causedBy().prioritizes(proofState.causedBy());
	}

	/**
	 * Gets the parent of this node.
	 *
	 * @return Parent of this node
	 */
	public PTElement getParent() {
		return parent;
	}

	/**
	 * Sets the parent of this node.
	 *
	 * @param parent Parent of this node
	 */
	public void setParent(PTElement parent) {
		this.parent = parent;
	}

	/**
	 * Gets the currently running service on this element if any.
	 *
	 * @return currently running service or null if no service is running on this
	 * node
	 */
	public SolverService getSolverService() {
		return solverService;
	}

	/**
	 * Gets the {@link Script} of this node.
	 *
	 * @return script
	 */
	public Script getScript() {
		return script;
	}

	/**
	 * Sets the script of this node.
	 *
	 * @param script to set to
	 */
	public void setScript(Script script) {
		this.script = script;
	}

	/**
	 * Gets the {@link Rule} this node was created from if any.
	 *
	 * @return rule that created this element or null if it was not created by a
	 * rule. In an optimal implementation this method should never return
	 * null.
	 */
	public Rule getRule() {
		return rule;
	}

	/**
	 * Gets the {@link ProofTree} this element is a part of if any.
	 *
	 * @return ProofTree
	 */
	public ProofTree getProofTree() {
		return proofTree;
	}

	/**
	 * Sets the {@link ProofTree} this element should be part of.
	 * This method is already called internally when using {@link ProofTree} and methods from this class.
	 * This method should therefore not be called from outside, as a call to this method might corrupt state.
	 *
	 * @param proofTree is the ProofTree in which the PTElement is a node
	 */
	public void setProofTree(ProofTree proofTree) {
		this.proofTree = proofTree;
	}

}
