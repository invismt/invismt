/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;


import invismt.tree.exception.CommandFailedToExecute;

import java.util.Deque;
import java.util.LinkedList;

/**
 * History of {@link TreeCommand}'s that provides undo and redo functionality.
 */
public class TreeCommandHistory {
	private final Deque<TreeCommand> done;
	private final Deque<TreeCommand> undo;

	/**
	 * Creates a new history without any commands.
	 */
	public TreeCommandHistory() {
		done = new LinkedList<>();
		undo = new LinkedList<>();
	}

	/**
	 * Tries to execute the command and if done successfully adds it to the done commands.
	 * This also removes any commands that could have been redone.
	 *
	 * @param treeCommand Command to execute
	 * @throws CommandFailedToExecute thrown when command could not be executed
	 */
	public void addCommand(TreeCommand treeCommand) throws CommandFailedToExecute {
		treeCommand.execute();
		done.push(treeCommand);
		undo.clear();
	}

	/**
	 * Undo's the last command.
	 *
	 * @throws CommandFailedToExecute thrown when command could not be undone
	 */
	public void undo() throws CommandFailedToExecute {
		if (done.isEmpty()) {
			return;
		}
		TreeCommand treeCommand = done.pop();
		try {
			treeCommand.undo();
			undo.push(treeCommand);
		} catch (CommandFailedToExecute commandFailedToExecute) {
			done.push(treeCommand);
			throw commandFailedToExecute;
		}
	}

	/**
	 * Redo's the last command.
	 *
	 * @throws CommandFailedToExecute thrown when command could not be redone
	 */
	public void redo() throws CommandFailedToExecute {
		if (undo.isEmpty()) {
			return;
		}
		TreeCommand treeCommand = undo.pop();
		try {
			treeCommand.redo();
			done.push(treeCommand);
		} catch (CommandFailedToExecute commandFailedToExecute) {
			undo.push(treeCommand);
			throw commandFailedToExecute;
		}
	}

	/**
	 * Gets all done commands.
	 *
	 * @return Commands that have been executed
	 */

	public Deque<TreeCommand> getDone() {
		return done;
	}

	/**
	 * Gets
	 *
	 * @return Commands that have been undone
	 */
	public Deque<TreeCommand> getUndo() {
		return undo;
	}
}
