/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import invismt.tree.exception.CommandFailedToExecute;

/**
 * Interface for the Commands in the Tree
 */
public interface TreeCommand {
	/**
	 * Execute Command on the Tree
	 *
	 * @throws CommandFailedToExecute is thrown when Command can not execute
	 */
	void execute() throws CommandFailedToExecute;

	/**
	 * Redo Tree
	 *
	 * @throws CommandFailedToExecute is thrown when Command can not execute
	 */
	void redo() throws CommandFailedToExecute;

	/**
	 * undo Tree
	 *
	 * @throws CommandFailedToExecute is thrown when Command can not execute
	 */
	void undo() throws CommandFailedToExecute;

	@Override
	String toString();
}
