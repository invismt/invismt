/**
 * Copyright (c) 2021 Kaan Berk Yaman <kaan.yaman@student.kit.edu>
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 * Copyright (c) 2017 Julian Thome <julian.thome.de@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/
grammar SMTLIBv2;
@header {
package invismt.parser.antlr;
}
////// Lexer rules after this line
Comment
    : Semicolon ~[\r\n]* -> skip
    ;
ParOpen
    : '('
    ;
ParClose
    : ')'
    ;
Semicolon
    : ';'
    ;
String
    : '"' (PrintableCharNoDquote | WhiteSpaceChar)+ '"'
    ;
QuotedSymbol:
        '|' (PrintableCharNoBackslash | WhiteSpaceChar)+ '|'
    ;
Numeral
    : '0'
    | [1-9] Digit*
    ;
Binary
    : '#b' BinaryDigit+
    ;
HexDecimal
    : '#x' HexDigit+
    ;
Decimal
    : Numeral '.' '0'* Numeral
    ;
//// Predefined symbols and keywords
// Info flags
PK_AllStatistics
    : ':all-statistics'
    ;
PK_AssertionStackLevels
    : ':assertion-stack-levels'
    ;
PK_Authors
    : ':authors'
    ;
PK_ErrorBehavior
    : ':error-behavior'
    ;
PK_Named
    : ':named'
    ;
PK_Name
    : ':name'
    ;
PK_ReasonUnknown
    : ':reason-unknown'
    ;
PK_Version
    : ':version'
    ;
PS_Not
    : 'not'
    ;
PS_Bool
    : 'Bool'
    ;
PS_ContinuedExecution
    : 'continued-execution'
    ;
PS_Error
    : 'error'
    ;
PS_False
    : 'false'
    ;
PS_ImmediateExit
    : 'immediate-exit'
    ;
PS_Incomplete
    : 'incomplete'
    ;
PS_Logic
    : 'logic'
    ;
PS_Memout
    : 'memout'
    ;
PS_Sat
    : 'sat'
    ;
PS_Success
    : 'success'
    ;
PS_Theory
    : 'theory'
    ;
PS_True
    : 'true'
    ;
PS_Unknown
    : 'unknown'
    ;
PS_Unsupported
    : 'unsupported'
    ;
PS_Unsat
    : 'unsat'
    ;
//// Reserved words
// Command names
CMD_Assert
    : 'assert'
    ;
CMD_CheckSat
    : 'check-sat'
    ;
CMD_CheckSatAssuming
    : 'check-sat-assuming'
    ;
CMD_DeclareConst
    : 'declare-const'
    ;
CMD_DeclareDatatype
    : 'declare-datatype'
    ;
CMD_DeclareDatatypes
    : 'declare-datatypes'
    ;
CMD_DeclareFun
    : 'declare-fun'
    ;
CMD_DeclareSort
    : 'declare-sort'
    ;
CMD_DefineFun
    : 'define-fun'
    ;
CMD_DefineFunRec
    : 'define-fun-rec'
    ;
CMD_DefineFunsRec
    : 'define-funs-rec'
    ;
CMD_DefineSort
    : 'define-sort'
    ;
CMD_Echo
    : 'echo'
    ;
CMD_Exit
    : 'exit'
    ;
CMD_Pop
    : 'pop'
    ;
CMD_Push
    : 'push'
    ;
CMD_Reset
    : 'reset'
    ;
CMD_SetLogic
    : 'set-logic'
    ;
CMD_SetInfo
    : 'set-info'
    ;
CMD_GetInfo
    : 'get-info'
    ;
CMD_SetOption
    : 'set-option'
    ;
CMD_GetOption
    : 'get-option'
    ;
CMD_GetModel
    : 'get-model'
    ;
CMD_GetProof
    : 'get-proof'
    ;
// General reserved words
GRW_Exclamation
    : '!'
    ;
GRW_Underscore
    : '_'
    ;
GRW_As
    : 'as'
    ;
GRW_Binary
    : 'BINARY'
    ;
GRW_Decimal
    : 'DECIMAL'
    ;
GRW_Exists
    : 'exists'
    ;
GRW_Hexadecimal
    : 'HEXADECIMAL'
    ;
GRW_Forall
    : 'forall'
    ;
GRW_Let
    : 'let'
    ;
GRW_Match
    : 'match'
    ;
GRW_Numeral
    : 'NUMERAL'
    ;
GRW_Par
    : 'par'
    ;
GRW_String
    : 'string'
    ;
fragment HexDigit
    : '0' .. '9' | 'a' .. 'f' | 'A' .. 'F'
    ;
fragment Colon
    : ':'
    ;
fragment Digit
    : [0-9]
    ;
fragment Sym
    : 'a'..'z'
    | 'A' .. 'Z'
    | '+'
    | '='
    | '/'
    | '*'
    | '%'
    | '?'
    | '!'
    | '$'
    | '-'
    | '_'
    | '~'
    | '&'
    | '^'
    | '<'
    | '>'
    | '@'
    | '.'
    ;
fragment BinaryDigit
    : [01]
    ;
fragment PrintableChar
    : '\u0020' .. '\u007E'
    | '\u0080' .. '\uffff'
    | EscapedSpace
    ;
fragment PrintableCharNoDquote
    : '\u0020' .. '\u0021'
    | '\u0023' .. '\u007E'
    | '\u0080' .. '\uffff'
    | EscapedSpace
    ;
fragment PrintableCharNoBackslash
    : '\u0020' .. '\u005B'
    | '\u005D' .. '\u007B'
    | '\u007D' .. '\u007E'
    | '\u0080' .. '\uffff'
    | EscapedSpace
    ;
fragment EscapedSpace
    : '""'
    ;
fragment WhiteSpaceChar
    : '\u0009'
    | '\u000A'
    | '\u000D'
    | '\u0020'
    ;
UndefinedSymbol
    : Sym (Digit | Sym)*
    ;
UndefinedKeyword
    : Colon UndefinedSymbol
    ;
////// Parser rules after this line
b_value
    : PS_True
    | PS_False
    ;
generalReservedWord
    : GRW_Exclamation
    | GRW_Underscore
    | GRW_As
    | GRW_Binary
    | GRW_Decimal
    | GRW_Exists
    | GRW_Hexadecimal
    | GRW_Forall
    | GRW_Let
    | GRW_Match
    | GRW_Numeral
    | GRW_Par
    | GRW_String
    ;
simpleSymbol
    : predefSymbol
    | UndefinedSymbol
    ;
quotedSymbol
    : QuotedSymbol
    ;
predefSymbol
    : PS_Not
    | PS_Bool
    | PS_ContinuedExecution
    | PS_Error
    | PS_False
    | PS_ImmediateExit
    | PS_Incomplete
    | PS_Logic
    | PS_Memout
    | PS_Sat
    | PS_Success
    | PS_Theory
    | PS_True
    | PS_Unknown
    | PS_Unsupported
    | PS_Unsat
    ;
predefKeyword
    : PK_AllStatistics
    | PK_AssertionStackLevels
    | PK_Authors
    | PK_Category
    | PK_Chainable
    | PK_Definition
    | PK_DiagnosticOutputChannel
    | PK_ErrorBehaviour
    | PK_Extension
    | PK_Funs
    | PK_FunsDescription
    | PK_GlobalDeclarations
    | PK_InteractiveMode
    | PK_Language
    | PK_LeftAssoc
    | PK_License
    | PK_Named
    | PK_Name
    | PK_Notes
    | PK_Pattern
    | PK_PrintSuccess
    | PK_ProduceAssertions
    | PK_ProduceAssignments
    | PK_ProduceModels
    | PK_ProduceProofs
    | PK_ProduceUnsatAssumptions
    | PK_ProduceUnsatCores
    | PK_RandomSeed
    | PK_ReasonUnknown
    | PK_RegularOutputChannel
    | PK_ReproducibleResourceLimit
    | PK_RightAssoc
    | PK_SmtLibVersion
    | PK_Sorts
    | PK_SortsDescription
    | PK_Source
    | PK_Status
    | PK_Theories
    | PK_Values
    | PK_Verbosity
    | PK_Version
    ;
symbol
    : simpleSymbol
    | quotedSymbol
    ;
/// InViSMT extension, this rule isn't a part of the SMT-LIB standard
single_symbol
    : symbol EOF
    ;
numeral
    : Numeral
    ;
decimal
    : Decimal
    ;
hexadecimal
    : HexDecimal
    ;
binary
    : Binary
    ;
string
    : String
    ;
keyword
    : predefKeyword
    | UndefinedKeyword
    ;
// S-expressions
spec_constant
    : numeral
    | decimal
    | hexadecimal
    | binary
    | string
    ;
s_expr
    : spec_constant
    | symbol
    | keyword
    | ParOpen s_expr* ParClose
    ;
// Identifiers and indexes
index
    : numeral
    | symbol
    ;
identifier
    : symbol
    | ParOpen GRW_Underscore symbol index+ ParClose
    ;
// Attributes
attribute_value
    : spec_constant
    | symbol
    | ParOpen s_expr* ParClose
    ;
attribute
    : keyword
    | keyword attribute_value
    ;
// Sorts
sort
    : identifier
    | ParOpen identifier sort+ ParClose
    ;
// Terms and formulae
qual_identifier
    : identifier
    | ParOpen GRW_As identifier sort ParClose
    ;
var_binding
    : ParOpen symbol term ParClose
    ;
sorted_var
    : ParOpen symbol sort ParClose
    ;
single_sorted_var
    : sorted_var EOF
    ;
pattern
    : symbol
    | ParOpen symbol symbol+ ParClose
    ;
match_case
    : ParOpen pattern term ParClose
    ;
term
    : spec_constant
    | qual_identifier
    | ParOpen qual_term ParClose
    | ParOpen let_term ParClose
    | ParOpen forall_term ParClose
    | ParOpen exists_term ParClose
    | ParOpen match_term ParClose
    | ParOpen exclamation_term ParClose
    ;
/// InViSMT extension, this rule isn't a part of the SMT-LIB standard
single_term
    : term EOF
    ;
qual_term
    : qual_identifier term+
    ;
let_term
    : GRW_Let ParOpen var_binding+ ParClose term
    ;
forall_term
    : GRW_Forall ParOpen sorted_var+ ParClose term
    ;
exists_term
    : GRW_Exists ParOpen sorted_var+ ParClose term
    ;
match_term
    : GRW_Match term ParOpen match_case+ ParClose
    ;
exclamation_term
    : GRW_Exclamation term attribute+
    ;
// Commands (and resp. secondary structures)
script
    : command* EOF
    ;
sort_dec
    : ParOpen symbol numeral ParClose
    ;
selector_dec
    : ParOpen symbol sort ParClose
    ;
constructor_dec
    : ParOpen symbol selector_dec* ParClose
    ;
datatype_dec
    : ParOpen constructor_dec+ ParClose
    | ParOpen GRW_Par ParOpen symbol+ ParClose ParOpen constructor_dec+
        ParClose ParClose
    ;
function_dec
    : ParOpen symbol ParOpen sorted_var* ParClose sort ParClose
    ;
prop_literal
    : symbol
    | ParOpen PS_Not symbol ParClose
    ;
cmd_declareConst
    : CMD_DeclareConst symbol sort
    ;
cmd_declareDatatype
    : CMD_DeclareDatatype symbol datatype_dec
    ;
cmd_declareDatatypes
    : CMD_DeclareDatatypes ParOpen sort_dec+ ParClose ParOpen datatype_dec+ ParClose
    ;
cmd_declareSort
    : CMD_DeclareSort symbol numeral
    ;
cmd_defineFun
    : CMD_DefineFun symbol ParOpen sorted_var* ParClose sort term
    ;
cmd_defineFunRec
    : CMD_DefineFunRec symbol ParOpen sorted_var* ParClose sort term
    ;
cmd_defineFunsRec
    : CMD_DefineFunsRec ParOpen function_dec+ ParClose ParOpen term+ ParClose
    ;
cmd_defineSort
    : CMD_DefineSort symbol ParOpen symbol* ParClose sort
    ;
cmd_assert
    : CMD_Assert term
    ;
cmd_checkSat
    : CMD_CheckSat
    ;
cmd_checkSatAssuming
    : CMD_CheckSatAssuming ParOpen prop_literal ParClose
    ;
cmd_declareFun
    : CMD_DeclareFun symbol ParOpen sort* ParClose sort
    ;
cmd_echo
    : CMD_Echo string
    ;
cmd_exit
    : CMD_Exit
    ;
cmd_pop
    : CMD_Pop numeral
    ;
cmd_push
    : CMD_Push numeral
    ;
cmd_reset
    : CMD_Reset
    ;
cmd_setLogic
    : CMD_SetLogic symbol
    ;
cmd_setInfo
    : CMD_SetInfo attribute
    ;
cmd_getOption
    : CMD_GetOption keyword
    ;
cmd_setOption
    : CMD_SetOption attribute
    ;
cmd_getProof
    : CMD_GetProof
    ;
cmd_getModel
    : CMD_GetModel
    ;
cmd_getInfo
    : CMD_GetInfo info_flag
    ;
command
    : ParOpen cmd_assert ParClose
    | ParOpen cmd_checkSat ParClose
    | ParOpen cmd_checkSatAssuming ParClose
    | ParOpen cmd_declareConst ParClose
    | ParOpen cmd_declareDatatype ParClose
    | ParOpen cmd_declareDatatypes ParClose
    | ParOpen cmd_declareFun ParClose
    | ParOpen cmd_declareSort ParClose
    | ParOpen cmd_defineFun ParClose
    | ParOpen cmd_defineFunRec ParClose
    | ParOpen cmd_defineFunsRec ParClose
    | ParOpen cmd_defineSort ParClose
    | ParOpen cmd_echo ParClose
    | ParOpen cmd_exit ParClose
    | ParOpen cmd_pop ParClose
    | ParOpen cmd_push ParClose
    | ParOpen cmd_reset ParClose
    | ParOpen cmd_setLogic ParClose
    | ParOpen cmd_getInfo ParClose
    | ParOpen cmd_setInfo ParClose
    | ParOpen cmd_getOption ParClose
    | ParOpen cmd_setOption ParClose
    | ParOpen cmd_getProof ParClose
    | ParOpen cmd_getModel ParClose
    ;
// Options
option
    : PK_DiagnosticOutputChannel string
    | PK_GlobalDeclarations b_value
    | PK_InteractiveMode b_value
    | PK_PrintSuccess b_value
    | PK_ProduceAssertions b_value
    | PK_ProduceAssignments b_value
    | PK_ProduceModels b_value
    | PK_ProduceProofs b_value
    | PK_ProduceUnsatAssumptions b_value
    | PK_ProduceUnsatCores b_value
    | PK_RandomSeed numeral
    | PK_RegularOutputChannel string
    | PK_ReproducibleResourceLimit numeral
    | PK_Verbosity numeral
    | attribute
    ;
// Info flags
info_flag
    : PK_AllStatistics
    | PK_AssertionStackLevels
    | PK_Authors
    | PK_ErrorBehavior
    | PK_Name
    | PK_ReasonUnknown
    | PK_Version
    | keyword
    ;
//// One last lexer rule
// Annulate whitespace (forward to different channel)
WS  :  [ \t\r\n]+ -> skip
    ;
