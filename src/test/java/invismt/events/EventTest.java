/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.events;

import invismt.formatting.Formatter;
import invismt.grammar.Expression;
import invismt.rule.RuleApplyCommand;
import invismt.rule.RuleWrapper;
import invismt.tree.ListOfProofObligation;
import invismt.tree.PTElement;
import invismt.tree.ProofObligation;
import invismt.tree.TreeCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

@ExtendWith(MockitoExtension.class)
class EventTest {

	private ExecuteRuleEvent executeRuleEvent;
	private RuleRequestedEvent ruleRequestedEvent;
	private StartSolverEvent startSolverEvent;
	private SetFormatterEvent setFormatterEvent;
	private SetPTElementEvent setPTElementEvent;
	private ChangeHistoryEvent changeHistoryEvent;
	private ChangeHistoryEvent changeHistoryEventNoCommand;
	private StopSolverEvent stopSolverEvent;
	private SetActiveProofObligationEvent setActiveProofObligationEvent;
	private ListOfProofObligationEvent listOfProofObligationEvent;
	private UpdateSolverStateEvent updateSolverStateEvent;
	private SaveToFileEvent saveToFileEvent;

	@Mock
	private RuleApplyCommand command;
	@Mock
	private ProofObligation obligation;
	@Mock
	private RuleWrapper wrapper;
	@Mock
	private PTElement element;
	@Mock
	private Expression expr;
	@Mock
	private Formatter formatter;
	@Mock
	private TreeCommand cmd;
	@Mock
	private ChangeHistoryEvent.Type type;
	@Mock
	private ListOfProofObligation list;

	@BeforeEach
	public void setUp() {
		this.executeRuleEvent = new ExecuteRuleEvent(command, obligation);
		this.ruleRequestedEvent = new RuleRequestedEvent(wrapper, element, expr);
		this.startSolverEvent = new StartSolverEvent(element);
		this.setFormatterEvent = new SetFormatterEvent(formatter);
		this.setPTElementEvent = new SetPTElementEvent(element);
		this.changeHistoryEvent = new ChangeHistoryEvent(obligation, type, cmd);
		this.changeHistoryEventNoCommand = new ChangeHistoryEvent(obligation, type);
		this.stopSolverEvent = new StopSolverEvent(element);
		this.setActiveProofObligationEvent = new SetActiveProofObligationEvent(obligation);
		this.listOfProofObligationEvent = new ListOfProofObligationEvent(list, EventAction.SET);
	}

	@Test
	void shouldGetIdenticalAttributes() {
		// ExecuteRuleEvent
		assertSame(command, executeRuleEvent.getCommand());
		assertSame(obligation, executeRuleEvent.getObligation());
		// RuleRequestedEvent
		assertSame(expr, ruleRequestedEvent.getExpression());
		assertSame(element, ruleRequestedEvent.getPTElement());
		assertSame(wrapper, ruleRequestedEvent.getRuleWrapper());
		// StartSolverEvent
		assertSame(element, startSolverEvent.getElement());
		// StopSolverEvent
		assertSame(element, stopSolverEvent.getElement());
		// SetFormatterEvent
		assertSame(formatter, setFormatterEvent.getNewFormatter());
		// SetPTElementEvent
		assertSame(element, setPTElementEvent.getPTElement());
		// ChangeHistoryEvent
		assertSame(obligation, changeHistoryEvent.getProofObligation());
		assertSame(type, changeHistoryEvent.getType());
		assertSame(cmd, changeHistoryEvent.getCommand());
		assertSame(type, changeHistoryEventNoCommand.getType());
		assertSame(obligation, changeHistoryEvent.getProofObligation());
		assertNull(changeHistoryEventNoCommand.getCommand());
		// SetActiveProofObligationEvent
		assertSame(obligation, setActiveProofObligationEvent.getObligation());
		// SetListOfProofObligationEvent
		assertSame(list, listOfProofObligationEvent.list());
	}
}
