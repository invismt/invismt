/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: How to test (hence, create) PrettyPrinterConfig with non-empty Identifier lists? Make inner Identifier class public?
class PrettyPrinterConfigTest {

	private PrettyPrinterConfig config;

	@BeforeEach
	public void setUp() {
		this.config = new PrettyPrinterConfig(new ArrayList<>());
	}

	@ParameterizedTest
	@ValueSource(strings = {"test", "name", "", "name", " identifier "})
	void shouldReturnDefaultValuesNullOrTrueForEveryGetterMethodWhenIdentifierListIsEmpty(
			String identifierName) {
		assertNull(config.getAlias(identifierName));
		assertNull(config.getNotation(identifierName));
		assertTrue(config.useParentheses(identifierName));
	}

}
