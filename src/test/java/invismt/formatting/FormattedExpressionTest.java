/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;


import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FormattedExpressionTest {

	Expression expr;
	Block block;
	List<Block> blocks;
	FormattedExpression formattedExpr;

	@BeforeEach
	void setUp() {
		blocks = new ArrayList<>();
		expr = new GeneralExpression("test", false);
		block = new Block(0, List.of(new Atom("text", expr), new Atom("text", expr)));
		blocks.add(block);
		formattedExpr = new FormattedExpression(blocks, expr);
	}

	@Test
	void shouldBeVisibleByDefaultAndReturnCorrectVisibilityAfterBeingSet() {
		assertTrue(formattedExpr.isVisible());
		formattedExpr.setVisible(false);
		assertFalse(formattedExpr.isVisible());
	}

	@Test
	void shouldReturnEqualButNotIdenticalBlocks() {
		assertNotNull(formattedExpr.getBlocks());
		assertEquals(formattedExpr.getBlocks(), blocks);
		assertNotSame(blocks, formattedExpr.getBlocks());
	}

	@Test
	void shouldPrintAtomTextsWithoutDelimiters() {
		assertEquals("texttext", formattedExpr.toString());
	}

	@Test
	void shouldReturnIdenticalExpression() {
		assertSame(expr, formattedExpr.getExpression());
	}

}