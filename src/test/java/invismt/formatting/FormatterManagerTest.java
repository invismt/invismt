/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.util.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Does not test what happens if an InstantiationException occurs when calling
 * {@link FormatterManager#getInstance(Class)} as that case cannot be reached
 * currently.
 */
class FormatterManagerTest {

	Class<? extends Formatter> notAnInstalledFormatter;

	Class<? extends Formatter> installedFormatter;

	private FormatterManager manager;

	@BeforeEach
	void setUp() {
		notAnInstalledFormatter = SMTLIBFormatter.class;
		installedFormatter = PrettyPrinter.class;
		manager = new FormatterManager();
	}

	@Test
	void shouldReturnInstanceForEveryElementOfInstalledFormatters() {
		Set<Tuple<Class<? extends Formatter>, String>> installedFormatters = manager.getInstalledFormatters();
		for (Tuple<Class<? extends Formatter>, String> entry : installedFormatters) {
			assertTrue(manager.getInstance(entry.getFirst()).isPresent());
		}
	}

	@Test
	void shouldReturnObjectOfEnteredClassAsInstance() {
		assertEquals(installedFormatter, manager.getInstance(installedFormatter).get().getClass());
	}

	@Test
	void shouldReturnFormatterGetNamesAsTupleStrings() {
		Set<Tuple<Class<? extends Formatter>, String>> installedFormatters = manager.getInstalledFormatters();
		for (Tuple<Class<? extends Formatter>, String> entry : installedFormatters) {
			assertEquals(entry.getSecond(), manager.getInstance(entry.getFirst()).get().getName());
		}
	}

	@Test
	void shouldHaveDefaultFormatterInstalled() {
		assertTrue(manager.getInstance(manager.getDefaultFormatterClass()).isPresent());
	}

	@Test
	void shouldReturnEmptyOptionalOfNonExistentFormatters() {
		assertTrue(manager.getInstance(notAnInstalledFormatter).isEmpty());
	}

	@Test
	void shouldReturnFormatterInstanceOfExistentFormatters() {
		Optional<Formatter> instance = manager.getInstance(installedFormatter);
		assertTrue(instance.isPresent());
	}

	@Test
	void shouldReturnNewFormatterEachTimeGetInstanceIsCalled() {
		Optional<Formatter> instance1 = manager.getInstance(installedFormatter);
		assertTrue(instance1.isPresent());
		Optional<Formatter> instance2 = manager.getInstance(installedFormatter);
		assertTrue(instance2.isPresent());
		assertEquals(instance1.get().getClass(), instance2.get().getClass());
		assertNotSame(instance1.get(), instance2.get());
	}

}
