/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.formatting;

import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FormattedExpressionBuilderTest {

	@Mock
	Expression expr;

	String text;

	private FormattedExpressionBuilder builder;

	@BeforeEach
	void init() {
		builder = new FormattedExpressionBuilder();
		text = "sample_text";
	}

	@Test
	void shouldCreateAsManyBlocksAsCallsToCreateBlockWhenAtomsAreAppendedEachTime() {
		// Should not create a block as no atoms are appended:
		builder.createBlock();

		builder.append(text, expr);
		builder.createBlock();
		builder.append(text, expr);
		builder.append(text, expr);
		builder.append(text, expr);
		builder.createBlock();
		FormattedExpression result = builder.getFormattedExpression(expr);
		assertEquals(2, result.getBlocks().size());
	}

	@Test
	void shouldEmptyBlocksWhenGetFormattedExpressionIsCalled() {
		builder.append(text, expr);
		builder.createBlock();
		builder.append(text, expr);
		builder.append(text, expr);
		builder.append(text, expr);
		builder.createBlock();
		FormattedExpression result = builder.getFormattedExpression(expr);
		assertEquals(2, result.getBlocks().size());
		FormattedExpression resultAfterClear = builder.getFormattedExpression(expr);
		assertTrue(resultAfterClear.getBlocks().isEmpty());
	}

	@Test
	void shouldAddAtomToBlock() {
		builder.append(text, expr);
		builder.createBlock();
		FormattedExpression result = builder.getFormattedExpression(expr);
		Block block = result.getBlocks().iterator().next();
		List<Atom> atoms = block.getAtoms();
		assertEquals(1, atoms.size());
		assertEquals(text, atoms.iterator().next().getText());
		assertEquals(expr, atoms.iterator().next().getAssociatedExpression());
	}

	@Test
	void shouldCreateNewAtomWithEveryAppend() {
		builder.append(text, expr);
		builder.append(text, expr);
		builder.append(text, expr);
		builder.createBlock();
		FormattedExpression result = builder.getFormattedExpression(expr);
		Block block = result.getBlocks().iterator().next();
		List<Atom> atoms = block.getAtoms();
		assertEquals(3, atoms.size());
		Atom atom1 = atoms.get(0);
		Atom atom2 = atoms.get(1);
		Atom atom3 = atoms.get(2);
		assertNotSame(atom1, atom2);
		assertNotSame(atom2, atom3);
		assertNotSame(atom1, atom3);
		assertSame(atom1, atom1);
	}

	@Test
	void shouldIncreaseAndDecreaseIndentationLevelByOne() {
		builder.append(text, expr);
		builder.getFormattedExpression(expr).getBlocks().forEach(block -> {
			assertEquals(0, block.getIndentationLevel());
		});
		builder.increaseIndentationLevel();
		builder.append(text, expr);
		builder.createBlock();
		builder.getFormattedExpression(expr).getBlocks().forEach(block -> {
			assertEquals(1, block.getIndentationLevel());
		});
		builder.append(text, expr);
		builder.getFormattedExpression(expr).getBlocks().forEach(block -> {
			assertEquals(1, block.getIndentationLevel());
		});
		builder.decreaseIndentationLevel();
		builder.append(text, expr);
		builder.getFormattedExpression(expr).getBlocks().forEach(block -> {
			assertEquals(0, block.getIndentationLevel());
		});
	}

	@ParameterizedTest
	@ValueSource(strings = {"random", "", "Command", "123 .- 9023 \\ß1 #"})
	void shouldSetStyleClassOfEveryAtomThatIsAppendedUntilReset(String styleClass) {
		builder.setStyleClass(styleClass);
		builder.append(text, expr);
		builder.append(text, expr);
		builder.append(text, expr);
		for (Block block : builder.getFormattedExpression(expr).getBlocks()) {
			for (Atom atom : block.getAtoms()) {
				assertTrue(atom.getStyleClass().isPresent());
				assertEquals(styleClass, atom.getStyleClass().get());
			}
		}
		builder.resetStyleClass();
		builder.append(text, expr);
		for (Block block : builder.getFormattedExpression(expr).getBlocks()) {
			for (Atom atom : block.getAtoms()) {
				assertFalse(atom.getStyleClass().isPresent());
			}
		}
	}

	@Test
	void shouldNotResetStyleClassWhenGettingFormattedExpression() {
		String styleClass = "styleClass";
		builder.setStyleClass(styleClass);
		builder.append(text, expr);
		for (Block block : builder.getFormattedExpression(expr).getBlocks()) {
			for (Atom atom : block.getAtoms()) {
				assertTrue(atom.getStyleClass().isPresent());
				assertEquals(styleClass, atom.getStyleClass().get());
			}
		}
		// Should not reset style class.
		builder.append(text, expr);
		for (Block block : builder.getFormattedExpression(expr).getBlocks()) {
			for (Atom atom : block.getAtoms()) {
				assertEquals(styleClass, atom.getStyleClass().get());
			}
		}
	}

	@Test
	void shouldChangeStyleClassWhenSetAnew() {
		String styleClass = "firstClass";
		builder.setStyleClass(styleClass);
		builder.append(text, expr);
		for (Block block : builder.getFormattedExpression(expr).getBlocks()) {
			for (Atom atom : block.getAtoms()) {
				assertEquals("firstClass", atom.getStyleClass().get());
			}
		}
		styleClass = "secondClass";
		builder.setStyleClass(styleClass);
		// Should not reset style class.
		builder.append(text, expr);
		for (Block block : builder.getFormattedExpression(expr).getBlocks()) {
			for (Atom atom : block.getAtoms()) {
				assertEquals("secondClass", atom.getStyleClass().get());
			}
		}
	}

	@Test
	void shouldOverrideAssociatedExpression() {
		Expression expr1 = new GeneralExpression("expr1", false);
		Expression expr2 = new GeneralExpression("expr2", false);
		builder.setAssociatedExpression(expr1);
		builder.append(text, expr2);
		assertSame(expr1, builder.getFormattedExpression(expr).getBlocks().get(0).getAtoms().get(0)
				.getAssociatedExpression());
	}

	@Test
	void shouldResetAssociatedExpression() {
		Expression expr1 = new GeneralExpression("expr1", false);
		Expression expr2 = new GeneralExpression("expr2", false);
		builder.setAssociatedExpression(expr1);
		builder.append(text, expr2);
		builder.resetAssociatedExpression();
		builder.append(text, expr2);
		FormattedExpression formattedExpression = builder.getFormattedExpression(expr);
		assertSame(expr1, formattedExpression.getBlocks().get(0).getAtoms().get(0).getAssociatedExpression());
		assertSame(expr2, formattedExpression.getBlocks().get(0).getAtoms().get(1).getAssociatedExpression());
	}

	@Test
	void shouldKeepSetStyleClassEvenWhenAtomWithNewStyleClassIsAppended() {
		builder.setStyleClass("style1");
		builder.append(text, expr);
		builder.append(text, expr, "style2");
		builder.append(text, expr);
		FormattedExpression formattedExpression = builder.getFormattedExpression(expr);
		for (Atom atom : formattedExpression.getBlocks().get(0).getAtoms()) {
			assertTrue(atom.getStyleClass().isPresent());
		}
		assertEquals("style1", formattedExpression.getBlocks().get(0).getAtoms().get(0).getStyleClass().get());
		assertEquals("style2", formattedExpression.getBlocks().get(0).getAtoms().get(1).getStyleClass().get());
		assertEquals("style1", formattedExpression.getBlocks().get(0).getAtoms().get(2).getStyleClass().get());
	}

}