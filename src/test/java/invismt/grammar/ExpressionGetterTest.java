/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.Token.SymbolToken;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionGetterTest {

	private Expression generalExpressionAtom;
	private Expression sortChainTail;
	private Expression sortChainBody;
	private Expression sortChainHead;
	private Expression tokenAtom;

	@org.junit.jupiter.api.BeforeEach
	void setUp() {
		this.sortChainTail = new Sort(new Identifier(new SymbolToken("tail"), List.of()), List.of());
		this.sortChainBody = new Sort(new Identifier(new SymbolToken("body"), List.of()),
				List.of((Sort) sortChainTail));
		this.sortChainHead = new Sort(new Identifier(new SymbolToken("head"), List.of()),
				List.of((Sort) sortChainBody));
		this.generalExpressionAtom = new GeneralExpression("atom", false);
		this.tokenAtom = new SymbolToken("atom");
	}

	@Test
	void shouldGetHeadCorrectlyUsingTail() {
		var parentList = sortChainHead.getParentChain(sortChainTail);
		assertFalse(parentList.isEmpty());
		assertTrue(parentList.stream().anyMatch((e) -> e == sortChainHead));
		// Use this instead of contains as it's the identity that needs to checked here
		assertTrue(parentList.stream().anyMatch((e) -> e == sortChainBody));
	}

	@Test
	void shouldHaveSameStringRepresentationButNotHaveEqualContent() {
		assertEquals(tokenAtom.toString(), generalExpressionAtom.toString());
		assertNotEquals(tokenAtom, generalExpressionAtom);
	}

	@Test
	void shouldBeAtomic() {
		assertTrue(this.tokenAtom.getChildren().isEmpty());
		assertTrue(this.generalExpressionAtom.getChildren().isEmpty());
	}

	@Test
	void shouldGetTailCorrectlyUsingHead() {
		var childList = sortChainHead.getChildren();
		assertTrue(childList.contains(sortChainBody));
		assertFalse(childList.contains(sortChainTail));
		var recursiveChildList = sortChainHead.getChildrenOfChildren();
		assertTrue(recursiveChildList.contains(sortChainBody));
		assertTrue(recursiveChildList.contains(sortChainTail));
	}

	@Test
	void shouldSearchAndFindEqualButNotIdentical() {
		var notIdentical = sortChainBody.deepCopy();
		assertTrue(sortChainHead.containsEqualExpressionSomewhere(notIdentical));
		assertFalse(sortChainHead.containsIdenticallySomewhere(notIdentical));
	}

	@Test
	void shouldSearchAndFindIdentical() {
		assertTrue(sortChainHead.containsIdenticallySomewhere(sortChainTail));
		assertTrue(sortChainHead.containsIdenticallySomewhere(sortChainBody));
	}

	@Test
	void shouldNotCopyChildrenWhenNotDeepCopied() {
		assertTrue(sortChainHead.copy().containsIdenticallySomewhere(sortChainBody));
		assertFalse(sortChainHead.deepCopy().containsIdenticallySomewhere(sortChainBody));
	}
}
