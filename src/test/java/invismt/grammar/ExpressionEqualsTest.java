/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.Token.NumeralToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Index;
import invismt.grammar.term.ConstantTerm;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ExpressionEqualsTest {

	private static Stream<Arguments> equalExpressions() {
		HashMap<Expression, List<Expression>> equalExpressions = new HashMap<>();
		equalExpressions.put(new Token.SymbolToken("|symbol|"), List.of(new Token.SymbolToken("|symbol|")));
		equalExpressions.put(new Token.BinaryToken("#b0101"), List.of(new Token.BinaryToken("#b0101")));
		equalExpressions.put(new Token.NumeralToken("42"), List.of(new Token.NumeralToken("42")));
		equalExpressions.put(new Token.DecimalToken("42.0"), List.of(new Token.DecimalToken("42.0")));
		equalExpressions.put(new Token.StringToken("\"string\""), List.of(new Token.StringToken("\"string\"")));
		equalExpressions.put(new Token.HexadecimalToken("#xabc"), List.of(new Token.HexadecimalToken("#xabc")));
		equalExpressions.put(new Token.ReservedWord(Token.ReservedWordName.ASSERT),
				List.of(new Token.ReservedWord(Token.ReservedWordName.ASSERT)));
		equalExpressions.put(new Token.Keyword(":named"), List.of(new Token.Keyword(":named")));
		equalExpressions.put(new Index.NumeralIndex(new NumeralToken("42")),
				List.of(new Index.NumeralIndex(new Token.NumeralToken("42"))));
		equalExpressions.put(new SyntaxCommand.DeclareFunction(new SymbolToken("f"), new ArrayList<>(),
				ExpressionFactory.INT), List.of(new SyntaxCommand.DeclareFunction(new SymbolToken("f"), new ArrayList<>(),
				ExpressionFactory.INT)));
		equalExpressions.put(new SyntaxCommand.Assert(
						new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("42")))),
				List.of(new SyntaxCommand.Assert(
						new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("42"))))));
		equalExpressions.put(new SyntaxCommand.Exit(), List.of(new SyntaxCommand.Exit()));
		equalExpressions.put(new SyntaxCommand.Push(new NumeralToken("7")), List.of(
				new SyntaxCommand.Push(new NumeralToken("7"))));
		equalExpressions.put(new SyntaxCommand.Pop(new NumeralToken("7")), List.of(
				new SyntaxCommand.Pop(new NumeralToken("7"))));
		return Stream.of(arguments(equalExpressions));
	}

	private static Stream<Arguments> unequalExpressions() {
		HashMap<Expression, List<Expression>> unequalExpressions = new HashMap<>();
		unequalExpressions.put(new Token.SymbolToken("|symbol|"), List.of(new Token.SymbolToken("|symbolUnequal|")));
		unequalExpressions.put(new Token.BinaryToken("#b0101"), List.of(new Token.BinaryToken("#b1010")));
		unequalExpressions.put(new Token.NumeralToken("42"), List.of(new Token.NumeralToken("69")));
		unequalExpressions.put(new Token.DecimalToken("42.0"), List.of(new Token.DecimalToken("42.00")));
		unequalExpressions
				.put(new Token.StringToken("\"string\""), List.of(new Token.StringToken("\"stringInequal\"")));
		unequalExpressions.put(new Token.HexadecimalToken("#xabc"), List.of(new Token.HexadecimalToken("#xABC")));
		unequalExpressions.put(new Index.NumeralIndex(new NumeralToken("42")),
				List.of(new Index.NumeralIndex(new Token.NumeralToken("69"))));
		unequalExpressions.put(new SyntaxCommand.Assert(
						new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("42")))),
				List.of(new SyntaxCommand.Assert(
						new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("43"))))));
		unequalExpressions.put(new SyntaxCommand.Assert(
						new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("42")))),
				List.of(new SyntaxCommand.DeclareFunction(new SymbolToken("f"), new ArrayList<>(),
						ExpressionFactory.INT)));
		return Stream.of(arguments(unequalExpressions));
	}

	private static Stream<Arguments> differentClassExpressions() {
		HashMap<Expression, List<Expression>> differentClassExpressions = new HashMap<>();
		differentClassExpressions.put(new Index.NumeralIndex(new NumeralToken("42")),
				List.of(new Index.SymbolIndex(new SymbolToken("symbol"))));

		return Stream.of(arguments(differentClassExpressions));
	}

	@ParameterizedTest
	@MethodSource("equalExpressions")
	void shouldBeEqualWhenComparedToSelf(HashMap<Expression, List<Expression>> equalExpressions) {
		for (Expression expr : equalExpressions.keySet()) {
			assertEquals(expr, expr);
		}
	}

	@ParameterizedTest
	@MethodSource("equalExpressions")
	void shouldBeEqualWhenComparedToNewObjectWithEqualChildrenAndTheSameClass(
			HashMap<Expression, List<Expression>> equalExpressions) {
		for (Expression expr : equalExpressions.keySet()) {
			for (Expression equal : equalExpressions.get(expr)) {
				assertEquals(equal, expr);
			}
		}
	}

	@ParameterizedTest
	@MethodSource("equalExpressions")
	void shouldBeEqualWhenComparedToACopyOrDeepCopy(HashMap<Expression, List<Expression>> equalExpressions) {
		for (Expression expr : equalExpressions.keySet()) {
			assertEquals(expr, expr.copy());
			assertEquals(expr, expr.deepCopy());
		}
	}

	@ParameterizedTest
	@MethodSource("equalExpressions")
	void shouldHaveEqualButNotIdenticalChildrenForDeepCopy(HashMap<Expression, List<Expression>> equalExpressions) {
		for (Expression expr : equalExpressions.keySet()) {
			List<Expression> deepCopyChildren = expr.deepCopy().getChildren();
			List<Expression> copyChildren = expr.copy().getChildren();
			List<Expression> children = expr.getChildren();
			assertEquals(children.size(), deepCopyChildren.size());
			assertEquals(children.size(), copyChildren.size());
			for (int i = 0; i < children.size(); i++) {
				assertEquals(children.get(i), deepCopyChildren.get(i));
				assertNotSame(children.get(i), deepCopyChildren.get(i));
				assertEquals(children.get(i), copyChildren.get(i));
			}
		}
	}

	@ParameterizedTest
	@MethodSource("unequalExpressions")
	void shouldNotBeEqualWhenTheClassesAreTheSameButTheChildrenAreNotEqualOrOneIsNull(
			HashMap<Expression, List<Expression>> unequalExpressions) {
		for (Expression expr : unequalExpressions.keySet()) {
			for (Expression unequal : unequalExpressions.get(expr)) {
				assertNotEquals(unequal, expr);
			}
			assertNotEquals(null, expr);
		}
	}

	@ParameterizedTest
	@MethodSource("differentClassExpressions")
	void shouldNotBeEqualWhenTheClassesAreDifferentButTypeCastedToTheSameParentClass(
			HashMap<Expression, List<Expression>> differentClassExpressions) {
		for (Expression expr : differentClassExpressions.keySet()) {
			for (Expression unequal : differentClassExpressions.get(expr)) {
				assertNotEquals(unequal, expr);
				assertNotEquals(((Expression) unequal), ((Expression) expr));
			}
			assertNotEquals(((Expression) null), ((Expression) expr));
		}
	}

}
