/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.grammar;

import invismt.grammar.SpecConstant.BinaryConstant;
import invismt.grammar.Token.BinaryToken;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.exceptions.DoesNotContainExpressionException;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;
import invismt.grammar.term.SortedVariable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScriptTest {

	private final List<Expression> listUnique = new ArrayList<>();
	private final List<Expression> listNotUniqueTopLevel = new ArrayList<>();
	private final List<Expression> listNotUniqueChildren = new ArrayList<>();

	BinaryToken token;
	Expression topLevel;

	@BeforeEach
	void setUp() {
		token = new BinaryToken("#b1");
		topLevel = new Assert(new ConstantTerm(new BinaryConstant(token)));

		listUnique.add(topLevel);
		listUnique.add(topLevel.deepCopy());

		listNotUniqueChildren.add(topLevel);
		listNotUniqueChildren.add(topLevel.copy());

		listNotUniqueTopLevel.add(topLevel);
		listNotUniqueTopLevel.add(topLevel);
	}

	@Test
	void shouldRemoveTopLevelWhenEmptyListIsInserted() {
		Script script = new Script(listUnique);
		int size = script.getTopLevelExpressions().size();
		assertEquals(size - 1, script.addTopLevelListInstead(token, new ArrayList<>()).getTopLevelExpressions().size());
	}

	@Test
	void shouldCreateScriptWhenTheGivenListIsUnique() {
		try {
			Script script = new Script(listUnique);
		} catch (IllegalArgumentException e) {
			fail("Script should have been created.");
		}
	}

	@Test
	void shouldNotCreateScriptWhenATopLevelExpressionExistsTwiceInTheList() throws IllegalArgumentException {
		assertThrows(IllegalArgumentException.class, () -> {
			Script script = new Script(listNotUniqueTopLevel);
		});
	}

	@Test
	void shouldNotCreateScriptWhenTwoExpressionsInTheListHaveAnIdenticalChildSomewhere() {
		assertThrows(IllegalArgumentException.class, () -> {
			Script script = new Script(listNotUniqueChildren);
		});
	}

	@Test
	void shouldGetTopLevelExpressionIdenticallyWhenIdenticalIsContained() {
		BinaryToken token = new BinaryToken("#b1");
		Expression topLevel = new Assert(new ConstantTerm(new BinaryConstant(token)));
		Script script = new Script(List.of(topLevel));
		assertSame(topLevel, script.getTopLevel(token));
	}

	@Test
	void shouldNotGetTopLevelExpressionWhenNotIdenticallyContained() {
		Script script = new Script(List.of(topLevel));
		assertSame(topLevel, script.getTopLevel(token));
		Token copy = token.copy();
		assertThrows(DoesNotContainExpressionException.class, () -> {
			script.getTopLevel(copy);
		});
	}

	@Test
	void shouldReturnCorrectParentChain() {
		ConstantTerm con = new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("0")));
		ForAllTerm forall = new ForAllTerm(List.of(new SortedVariable(new SymbolToken("x"),
				new Sort(new Identifier(new SymbolToken("Int"), List.of()), List.of()))), con);
		Expression assertion = new Assert(forall);
		Script s = new Script(List.of(assertion));
		List<? extends Expression> parents = s.getParentChain(con);
		assertEquals(3, parents.size());
		assertSame(assertion, parents.get(0));
		assertSame(forall, parents.get(1));
		assertSame(con, parents.get(2));
	}

	@Test
	void shouldThrowExceptionWhenGettingParentChainOfExpressionNotInScript() {
		Script s = new Script(listUnique);
		ConstantTerm term = new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("0")));
		assertThrows(DoesNotContainExpressionException.class, () -> s.getParentChain(term));
	}

	@Test
	void shouldReturnCorrectMetadata() {
		Script s = new Script(listUnique);
		Object o = new Object();
		s.setMetadata(listUnique.get(0), "test", o);
		assertSame(o, s.getMetadata(listUnique.get(0), "test"));
	}

	@Test
	void shouldReturnNullWhenMetadataDoesNotExist() {
		Script s = new Script(listUnique);
		assertEquals(null, s.getMetadata(listUnique.get(0).getChildren().get(0), "test"));
	}

	@Test
	void shouldReturnTrueIfExpressionIsTopLevel() {
		Script s = new Script(listUnique);
		assertEquals(true, s.isTopLevel(listUnique.get(0)));
	}

	@Test
	void shouldReturnFalseIfExpressionIsNotTopLevel() {
		Script s = new Script(listUnique);
		assertEquals(false, s.isTopLevel(listUnique.get(0).getChildren().get(0)));
	}

	@Test
	void shouldReturnFalseIfExpressionIsNotInScript() {
		Script s = new Script(listUnique);
		assertEquals(false, s.isTopLevel(new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken("0")))));
	}

}
