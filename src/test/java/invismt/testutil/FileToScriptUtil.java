/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.testutil;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.term.Term;
import invismt.parser.ScriptParserDispatcher;
import invismt.parser.TermParserDispatcher;
import invismt.testparser.MarkTermParserDispatcher;
import invismt.testparser.SingleExprParserDispatcher;
import invismt.util.Tuple;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Util class that reads .smt2 files and parses them into various kinds of lists and expressions.
 */
public class FileToScriptUtil {

	private static final Path workingDir = Path.of("", "src/test/resources/");
	private static final ScriptParserDispatcher dispatcher = new ScriptParserDispatcher();

	private FileToScriptUtil() {
		throw new InstantiationError();
	}

	/**
	 * Makes a Path out of a String path to a file.
	 *
	 * @param path the String path to the file, implicitly starting with {@link #workingDir}
	 * @return a Path with the given String path
	 */
	public static Path getFile(String path) {
		Path file = workingDir.resolve(path);
		return file;
	}

	/**
	 * Makes a list of expressions out of a file that is built like this:
	 * (ctx_name contentString \n)*
	 * where ctx_name is a name of a ParserRuleContext in the SMTLIBv2Parser
	 * created by the ANTLR package.
	 * Creates an object of the given context if possible.
	 *
	 * @param file the file from which the parameters will be gotten
	 * @return
	 * @throws IOException
	 */
	public static List<Expression> getParameters(Path file) throws IOException {
		List<Expression> params = new ArrayList<>();
		for (String line : Files.readAllLines(file)) {
			String[] args = line.split("%", 2);
			if (args.length == 2) {
				params.add(new SingleExprParserDispatcher(args[0]).parseInto(args[1]));
			}
		}
		return params;
	}

	/**
	 * Makes a list of parameters for {@link invismt.rule.TermReplacer} classes.
	 * The file has to have lines like "term;boolean" where boolean is parsed into a
	 * boolean value (true/false) and term is made into a {@link Term}.
	 *
	 * @param file the file that is read
	 * @return a list of Tuples of booleans and terms
	 * @throws IOException
	 */
	public static List<Tuple<Term, Boolean>> getReplacementParameters(Path file) throws IOException {
		List<Tuple<Term, Boolean>> replacements = new ArrayList<>();
		for (String line : Files.readAllLines(file)) {
			String[] args = line.split(";", 2);
			if (args.length == 2) {
				replacements.add(new Tuple<>(new TermParserDispatcher().parseInto(args[0]), Boolean.valueOf(args[1])));
			}
		}
		return replacements;
	}

	/**
	 * Makes a Script and reads marked terms out of a file.
	 * Marked terms look like that: "(param term param)"
	 *
	 * @param file the file that is read
	 * @return a Tuple of the parsed Script and the Expressions that are marked inside of the file
	 * @throws IOException
	 */
	public static Tuple<Script, List<Term>> getMarkedExpressionsScript(Path file) throws IOException {
		String content = Files.readString(file);
		MarkTermParserDispatcher parser = new MarkTermParserDispatcher();
		return parser.parseInto(content);
	}

	/**
	 * Parses a file into a Script using {@link ScriptParserDispatcher}.
	 *
	 * @param file the file that is read
	 * @return the parsed Script
	 * @throws IOException
	 */
	public static Script toScript(Path file) throws IOException {
		String content = Files.readString(file);
		return new Script(dispatcher.parseInto(content));
	}

}
