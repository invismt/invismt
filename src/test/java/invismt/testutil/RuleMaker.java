/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.testutil;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.Rule;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Makes {@link Rule rules} out of a Rule class and parameters.
 */
public class RuleMaker {

	/**
	 * Makes {@link Rule rules} out of a Rule class and parameters.
	 *
	 * @param ruleClass  the class of the created rule
	 * @param script     the script parameter for the rule
	 * @param applyTo    the expression the rule is to be applied to
	 * @param parameters other rule parameters
	 * @return the created rule, if successful
	 * @throws RuleCreationException
	 */
	public static Rule createRule(Class<? extends Rule> ruleClass, Script script, Expression applyTo,
								  List<Expression> parameters) throws RuleCreationException {

		Constructor<?> constructor = ruleClass.getConstructors()[0];
		try {
			List<Object> params = new ArrayList<>();
			params.add(script);
			params.add(applyTo);
			params.addAll(parameters);
			return (Rule) constructor.newInstance(params.toArray());
		} catch (IllegalArgumentException e) {
			throw new RuleCreationException(e.getMessage());
		} catch (InstantiationException e) {
			throw new RuleCreationException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new RuleCreationException(e.getMessage());
		} catch (InvocationTargetException e) {
			throw new RuleCreationException(e.getMessage());
		}
	}

	/**
	 * Checks whether a Rule creation fails and returns the exception that caused it to fail
	 * in that case.
	 *
	 * @param ruleClass  the class of the created rule
	 * @param script     the script parameter for the rule
	 * @param applyTo    the expression the rule is to be applied to
	 * @param parameters other rule parameters
	 * @return the thrown exception if one was thrown, an empty Optional otherwise
	 * @throws RuleCreationException
	 */
	public static Optional<CannotApplyRuleException> failRule(Class<? extends Rule> ruleClass, Script script,
															  Expression applyTo, List<Expression> parameters)
			throws RuleCreationException {

		Constructor<?> constructor = ruleClass.getConstructors()[0];
		try {
			List<Object> params = new ArrayList<>();
			params.add(script);
			params.add(applyTo);
			params.addAll(parameters);
			constructor.newInstance(params.toArray());
		} catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
			if (e.getCause() instanceof CannotApplyRuleException) {
				return Optional.of((CannotApplyRuleException) e.getCause());
			}
			throw new RuleCreationException(e.getMessage());
		}
		return Optional.empty();
	}

	private static class RuleCreationException extends IllegalArgumentException {

		private RuleCreationException(String message) {
			super(message);
		}

	}

}
