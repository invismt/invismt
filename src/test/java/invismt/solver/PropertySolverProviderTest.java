package invismt.solver;

import com.google.common.truth.Truth;
import invismt.solver.z3.Z3Solver;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Weigl
 * @version 1 (10/3/21)
 */
class PropertySolverProviderTest {
    PropertySolverProvider p = new PropertySolverProvider();

    @Test
    void parseSolvers1() {
        var actual = Truth.assertThat(p.parseSolvers("a=/home/weigl/bin/test.sh"));
        actual.hasSize(1);
        actual.contains(new Z3Solver("a", Collections.singletonList("/home/weigl/bin/test.sh")));
    }

    @Test
    void parseSolvers2() {
        var actual = Truth.assertThat(p.parseSolvers("cvc4=cvc4 -in --verbose"));
        actual.hasSize(1);
        actual.contains(new Z3Solver("cvc4", List.of("cvc4", "-in", "--verbose")));
    }

    @Test
    void parseSolvers3() {
        var actual = Truth.assertThat(p.parseSolvers("a=a b c;b=a b c"));
        actual.hasSize(2);
        actual.contains(new Z3Solver("a", List.of("a", "b", "c")));
        actual.contains(new Z3Solver("b", List.of("a", "b", "c")));
    }

    @Test
    void parseSolvers4() {
        var actual = Truth.assertThat(p.parseSolvers("a=a b c;b = a b c ;c = 1 2 3;"));
        actual.hasSize(3);
        actual.contains(new Z3Solver("a", List.of("a", "b", "c")));
        actual.contains(new Z3Solver("b", List.of("a", "b", "c")));
        actual.contains(new Z3Solver("c", List.of("1", "2", "3")));
    }
}