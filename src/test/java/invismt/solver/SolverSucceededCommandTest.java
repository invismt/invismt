/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.solver;

import invismt.tree.PTElement;
import invismt.tree.ProofState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test for SolverSucceededCommandTest
 */
class SolverSucceededCommandTest {

	SolverSucceededCommand solverSucceededCommand;
	PTElement ptElement;

	/**
	 * Setup up a Solver Succeeded Command
	 */
	@BeforeEach
	void setUp() {
		ptElement = new PTElement(null, null, null, null);
		SolverAnswer solverAnswer = new SolverAnswer(AnswerType.SAT, "");
		solverSucceededCommand = new SolverSucceededCommand(solverAnswer, ptElement);
	}

	/**
	 * Test if Solver Succeeded Command to Sting is Working
	 */
	@Test
	void testToString() {
		assertEquals(solverSucceededCommand.toString(), "Solver: From Unknown is " + ProofState.SAT);
	}
}