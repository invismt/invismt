/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test {@link ListOfProofObligation}
 *
 * @author Tim Junginger
 */
class ListOfProofObligationTest {

	/**
	 * Test {@link ListOfProofObligation}
	 */
	@Test
	void shouldThrowIllegalArgumentExceptionOnNullConstructor() {
		assertThrows(IllegalArgumentException.class, () -> new ListOfProofObligation(null));
	}

	/**
	 * Test {@link ListOfProofObligation}
	 */
	@Test
	void shouldNotAcceptEmptyList() {
		List<ProofObligation> list = new ArrayList<ProofObligation>();
		assertThrows(IllegalArgumentException.class, () -> new ListOfProofObligation(list));
	}

	/**
	 * Test {@link ListOfProofObligation#getActiveProofObligation()}
	 */
	@Test
	void shouldReturnCorrectActiveObligation() {
		List<ProofObligation> list = new ArrayList<ProofObligation>();
		ProofObligation obl = new ProofObligation(null);
		list.add(obl);
		ListOfProofObligation oblList = new ListOfProofObligation(list);
		assertSame(obl, oblList.getActiveProofObligation());
	}

	/**
	 * Test {@link ListOfProofObligation#setActiveProofObligation()}
	 */
	@Test
	void shouldSetCorrectActiveObligation() {
		List<ProofObligation> list = new ArrayList<ProofObligation>();
		list.add(new ProofObligation(null));
		ProofObligation obl = new ProofObligation(null);
		list.add(obl);
		list.add(new ProofObligation(null));
		ListOfProofObligation oblList = new ListOfProofObligation(list);
		assertNotSame(obl, oblList.getActiveProofObligation());
		oblList.setActiveProofObligation(obl);
		assertSame(obl, oblList.getActiveProofObligation());
	}

}
