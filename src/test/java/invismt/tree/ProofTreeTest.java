/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.tree;

import org.junit.jupiter.api.Test;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests {@link ProofTree}
 *
 * @author Tim Junginger
 */
class ProofTreeTest {

	private static boolean notified;

	/**
	 * Tests {@link ProofTree#getRootPTElement()} and {@link ProofTree#setRootPTElement(PTElement)}.
	 */
	@Test
	void shouldReturnCorrectRoot() {
		PTElement root1 = new PTElement(null, null, null, null);
		ProofTree tree = new ProofTree(root1);
		root1.setProofTree(tree);
		assertSame(root1, tree.getRootPTElement());
		PTElement root2 = new PTElement(null, null, null, tree);
		tree.setRootPTElement(root2);
		assertSame(root2, tree.getRootPTElement());
	}

	/**
	 * Tests {@link ProofTree#getActivePTElement()} and {@link ProofTree#setActivePTElement(PTElement)}.
	 */
	@Test
	void shouldReturnCorrectActive() {
		PTElement root = new PTElement(null, null, null, null);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		assertSame(root, tree.getActivePTElement());

		PTElement child = new PTElement(root, null, null, tree);
		root.addChildren(child);
		tree.setActivePTElement(child);
		assertSame(child, tree.getActivePTElement());
	}

	/**
	 * Tests {@link ProofTree#getActivePTElement()} and {@link ProofTree#setRootPTElement(PTElement)}.
	 */
	@Test
	void shouldNotReturnOldActiveAfterSetRoot() {
		PTElement root1 = new PTElement(null, null, null, null);
		ProofTree tree = new ProofTree(root1);
		root1.setProofTree(tree);
		PTElement root2 = new PTElement(null, null, null, tree);
		assertSame(root1, tree.getActivePTElement());
		tree.setRootPTElement(root2);
		assertNotSame(root1, tree.getActivePTElement());
	}

	/**
	 * Tests {@link ProofTree#setActivePTElement(PTElement)}}.
	 */
	@Test
	void shouldNotChangeActivePTElementWhenNotInTree() {
		PTElement root = new PTElement(null, null, null, null);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		PTElement element = new PTElement(null, null, null, tree);
		assertSame(root, tree.getActivePTElement());
		tree.setActivePTElement(element);
		assertSame(root, tree.getActivePTElement());
	}

	/**
	 * Tests {@link ProofTree#getActivePTElement()} and {@link ProofTree#setRootPTElement(PTElement)}.
	 */
	@Test
	void shouldNotKeepOldActiveWhenDeleted() {
		PTElement root = new PTElement(null, null, null, null);
		PTElement child = new PTElement(null, null, null, null);
		root.addChildren(child);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		child.setProofTree(tree);
		tree.setActivePTElement(child);
		root.rmChildren(child);
		assertNotSame(child, tree.getActivePTElement());
	}

	/**
	 * Tests {@link ProofTree#addChangeListener(java.beans.PropertyChangeListener)}
	 */
	@Test
	void shouldNotifyListeners() {
		notified = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				notified = true;
			}
		};
		PTElement root = new PTElement(null, null, null, null);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		tree.addChangeListener(listener);
		tree.setActivePTElement(root);
		assertEquals(true, notified);
	}

	/**
	 * Tests {@link ProofTree#addChangeListener(java.beans.PropertyChangeListener)}
	 */
	@Test
	void shouldRemoveListeners() {
		notified = false;
		PropertyChangeListener listener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				notified = true;
			}
		};
		PTElement root = new PTElement(null, null, null, null);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		tree.addChangeListener(listener);
		tree.setActivePTElement(root);
		assertEquals(true, notified);
		tree.rmChangeListener(listener);
		notified = false;
		tree.setActivePTElement(root);
		assertEquals(false, notified);
	}

	/**
	 * Tests {@link ProofTree#checkActive()}
	 */
	@Test
	void shouldRefreshActiveWhenOldActiveNewParent() {
		PTElement root = new PTElement(null, null, null, null);
		PTElement parent = new PTElement(null, null, null, null);
		PTElement child = new PTElement(null, null, null, null);
		root.addChildren(child);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		child.setProofTree(tree);
		tree.setActivePTElement(child);
		parent.addChildren(child);
		root.rmChildren(child);
		assertNotSame(child, tree.getActivePTElement());
	}

	/**
	 * Tests {@link ProofTree#checkActive()}
	 */
	@Test
	void shouldKeepOldActiveWhenCorrect() {
		PTElement root = new PTElement(null, null, null, null);
		PTElement child = new PTElement(null, null, null, null);
		root.addChildren(child);
		ProofTree tree = new ProofTree(root);
		root.setProofTree(tree);
		child.setProofTree(tree);
		tree.setActivePTElement(child);
		assertSame(child, tree.getActivePTElement());
		tree.checkActive();
		assertSame(child, tree.getActivePTElement());
	}
}
