/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.induction;

import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class InductionTest {

	private Script scriptWithTerm;
	private Token.SymbolToken induceOnVariableName;
	private QualIdentifier variable;
	private Sort integerSort;
	private Sort otherSort;
	private Term innerTerm;
	private ForAllTerm forallTermIntVariable;
	private List<SortedVariable> variables;

	@BeforeEach
	void setUp() {
		induceOnVariableName = new Token.SymbolToken("x");
		integerSort = new Sort(new Identifier(new Token.SymbolToken("Int"), new ArrayList<>()), new ArrayList<>());
		otherSort = new Sort(new Identifier(new Token.SymbolToken("NotInt"), new ArrayList<>()), new ArrayList<>());
		variable = new QualIdentifier(new Identifier(induceOnVariableName.copy(), new ArrayList<>()), Optional.empty());
		innerTerm = new IdentifierTerm(variable, new ArrayList<>());
		variables = List.of(new SortedVariable(induceOnVariableName.copy(), integerSort.copy()));
		forallTermIntVariable = new ForAllTerm(variables, innerTerm.copy());
		scriptWithTerm = new Script(List.of(new SyntaxCommand.Assert(forallTermIntVariable)));
	}

	@Nested
	class SuccessCases {

		@Test
		void shouldCreateAndApplyInductionRuleWhenParametersAreCorrect() {
			InductionRule rule = new InductionRule(scriptWithTerm, forallTermIntVariable,
					new SortedVariable(induceOnVariableName, integerSort));
			assertEquals(1, rule.getCreatedScripts().size());
			Script createdScript = rule.getCreatedScripts().get(0);
			assertNotEquals(scriptWithTerm, createdScript);
		}

	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldNotCreateInductionRuleWhenVariableTypeIsNotInt() {
			SortedVariable var = new SortedVariable(induceOnVariableName, otherSort);
			assertThrows(CannotApplyRuleException.class, () -> {
				InductionRule rule = new InductionRule(scriptWithTerm, forallTermIntVariable, var);
			});
		}

		@Test
		void shouldNotCreateInductionRuleWhenVariableIsNotInExpression() {
			SortedVariable var = new SortedVariable(new Token.SymbolToken("y"), integerSort);
			assertThrows(CannotApplyRuleException.class, () -> {
				InductionRule rule = new InductionRule(scriptWithTerm, forallTermIntVariable, var);
			});
		}

		@Test
		void shouldNotCreateInductionRuleWhenExpressionIsNotInScript() {
			SortedVariable var = new SortedVariable(induceOnVariableName, otherSort);
			ForAllTerm copy = forallTermIntVariable.copy();
			assertThrows(CannotApplyRuleException.class, () -> {
				InductionRule rule = new InductionRule(scriptWithTerm, copy, var);
			});
		}

	}

}
