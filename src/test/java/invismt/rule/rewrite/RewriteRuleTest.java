/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.rewrite;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant.StringConstant;
import invismt.grammar.Token.StringToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Tim Junginger
 */
@ExtendWith(MockitoExtension.class)
class RewriteRuleTest {

	@Mock
	Script script;
	@Mock
	Term term;
	@Mock
	Term newTerm;

	@Nested
	class SuccessCases {

		/**
		 * @throws InstantiationException
		 */
		@Test
		void shouldProduceTwoScripts() throws InstantiationException {
			// (assert true)
			ConstantTerm term = new ConstantTerm(new StringConstant(new StringToken("\"a\"")));
			ConstantTerm term2 = new ConstantTerm(new StringConstant(new StringToken("\"b\"")));
			Assert assertion = new Assert(term);
			Script script = new Script(List.of(assertion));
			// create new rule
			RewriteRule rule = new RewriteRule(script, term, term2);
			// test if only one script
			List<Script> result = rule.getCreatedScripts();
			assertEquals(2, result.size());
			Script replacedScript = result.get(0);
			Script unsatScript = result.get(1);
			// check first script
			List<Expression> exprs = replacedScript.getTopLevelExpressions();
			// test if only 2 expression
			assertEquals(2, exprs.size());
			// test if
			// (assert (= "a" "b"))
			// (assert "b")
			assert (exprs.get(0) instanceof SyntaxCommand.Assert);
			assert (exprs.get(1) instanceof SyntaxCommand.Assert);
			Assert equal = (Assert) exprs.get(0);
			Assert replaced = (Assert) exprs.get(1);

			assert (equal.getTerm() instanceof IdentifierTerm);
			IdentifierTerm identifier = (IdentifierTerm) equal.getTerm();
			assertEquals("=", identifier.getIdentifier().getQualifiedIdentifier().getSymbol().getValue());

			assertEquals(replaced.getTerm(), term2);

			// test if
			// (assert (not (= "a" "b")))
			// (assert ("a"))
		}

	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldThrowCannotApplyRuleExceptionIfTermIsNotInAssertion() {
			when(script.existsIdenticallySomewhere(any(Expression.class))).thenReturn(true);
			// Make script.getTopLevel() return something that doesn't equal an Assert command containing the term .
			when(script.getTopLevel(term)).thenReturn(term);
			assertThrows(CannotApplyRuleException.class, () -> {
				new RewriteRule(script, term, newTerm);
			});
		}

	}

}
