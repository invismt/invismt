/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.cut;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.Term;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import utils.GenericTerm;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CutRuleTest {

	private Script script;
	private GenericTerm commonTerm;
	private GenericTerm cutTerm;

	@BeforeEach
	public void setUp() {
		commonTerm = new GenericTerm();
		cutTerm = new GenericTerm();
	}

	@Nested
	class SuccessCases {
		@BeforeEach
		void setUp() {
			script = new Script(Collections.singletonList(new SyntaxCommand.Assert(commonTerm)));
		}

		@Test
		void shouldNotThrow() {
			assertDoesNotThrow(() -> {
				new CutRule(script, commonTerm, cutTerm);
			});
		}

		@Test
		void shouldCreateExactlyTwoScripts() {
			CutRule cutRule = new CutRule(script, commonTerm, cutTerm);
			assertEquals(2, cutRule.getCreatedScripts().size());
		}

		@Test
		void shouldCreateExactlyOneScriptThatContainsCutTermInAnAssertion() {
			CutRule cutRule = new CutRule(script, commonTerm, cutTerm);
			Expression cutTermAssertion = new SyntaxCommand.Assert(cutTerm);

			// Count occurrences of the cut term across all created scripts
			int occurrencesOfCutTerm = 0;
			for (Script createdScript : cutRule.getCreatedScripts()) {
				List<Expression> expressions = createdScript.getTopLevelExpressions();
				if (expressions.contains(cutTermAssertion)) {
					occurrencesOfCutTerm++;
				}
			}

			assertEquals(1, occurrencesOfCutTerm);
		}

		@Test
		void shouldCreateExactlyOneScriptThatContainsNegatedCutTermInAnAssertion() {
			CutRule cutRule = new CutRule(script, commonTerm, cutTerm);
			Term negatedCutTerm = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(), List.of(cutTerm));
			Expression negatedCutTermAssertion = new SyntaxCommand.Assert(negatedCutTerm);

			// Count occurrences of the negated cut term across all created scripts
			int occurrencesOfNegatedCutTerm = 0;
			for (Script createdScript : cutRule.getCreatedScripts()) {
				List<Expression> expressions = createdScript.getTopLevelExpressions();
				if (expressions.contains(negatedCutTermAssertion)) {
					occurrencesOfNegatedCutTerm++;
				}
			}

			assertEquals(1, occurrencesOfNegatedCutTerm);
		}

		@Test
		void shouldCreateScriptsThatEachContainCommonTermInAnAssertion() {
			CutRule cutRule = new CutRule(script, commonTerm, cutTerm);
			Expression commonTermAssertion = new SyntaxCommand.Assert(commonTerm);

			for (Script createdScript : cutRule.getCreatedScripts()) {
				List<Expression> expressions = createdScript.getTopLevelExpressions();
				assertTrue(expressions.contains(commonTermAssertion),
						String.format("Created script \"%s\" does not contain common term in an assertion",
								createdScript));
			}
		}
	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldThrowCannotApplyRuleExceptionWhenTermIsNotTopLevelInAssertion() {
			script = new Script(Collections.singletonList(commonTerm));
			assertThrows(CannotApplyRuleException.class, () -> {
				new CutRule(script, commonTerm, cutTerm);
			});
		}

	}
}