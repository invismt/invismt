/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.removeannotations;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.term.AnnotationTerm;
import invismt.grammar.term.ConstantTerm;
import invismt.rule.CannotApplyRuleException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RemoveAnnotationRuleTest {

	@Mock
	Script script;
	@Mock
	AnnotationTerm annotationTerm;

	@Nested
	class SuccessCases {

		@Test
		void shouldRemoveAnnotation() {
			AnnotationTerm term = new AnnotationTerm(
					new ConstantTerm(new SpecConstant.NumeralConstant(new Token.NumeralToken("6"))),
					List.of(new Attribute(new Token.Keyword(":named"), Optional.empty())));
			Script script = new Script(List.of(new SyntaxCommand.Assert(term)));
			RemoveAnnotationRule rule = new RemoveAnnotationRule(script, term);
			assertEquals(1, rule.getCreatedScripts().size());
			assertEquals(rule.getCreatedScripts().get(0), new Script(List.of(new SyntaxCommand.Assert(term),
					new SyntaxCommand.Assert(
							new ConstantTerm(new SpecConstant.NumeralConstant(new Token.NumeralToken("6")))))));
		}

	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldThrowCannotApplyRuleExceptionIfTermIsNotInAssertion() {
			when(script.existsIdenticallySomewhere(any(Expression.class))).thenReturn(true);
			// Make script.getTopLevel() return something that not equals an Assert command containing the term .
			when(script.getTopLevel(annotationTerm)).thenReturn(annotationTerm);

			assertThrows(CannotApplyRuleException.class, () -> {
				new RemoveAnnotationRule(script, annotationTerm);
			});
		}

	}

}