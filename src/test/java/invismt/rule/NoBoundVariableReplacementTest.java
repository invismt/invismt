/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Expression;
import invismt.grammar.Script;
import invismt.grammar.Token;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import invismt.grammar.term.QuantifiedTerm.ForAllTerm;
import invismt.grammar.term.SortedVariable;
import invismt.grammar.term.Term;
import invismt.rule.ExpressionComparator.EqualComparator;
import invismt.rule.ExpressionComparator.IdenticalComparator;
import invismt.testutil.FileToScriptUtil;
import invismt.util.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class NoBoundVariableReplacementTest {

	private static final ExpressionComparator identical = new ExpressionComparator.IdenticalComparator();
	private static final ExpressionComparator equal = new ExpressionComparator.EqualComparator();
	private ForAllTerm term;
	private ForAllTerm doubleBoundXTerm;
	private Sort sort;
	private IdentifierTerm x;
	private IdentifierTerm y;
	private IdentifierTerm equalXs;
	private Expression replacedTerm;

	@BeforeEach
	void setUp() {
		x = new IdentifierTerm(
				new QualIdentifier(new Identifier(new Token.SymbolToken("x"), new ArrayList<>()), Optional.empty()),
				new ArrayList<>());
		y = new IdentifierTerm(
				new QualIdentifier(new Identifier(new Token.SymbolToken("y"), new ArrayList<>()), Optional.empty()),
				new ArrayList<>());
		sort = new Sort(new Identifier(new Token.SymbolToken("Int"), new ArrayList<>()), new ArrayList<>());
		term = new ForAllTerm(List.of(new SortedVariable(x.getIdentifier().getQualifiedIdentifier().getSymbol(), sort)),
				x);
		doubleBoundXTerm = new ForAllTerm(term.getVariables(), term);
		equalXs = new IdentifierTerm(
				new QualIdentifier(new Identifier(new Token.SymbolToken("test"), new ArrayList<>()), Optional.empty()),
				List.of(x, x.copy(), x.copy(), x));
	}

	@Test
	void shouldOnlyReplaceIdenticalWhenMustBeIdenticalIsTrue() {
		replacedTerm = (new NoBoundVariableReplacer(x.copy(), y, identical)).substitute(x);
		assertSame(x, replacedTerm);
		replacedTerm = (new NoBoundVariableReplacer(x, y, identical).substitute(equalXs));
		assertEquals(replacedTerm, new IdentifierTerm(
				new QualIdentifier(new Identifier(new Token.SymbolToken("test"), new ArrayList<>()), Optional.empty()),
				List.of(y, x, x, y)));
		Term term = (new NoBoundVariableReplacer(x, y, identical)).substitute((Term) replacedTerm);
	}

	@Test
	void shouldReplaceEqualWhenReplacedMustBeIdenticalIsFalse() {
		replacedTerm = (new NoBoundVariableReplacer(x, y, equal)).substitute(x);
		assertNotSame(replacedTerm, x);
		replacedTerm = (new NoBoundVariableReplacer(x, y, equal)).substitute(equalXs);
		assertEquals(replacedTerm, new IdentifierTerm(
				new QualIdentifier(new Identifier(new Token.SymbolToken("test"), new ArrayList<>()), Optional.empty()),
				List.of(y, y, y, y)));
	}

	@Test
	void shouldNotReplaceBoundVariablesUnlessTheyAreBoundAnew() {
		ForAllTerm innerTermReplaced = new ForAllTerm(term.getVariables(),
				(new NoBoundVariableReplacer(x, y, equal)).substitute(term.getTerm()));
		assertEquals(innerTermReplaced, new ForAllTerm(term.getVariables(), y));
		replacedTerm = (new NoBoundVariableReplacer(term, innerTermReplaced, identical)).substitute(doubleBoundXTerm);
		assertNotSame(doubleBoundXTerm, replacedTerm);
		assertNotEquals(doubleBoundXTerm, replacedTerm);
		assertEquals(replacedTerm, new ForAllTerm(term.getVariables(), new ForAllTerm(term.getVariables(), y)));
	}

	@Test
	void shouldBeIdenticalWhenNothingIsReplaced() {
		replacedTerm = (new NoBoundVariableReplacer(y, y, identical)).substitute(x);
		assertSame(replacedTerm, x);
	}

	@Nested
	class Parameterized {

		private static Stream<Arguments> replacementTerms() {
			return Stream.of(arguments("replace.smt2", "params.smt2", "compare.smt2"));
		}

		private static Stream<Arguments> noReplacementTerms() {
			return Stream.of(arguments("replace2.smt2", "params2.smt2"));
		}

		/**
		 * Test whether a {@link NoBoundVariableReplacer} replaces correctly.
		 * The test files are compared line by line:
		 * Each line in filePath has to contain one top level expression with one marked expression
		 * which is the old expression to be replaced. The replacer is then called on the top level
		 * expression in that line and the result is compared with the parsed expression in compareFilePath.
		 * The new term and whether identical or equal expressions are to be replaced is found in parameterFilePath.
		 * <p>
		 * {@see FileToScriptUtil} for more information on the syntax and methods used for parsing.
		 *
		 * @param filePath          the path with the old terms and the terms to call {@link TermReplacer#substitute(Term)} on
		 * @param parameterFilePath the path with the new terms to replace the old terms by and whether
		 *                          they are to be replaced only identically or equally
		 * @param compareFilePath   the path with the expected replacement results
		 * @throws IOException
		 */
		@ParameterizedTest
		@MethodSource("replacementTerms")
		void shouldReplaceTermsCorrectly(String filePath, String parameterFilePath, String compareFilePath)
				throws IOException {
			Path file = FileToScriptUtil.getFile("replacerTestFiles/" + filePath);
			Path compare = FileToScriptUtil.getFile("replacerTestFiles/" + compareFilePath);
			Path parameters = FileToScriptUtil.getFile("replacerTestFiles/" + parameterFilePath);
			Script compareTo = FileToScriptUtil.toScript(compare);
			List<Tuple<Term, Boolean>> otherParams = FileToScriptUtil.getReplacementParameters(parameters);
			Tuple<Script, List<Term>> parsed = FileToScriptUtil.getMarkedExpressionsScript(file);
			for (int i = 0; i < parsed.getSecond().size(); i++) {
				Tuple<Term, Boolean> other = otherParams.get(i);
				ExpressionComparator comparator;
				if (other.getSecond()) {
					comparator = new IdenticalComparator();
				} else {
					comparator = new EqualComparator();
				}
				Term top = (Term) parsed.getFirst().getParentChain(parsed.getSecond().get(i)).get(1);
				TermReplacer replacer = new NoBoundVariableReplacer(
						(Term) parsed.getSecond().get(i),
						other.getFirst(), comparator);
				assertTrue(replacer.applicable(top));
				assertEquals(compareTo.getTopLevelExpressions().get(i), new Assert(replacer.substitute(top)));
			}
		}

		/**
		 * Test whether a {@link NoBoundVariableReplacer} fails to replace correctly.
		 * <p>
		 * {@see FileToScriptUtil} for more information on the syntax and methods used for parsing.
		 *
		 * @param filePath          the path with the old terms and the terms to call {@link TermReplacer#substitute(Term)} on
		 * @param parameterFilePath the path with the new terms to replace the old terms by and whether
		 *                          they are to be replaced only identically or equally
		 * @throws IOException
		 */
		@ParameterizedTest
		@MethodSource("noReplacementTerms")
		void shouldNotReplaceTerms(String filePath, String parameterFilePath)
				throws IOException {
			Path file = FileToScriptUtil.getFile("replacerTestFiles/" + filePath);
			Path parameters = FileToScriptUtil.getFile("replacerTestFiles/" + parameterFilePath);
			List<Tuple<Term, Boolean>> otherParams = FileToScriptUtil.getReplacementParameters(parameters);
			Tuple<Script, List<Term>> parsed = FileToScriptUtil.getMarkedExpressionsScript(file);
			for (int i = 0; i < parsed.getSecond().size(); i++) {
				Tuple<Term, Boolean> other = otherParams.get(i);
				ExpressionComparator comparator;
				if (other.getSecond()) {
					comparator = new IdenticalComparator();
				} else {
					comparator = new EqualComparator();
				}
				Term top = (Term) parsed.getFirst().getParentChain(parsed.getSecond().get(i)).get(1);
				TermReplacer replacer = new NoBoundVariableReplacer(
						(Term) parsed.getSecond().get(i),
						other.getFirst(), comparator);
				assertFalse(replacer.applicable(top));
				assertSame(top, replacer.substitute(top));
			}
		}

	}


}
