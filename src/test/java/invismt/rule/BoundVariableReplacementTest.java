/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule;

import invismt.grammar.Script;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.Term;
import invismt.rule.ExpressionComparator.EqualComparator;
import invismt.rule.ExpressionComparator.IdenticalComparator;
import invismt.testutil.FileToScriptUtil;
import invismt.util.Tuple;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class BoundVariableReplacementTest {

	@Nested
	class Parameterized {
		private static Stream<Arguments> replacementTerms() {
			return Stream.of(arguments("replace.smt2", "params.smt2", "compare.smt2"));
		}

		private static Stream<Arguments> noReplacementTerms() {
			return Stream.of(arguments("replace3.smt2", "params3.smt2"));
		}

		/**
		 * Test whether a {@link BoundVariableReplacer} replaces correctly.
		 * The test files are compared line by line:
		 * Each line in filePath has to contain one top level expression with one marked expression
		 * which is the old expression to be replaced. The replacer is then called on the top level
		 * expression in that line and the result is compared with the parsed expression in compareFilePath.
		 * The new term and whether identical or equal expressions are to be replaced is found in parameterFilePath.
		 * <p>
		 * {@see FileToScriptUtil} for more information on the syntax and methods used for parsing.
		 *
		 * @param filePath          the path with the old terms and the terms to call {@link TermReplacer#substitute(Term)} on
		 * @param parameterFilePath the path with the new terms to replace the old terms by and whether
		 *                          they are to be replaced only identically or equally
		 * @param compareFilePath   the path with the expected replacement results
		 * @throws IOException
		 */
		@ParameterizedTest
		@MethodSource("replacementTerms")
		void shouldReplaceTermsCorrectly(String filePath, String parameterFilePath, String compareFilePath)
				throws IOException {
			Path file = FileToScriptUtil.getFile("replacerTestFiles/" + filePath);
			Path compare = FileToScriptUtil.getFile("replacerTestFiles/" + compareFilePath);
			Path parameters = FileToScriptUtil.getFile("replacerTestFiles/" + parameterFilePath);
			Script compareTo = FileToScriptUtil.toScript(compare);
			List<Tuple<Term, Boolean>> otherParams = FileToScriptUtil.getReplacementParameters(parameters);
			Tuple<Script, List<Term>> parsed = FileToScriptUtil.getMarkedExpressionsScript(file);
			for (int i = 0; i < parsed.getSecond().size(); i++) {
				Tuple<Term, Boolean> other = otherParams.get(i);
				ExpressionComparator comparator;
				if (other.getSecond()) {
					comparator = new IdenticalComparator();
				} else {
					comparator = new EqualComparator();
				}
				Term top = (Term) parsed.getFirst().getParentChain(parsed.getSecond().get(i)).get(1);
				TermReplacer replacer = new BoundVariableReplacer(parsed.getSecond().get(i), other.getFirst(),
						comparator);
				assertTrue(replacer.applicable(top));
				assertEquals(compareTo.getTopLevelExpressions().get(i), new Assert(replacer.substitute(top)));
			}
		}

		/**
		 * Test whether a {@link BoundVariableReplacer} fails to replace correctly.
		 * <p>
		 * {@see FileToScriptUtil} for more information on the syntax and methods used for parsing.
		 *
		 * @param filePath          the path with the old terms and the terms to call {@link TermReplacer#substitute(Term)} on
		 * @param parameterFilePath the path with the new terms to replace the old terms by and whether
		 *                          they are to be replaced only identically or equally
		 * @throws IOException
		 */
		@ParameterizedTest
		@MethodSource("noReplacementTerms")
		void shouldNotReplaceTerms(String filePath, String parameterFilePath)
				throws IOException {
			Path file = FileToScriptUtil.getFile("replacerTestFiles/" + filePath);
			Path parameters = FileToScriptUtil.getFile("replacerTestFiles/" + parameterFilePath);
			List<Tuple<Term, Boolean>> otherParams = FileToScriptUtil.getReplacementParameters(parameters);
			Tuple<Script, List<Term>> parsed = FileToScriptUtil.getMarkedExpressionsScript(file);
			for (int i = 0; i < parsed.getSecond().size(); i++) {
				Tuple<Term, Boolean> other = otherParams.get(i);
				ExpressionComparator comparator;
				if (other.getSecond()) {
					comparator = new IdenticalComparator();
				} else {
					comparator = new EqualComparator();
				}
				Term top = (Term) parsed.getFirst().getParentChain(parsed.getSecond().get(i)).get(1);
				TermReplacer replacer = new BoundVariableReplacer(parsed.getSecond().get(i), other.getFirst(),
						comparator);
				assertFalse(replacer.applicable(top));
				assertSame(top, replacer.substitute(top));
			}
		}

	}

}
