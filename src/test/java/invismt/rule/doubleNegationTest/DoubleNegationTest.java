/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.doubleNegationTest;

import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant.NumeralConstant;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.commands.SyntaxCommand.Assert;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.rule.CannotApplyRuleException;
import invismt.rule.doublenegation.DoubleNegationRule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DoubleNegationTest {

	IdentifierTerm notTerm;
	IdentifierTerm notNotTerm;
	IdentifierTerm notNotNotTerm;
	Script script;

	@BeforeEach
	public void setUp() {
		notTerm = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(),
				List.of(new ConstantTerm(new NumeralConstant(new NumeralToken("42")))));
		notNotTerm = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(), List.of(notTerm.deepCopy()));
		notNotNotTerm = new IdentifierTerm(ExpressionFactory.NOT.deepCopy(), List.of(notNotTerm.deepCopy()));
		script = new Script(List.of(new Assert(notTerm), new Assert(notNotTerm), new Assert(notNotNotTerm)));
	}

	@Nested
	class SuccessCases {

		@Test
		void shouldOnlyRemoveTwoNots() {
			DoubleNegationRule rule = new DoubleNegationRule(script, notNotNotTerm);
			assertEquals(3, rule.getCreatedScripts().get(0).getTopLevelExpressions().size());
			assertEquals(new Assert(notTerm), rule.getCreatedScripts().get(0).getTopLevelExpressions().get(2));
			rule = new DoubleNegationRule(script, notNotTerm);
			assertEquals(3, rule.getCreatedScripts().get(0).getTopLevelExpressions().size());
			assertEquals(new Assert(new ConstantTerm(new NumeralConstant(new NumeralToken("42")))),
					rule.getCreatedScripts().get(0).getTopLevelExpressions().get(1));
		}

	}

	@Nested
	class ExceptionCases {

		@Test
		void shouldNotCreateDoubleNegationRuleWhenExpressionIsNotInAssertion() {
			Script newScript = new Script(List.of(notNotTerm));
			assertThrows(CannotApplyRuleException.class, () -> {
				DoubleNegationRule rule = new DoubleNegationRule(newScript, notNotTerm);
			});
		}


		@Test
		void shouldNotCreateDoubleNegationRuleWhenThereArentTwoNotsInARow() {
			assertThrows(CannotApplyRuleException.class, () -> {
				DoubleNegationRule rule = new DoubleNegationRule(script, notTerm);
			});
		}

	}
}
