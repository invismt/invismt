/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.rule.demorgan;

import invismt.grammar.Expression;
import invismt.grammar.ExpressionFactory;
import invismt.grammar.Script;
import invismt.grammar.SpecConstant.NumeralConstant;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Index.NumeralIndex;
import invismt.grammar.term.ConstantTerm;
import invismt.grammar.term.IdentifierTerm;
import invismt.grammar.term.QualIdentifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class DeMorganRuleTest {

	private Script script;
	private ConstantTerm termA;
	private ConstantTerm termB;
	private IdentifierTerm notTermA;
	private IdentifierTerm notTermB;

	@BeforeEach
	void setUp() {
		termA = new ConstantTerm(new NumeralConstant(new NumeralToken("42")));
		termB = new ConstantTerm(new NumeralConstant(new NumeralToken("420")));
		notTermA = new IdentifierTerm(ExpressionFactory.NOT, Collections.singletonList(termA));
		notTermB = new IdentifierTerm(ExpressionFactory.NOT, Collections.singletonList(termB));
	}

	@Nested
	class TermIsNegationOfDisjunction {

		IdentifierTerm term;

		@BeforeEach
		void setUp() {
			IdentifierTerm disjunction = new IdentifierTerm(ExpressionFactory.OR, Arrays.asList(termA, termB));
			term = new IdentifierTerm(ExpressionFactory.NOT, Collections.singletonList(disjunction));
			script = new Script(Collections.singletonList(new SyntaxCommand.Assert(term)));
		}

		@Test
		void shouldNotThrow() {
			assertDoesNotThrow(() -> {
				new DeMorganRule(script, term);
			});
		}

		@Test
		void shouldCreateExactlyOneScript() {
			DeMorganRule deMorganRule = new DeMorganRule(script, term);
			assertEquals(1, deMorganRule.getCreatedScripts().size());
		}

		@Test
		void shouldCreateAScriptThatContainsTheConjunctionOfTheNegationsOfBothTerms() {
			DeMorganRule deMorganRule = new DeMorganRule(script, term);
			IdentifierTerm conjunctionOfNegations = new IdentifierTerm(ExpressionFactory.AND,
					Arrays.asList(notTermA, notTermB));
			Expression conjunctionOfNegationsAssertion = new SyntaxCommand.Assert(conjunctionOfNegations);

			Script createdScript = deMorganRule.getCreatedScripts().get(0);
			// TODO: GenericTerm does not implement equals method -> contains will always fail except for identical occurences
			assertTrue(createdScript.getTopLevelExpressions().contains(conjunctionOfNegationsAssertion));
		}

		@Test
		void shouldNotCreateAScriptThatContainsTheOriginalTermInAnAssertion() {
			DeMorganRule deMorganRule = new DeMorganRule(script, term);
			Script createdScript = deMorganRule.getCreatedScripts().get(0);
			Expression termAssertion = new SyntaxCommand.Assert(term);
			assertFalse(createdScript.getTopLevelExpressions().contains(termAssertion));
		}
	}

	@Nested
	class TermIsNegationOfConjunction {

		IdentifierTerm term;

		@BeforeEach
		void setUp() {
			IdentifierTerm conjunction = new IdentifierTerm(ExpressionFactory.AND, Arrays.asList(termA, termB));
			term = new IdentifierTerm(ExpressionFactory.NOT, Collections.singletonList(conjunction));
			script = new Script(Collections.singletonList(new SyntaxCommand.Assert(term)));
		}

		@Test
		void shouldNotThrow() {
			assertDoesNotThrow(() -> {
				new DeMorganRule(script, term);
			});
		}

		@Test
		void shouldCreateExactlyOneScript() {
			DeMorganRule deMorganRule = new DeMorganRule(script, term);
			assertEquals(1, deMorganRule.getCreatedScripts().size());
		}

		@Test
		void shouldCreateAScriptThatContainsTheDisjunctionOfTheNegationsOfBothTerms() {
			DeMorganRule deMorganRule = new DeMorganRule(script, term);
			IdentifierTerm disjunctionOfNegations = new IdentifierTerm(ExpressionFactory.OR,
					Arrays.asList(notTermA, notTermB));
			Expression disjunctionOfNegationsAssertion = new SyntaxCommand.Assert(disjunctionOfNegations);
			Script createdScript = deMorganRule.getCreatedScripts().get(0);
			assertTrue(createdScript.getTopLevelExpressions().contains(disjunctionOfNegationsAssertion));
		}

		@Test
		void shouldNotCreateAScriptThatContainsTheOriginalTermInAnAssertion() {
			DeMorganRule deMorganRule = new DeMorganRule(script, term);
			Script createdScript = deMorganRule.getCreatedScripts().get(0);
			Expression termAssertion = new SyntaxCommand.Assert(term);
			assertFalse(createdScript.getTopLevelExpressions().contains(termAssertion));
		}
	}

	@Nested
	class TermIsARandomIdentifierTerm {

		IdentifierTerm term;

		@BeforeEach
		void setUp() {
			term = new IdentifierTerm(new QualIdentifier(ExpressionFactory.makeIdentifier("and", List.of(new NumeralIndex(new NumeralToken("7")))), Optional.empty()), Arrays.asList(termA, termB));
			script = new Script(Collections.singletonList(new SyntaxCommand.Assert(term)));
		}

		@Test
		void shouldThrowIllegalArgumentException() {
			assertThrows(IllegalArgumentException.class, () -> {
				new DeMorganRule(script, term);
			});
		}
	}
}