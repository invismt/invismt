/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.*;
import invismt.grammar.SpecConstant.*;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue.ConstantAttributeValue;
import invismt.grammar.attributes.AttributeValue.ListAttributeValue;
import invismt.grammar.attributes.AttributeValue.SymbolAttributeValue;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index.NumeralIndex;
import invismt.grammar.identifiers.Index.SymbolIndex;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;
import invismt.parser.exceptions.SMTLIBParsingException;
import invismt.tree.ListOfProofObligation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests {@link SMTLIBProblemBuilder}
 *
 * @author Tim Junginger
 */
class ProblemBuilderTest {

	private static final SymbolToken X = new SymbolToken("x");
	private static final Sort INTEGER = new Sort(new Identifier(new SymbolToken("Int"), List.of()), List.of());
	private SMTLIBProblemBuilder builder;

	/**
	 * Instantiate the builder each time anew.
	 */
	@BeforeEach
	void init() {
		builder = new SMTLIBProblemBuilder();
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#visit}
	 */
	@Test
	void shouldThrowExceptionOnNotTopLevelExpressions() {
		assertThrows(RuntimeException.class, () -> builder.visit((QuantifiedTerm.ForAllTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((QuantifiedTerm.ExistsTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((LetTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((AnnotationTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((MatchTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((ConstantTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((IdentifierTerm) null));
		assertThrows(RuntimeException.class, () -> builder.visit((VariableBinding) null));
		assertThrows(RuntimeException.class, () -> builder.visit((SortedVariable) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Identifier) null));
		assertThrows(RuntimeException.class, () -> builder.visit((NumeralIndex) null));
		assertThrows(RuntimeException.class, () -> builder.visit((SymbolIndex) null));
		assertThrows(RuntimeException.class, () -> builder.visit((QualIdentifier) null));
		assertThrows(RuntimeException.class, () -> builder.visit((ListAttributeValue) null));
		assertThrows(RuntimeException.class, () -> builder.visit((ConstantAttributeValue) null));
		assertThrows(RuntimeException.class, () -> builder.visit((SymbolAttributeValue) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Attribute) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.NumeralToken) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.StringToken) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.BinaryToken) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.DecimalToken) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.HexadecimalToken) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.SymbolToken) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.ReservedWord) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Token.Keyword) null));
		assertThrows(RuntimeException.class, () -> builder.visit((NumeralConstant) null));
		assertThrows(RuntimeException.class, () -> builder.visit((StringConstant) null));
		assertThrows(RuntimeException.class, () -> builder.visit((DecimalConstant) null));
		assertThrows(RuntimeException.class, () -> builder.visit((HexadecimalConstant) null));
		assertThrows(RuntimeException.class, () -> builder.visit((BinaryConstant) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Sort) null));
		assertThrows(RuntimeException.class, () -> builder.visit((Pattern) null));
		assertThrows(RuntimeException.class, () -> builder.visit((MatchCase) null));
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldNotCreateProblemFromOnlyCheckSat() {
		List<Expression> commands = List.of(new SyntaxCommand.CheckSat());
		assertThrows(SMTLIBParsingException.class, () -> builder.buildProblems(commands));
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldCreateOneProblemFromAssertAndCheckSat() {
		List<Expression> commands = List.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(1, list.getProofObligations().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldCreateTwoProblemsFromTwoCheckSat() {
		List<Expression> commands = List.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat(),
				new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(2, list.getProofObligations().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldOnlyCreateProblemsUntilExit() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat(), new SyntaxCommand.Exit(),
						new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(1, list.getProofObligations().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldAddDeclareConstantToProblem() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.DeclareConstant(X, INTEGER),
						new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(1, list.getProofObligations().size());
		Script script = list.getProofObligations().get(0).getProofTree().getRootPTElement().getScript();
		assertEquals(2, script.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldAddDeclareFunctionToProblem() {
		List<Expression> commands = List.of(new SyntaxCommand.Assert(new FakeTerm()),
				new SyntaxCommand.DeclareFunction(X, List.of(INTEGER.deepCopy()), INTEGER.deepCopy()),
				new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(1, list.getProofObligations().size());
		Script script = list.getProofObligations().get(0).getProofTree().getRootPTElement().getScript();
		assertEquals(2, script.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldCreateProblemWithoutAssert() {
		List<Expression> commands = List
				.of(new SyntaxCommand.DeclareConstant(X, INTEGER), new SyntaxCommand.CheckSat());
		ListOfProofObligation obligations = builder.buildProblems(commands);
		assertNotNull(obligations);
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldAddCommandOnLowerLevelToHigherAssertionLevels() {
		List<Expression> commands = List.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat(),
				new SyntaxCommand.Push(new NumeralToken("1")), new SyntaxCommand.Assert(new FakeTerm()),
				new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(2, list.getProofObligations().size());
		// get the script of the second problem
		Script script = list.getProofObligations().get(1).getProofTree().getRootPTElement().getScript();
		// should contain both FakeTerms
		assertEquals(2, script.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldRemoveAllAssertionsByReset() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.Reset(), new SyntaxCommand.CheckSat());
		assertThrows(SMTLIBParsingException.class, () -> builder.buildProblems(commands));
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldRemoveAllDeclarationsByReset() {
		List<Expression> commands = List.of(new SyntaxCommand.DeclareConstant(X, INTEGER), new SyntaxCommand.Reset(),
				new SyntaxCommand.CheckSat());
		assertThrows(SMTLIBParsingException.class, () -> builder.buildProblems(commands));
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldNotRemoveLowestAssertionLevelWithPop() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.Pop(new NumeralToken("1")),
						new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(1, list.getProofObligations().size());
		Script script = list.getProofObligations().get(0).getProofTree().getRootPTElement().getScript();
		assertEquals(1, script.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldRemoveAssertionsByPop() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Push(new NumeralToken("1")), new SyntaxCommand.Assert(new FakeTerm()),
						new SyntaxCommand.CheckSat(), new SyntaxCommand.Pop(new NumeralToken("1")),
						new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(2, list.getProofObligations().size());
		// get second script
		Script script = list.getProofObligations().get(1).getProofTree().getRootPTElement().getScript();
		// should only contain last term
		assertEquals(1, script.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldRemoveMultipleLevelsAtOnceWithPop() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Push(new NumeralToken("1")), new SyntaxCommand.Assert(new FakeTerm()),
						new SyntaxCommand.CheckSat(), new SyntaxCommand.Push(new NumeralToken("1")),
						new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat(),
						new SyntaxCommand.Pop(new NumeralToken("2")), new SyntaxCommand.Assert(new FakeTerm()),
						new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(3, list.getProofObligations().size());
		// get last script
		Script script = list.getProofObligations().get(2).getProofTree().getRootPTElement().getScript();
		// should only contain last term
		assertEquals(1, script.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldAddGeneralExpressionToCurrentLevel() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Push(new NumeralToken("1")), new GeneralExpression("", false),
						new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat(),
						new SyntaxCommand.Pop(new NumeralToken("1")), new SyntaxCommand.Assert(new FakeTerm()),
						new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(2, list.getProofObligations().size());
		// get scripts
		Script script1 = list.getProofObligations().get(0).getProofTree().getRootPTElement().getScript();
		Script script2 = list.getProofObligations().get(1).getProofTree().getRootPTElement().getScript();
		assertEquals(2, script1.getTopLevelExpressions().size());
		assertEquals(1, script2.getTopLevelExpressions().size());
	}

	/**
	 * Tests {@link SMTLIBProblemBuilder#buildProblems(List)}
	 */
	@Test
	void shouldAddGeneralExpressionToAllProblems() {
		List<Expression> commands = List
				.of(new SyntaxCommand.Push(new NumeralToken("1")), new GeneralExpression("", true),
						new SyntaxCommand.Assert(new FakeTerm()), new SyntaxCommand.CheckSat(),
						new SyntaxCommand.Pop(new NumeralToken("1")), new SyntaxCommand.Assert(new FakeTerm()),
						new SyntaxCommand.CheckSat());
		ListOfProofObligation list = builder.buildProblems(commands);
		assertEquals(2, list.getProofObligations().size());
		// get scripts
		Script script1 = list.getProofObligations().get(0).getProofTree().getRootPTElement().getScript();
		Script script2 = list.getProofObligations().get(1).getProofTree().getRootPTElement().getScript();
		assertEquals(2, script1.getTopLevelExpressions().size());
		assertEquals(2, script2.getTopLevelExpressions().size());
	}

	private static class FakeTerm implements Term {
		@Override
		public List<Expression> getChildren() {
			return List.of();
		}

		@Override
		public <T> T accept(ExpressionVisitor<T> visitor) {
			return null;
		}

		@Override
		public List<SymbolToken> getBoundVariables() {
			return List.of();
		}

		@Override
		public List<SymbolToken> getFreeVariables() {
			return List.of();
		}

		@Override
		public Term copy() {
			throw new IllegalStateException("Called method copy() in FakeTerm.");
		}

		@Override
		public Term deepCopy() {
			throw new IllegalStateException("Called method deepCopy() in FakeTerm.");
		}
	}

}
