/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.Expression;
import invismt.grammar.GeneralExpression;
import invismt.grammar.Token.NumeralToken;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.commands.SyntaxCommand;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import invismt.parser.exceptions.SMTLIBParsingException;
import invismt.testutil.FileToScriptUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

/**
 * Tests {@link ScriptParserDispatcher}. This class also (indirectly) tests
 * {@link SMTLIBExpressionProductionVisitor} as that cannot be easily tested.
 * This class does not test parsing of terms as this is done in
 * {@link TermParseTest}.
 *
 * @author Tim Junginger
 */
class ScriptParserTest {

	private final Sort bool = new Sort(new Identifier(new SymbolToken("Bool"), List.of()), List.of());
	private final NumeralToken one = new NumeralToken("1");
	private ScriptParserDispatcher parser;

	private static Stream<Arguments> filePaths() {
		List<Arguments> list = new ArrayList<>();
		File f = new File("src/test/resources/smtexamples/SMT");
		for (String path: f.list()) {
			list.add(arguments("../smtexamples/SMT/" + path));
		}
		list.add(arguments("importantConstructs.smt2"));
		return list.stream();
	}

	@BeforeEach
	void init() {
		parser = new ScriptParserDispatcher();
	}

	@ParameterizedTest
	@MethodSource("filePaths")
	void shouldParseWithoutExceptions(String filePath) {
		Path path = FileToScriptUtil.getFile("smtlibParserTestFiles/" + filePath);
		assertDoesNotThrow(() -> {
			parser.parseInto(Files.readString(path));
		});
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_declareConst(invismt.parser.antlr.SMTLIBv2Parser.Cmd_declareConstContext)
	 */
	@Test
	void shouldParseDeclareConstant() {
		String content = "(declare-const x Bool)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.DeclareConstant(new SymbolToken("x"), bool), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_declareFun(invismt.parser.antlr.SMTLIBv2Parser.Cmd_declareFunContext)
	 */
	@Test
	void shouldParseDeclareFunction() {
		String content = "(declare-fun x (Bool) Bool)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.DeclareFunction(new SymbolToken("x"), List.of(bool), bool), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_exit(invismt.parser.antlr.SMTLIBv2Parser.Cmd_exitContext)
	 */
	@Test
	void shouldParseExitCorrectly() {
		String content = "(exit)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.Exit(), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_push(invismt.parser.antlr.SMTLIBv2Parser.Cmd_pushContext)
	 */
	@Test
	void shouldParsePushCorrectly() {
		String content = "(push 1)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.Push(one), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_pop(invismt.parser.antlr.SMTLIBv2Parser.Cmd_popContext)
	 */
	@Test
	void shouldParsePopCorrectly() {
		String content = "(pop 1)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.Pop(one), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_checkSat(invismt.parser.antlr.SMTLIBv2Parser.Cmd_checkSatContext)
	 */
	@Test
	void shouldParseCheckSatCorrectly() {
		String content = "(check-sat)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.CheckSat(), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_reset(invismt.parser.antlr.SMTLIBv2Parser.Cmd_resetContext)
	 */
	@Test
	void shouldParseResetCorrectly() {
		String content = "(reset)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new SyntaxCommand.Reset(), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_assert(invismt.parser.antlr.SMTLIBv2Parser.Cmd_assertContext)
	 */
	@Test
	void shouldNotParseNonTopLevelExpression() {
		String content = "(and true true)";
		assertThrows(SMTLIBParsingException.class, () -> parser.parseInto(content));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_declareDatatype(invismt.parser.antlr.SMTLIBv2Parser.Cmd_declareDatatypeContext)
	 */
	@Test
	void shouldParseDeclareDatatypeToGeneralExpression() {
		String content = "(declare-datatype T ((S)))";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_declareDatatypes(invismt.parser.antlr.SMTLIBv2Parser.Cmd_declareDatatypesContext)
	 */
	@Test
	void shouldParseDeclareDatatypesToGeneralExpression() {
		String content = "(declare-datatypes ((T 0)) (((S))))";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_declareSort(invismt.parser.antlr.SMTLIBv2Parser.Cmd_declareSortContext)
	 */
	@Test
	void shouldParseDeclareSortToGeneralExpression() {
		String content = "(declare-sort S 0)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_defineSort(invismt.parser.antlr.SMTLIBv2Parser.Cmd_defineSortContext)
	 */
	@Test
	void shouldParseDefineSortToGeneralExpression() {
		String content = "(define-sort S () R)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_defineFun(invismt.parser.antlr.SMTLIBv2Parser.Cmd_defineFunContext)
	 */
	@Test
	void shouldParseDefineFunToGeneralExpression() {
		String content = "(define-fun first ((a Bool) (b Bool)) Bool a)";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_defineFunRec(invismt.parser.antlr.SMTLIBv2Parser.Cmd_defineFunRecContext)
	 */
	@Test
	void shouldParseDefineFunRecToGeneralExpression() {
		String content = "(define-fun-rec increm ((a Int)) Int (increm a))";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

	/**
	 * Tests {@link ScriptParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitCmd_defineFunsRec(invismt.parser.antlr.SMTLIBv2Parser.Cmd_defineFunsRecContext)
	 */
	@Test
	void shouldParseDefineFunsRecToGeneralExpression() {
		String content = "(define-funs-rec ((f ((a Int)) Int) (g ((x Int) (y Int)) Int)) (0 1))";
		List<Expression> expressions = parser.parseInto(content);
		assertEquals(1, expressions.size());
		assertEquals(new GeneralExpression(content, false), expressions.get(0));
	}

}
