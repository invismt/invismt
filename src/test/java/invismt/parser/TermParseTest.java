/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 *
 * This file is part of InViSMT.
 *
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.SpecConstant;
import invismt.grammar.Token;
import invismt.grammar.Token.*;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.identifiers.Index;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.*;
import invismt.parser.exceptions.SMTLIBParsingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests {@link TermParserDispatcher} and
 * {@link SMTLIBExpressionProductionVisitor}.
 *
 * @author Tim Junginger
 */
class TermParseTest {

	private static final QualIdentifier X = new QualIdentifier(new Identifier(new SymbolToken("x"), List.of()),
			Optional.empty());
	private static final QualIdentifier TRUE = new QualIdentifier(new Identifier(new SymbolToken("true"), List.of()),
			Optional.empty());
	private static final Sort BOOL = new Sort(new Identifier(new SymbolToken("Bool"), List.of()), List.of());
	private TermParserDispatcher parser;

	/**
	 * Initialize parser for each test anew.
	 */
	@BeforeEach
	void initialize() {
		parser = new TermParserDispatcher();
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 */
	@Test
	void shouldThrowExceptionTest() {
		assertThrows(SMTLIBParsingException.class, () -> {
			parser.parseInto(") )");
		});
		assertThrows(SMTLIBParsingException.class, () -> {
			parser.parseInto("identifier term");
		});
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitString(invismt.parser.antlr.SMTLIBv2Parser.StringContext)
	 */
	@Test
	void shouldParseConstantTermStringCorrectly() {
		String content = "\"string\"";
		Term term = parser.parseInto(content);
		assertEquals(new ConstantTerm(new SpecConstant.StringConstant(new StringToken(content))), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitNumeral(invismt.parser.antlr.SMTLIBv2Parser.NumeralContext)
	 */
	@Test
	void shouldParseConstantTermNumeralCorrectly() {
		String content = "1";
		Term term = parser.parseInto(content);
		assertEquals(new ConstantTerm(new SpecConstant.NumeralConstant(new NumeralToken(content))), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitHexadecimal(invismt.parser.antlr.SMTLIBv2Parser.HexadecimalContext)
	 */
	@Test
	void shouldParseConstantTermHexadecimalCorrectly() {
		String content = "#x0";
		Term term = parser.parseInto(content);
		assertEquals(new ConstantTerm(new SpecConstant.HexadecimalConstant(new HexadecimalToken(content))), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitBinary(invismt.parser.antlr.SMTLIBv2Parser.BinaryContext)
	 */
	@Test
	void shouldParseConstantTermBinaryCorrectly() {
		String content = "#b00";
		Term term = parser.parseInto(content);
		assertEquals(new ConstantTerm(new SpecConstant.BinaryConstant(new BinaryToken(content))), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitDecimal(invismt.parser.antlr.SMTLIBv2Parser.DecimalContext)
	 */
	@Test
	void shouldParseConstantTermDecimalCorrectly() {
		String content = "1.5";
		Term term = parser.parseInto(content);
		assertEquals(new ConstantTerm(new SpecConstant.DecimalConstant(new DecimalToken(content))), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitTerm(invismt.parser.antlr.SMTLIBv2Parser.TermContext)
	 * @see SMTLIBExpressionProductionVisitor#visitIdentifier(invismt.parser.antlr.SMTLIBv2Parser.IdentifierContext)
	 */
	@Test
	void shouldParseIdentifierTermCorrectly() {
		String content = "x";
		Term term = parser.parseInto(content);
		assertEquals(new IdentifierTerm(X, List.of()), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitTerm(invismt.parser.antlr.SMTLIBv2Parser.TermContext)
	 * @see SMTLIBExpressionProductionVisitor#visitIdentifier(invismt.parser.antlr.SMTLIBv2Parser.IdentifierContext)
	 */
	@Test
	void shouldParseIdentifierTermWithIndexCorrectly() {
		String content = "(_ x 5)";
		Term term = parser.parseInto(content);
		assertEquals(new IdentifierTerm(new QualIdentifier(
				new Identifier(new SymbolToken("x"), List.of(new Index.NumeralIndex(new NumeralToken("5")))),
				Optional.empty()), List.of()), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitTerm(invismt.parser.antlr.SMTLIBv2Parser.TermContext)
	 * @see SMTLIBExpressionProductionVisitor#visitIdentifier(invismt.parser.antlr.SMTLIBv2Parser.IdentifierContext)
	 */
	@Test
	void shouldParseIdentifierTermWithSymbolIndexCorrectly() {
		String content = "(_ x a)";
		Term term = parser.parseInto(content);
		assertEquals(1, ((IdentifierTerm) term).getIdentifier().getQualifiedIdentifier().getIndices().size());
		assertSame(Index.SymbolIndex.class,
				((IdentifierTerm) term).getIdentifier().getQualifiedIdentifier().getIndices().get(0).getClass());
		assertEquals(new IdentifierTerm(new QualIdentifier(
				new Identifier(new SymbolToken("x"), List.of(new Index.SymbolIndex(new SymbolToken("a")))),
				Optional.empty()), List.of()), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitLet_term(invismt.parser.antlr.SMTLIBv2Parser.Let_termContext)
	 */
	@Test
	void shouldParseLetTermCorrectly() {
		String content = "(let ((h true)) h)";
		Term term = parser.parseInto(content);
		assertEquals(
				new LetTerm(List.of(new VariableBinding(new SymbolToken("h"), new IdentifierTerm(TRUE, List.of()))),
						new IdentifierTerm(
								new QualIdentifier(new Identifier(new SymbolToken("h"), List.of()), Optional.empty()),
								List.of())), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitMatch_term(invismt.parser.antlr.SMTLIBv2Parser.Match_termContext)
	 */
	@Test
	void shouldParseMatchTermCorrectly() {
		String content = "(match true ((s true)))";
		Term term = parser.parseInto(content);
		assertEquals(new MatchTerm(new IdentifierTerm(TRUE, List.of()),
				List.of(new MatchCase(new Pattern(new SymbolToken("s"), List.of()),
						new IdentifierTerm(TRUE, List.of())))), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitExists_term(invismt.parser.antlr.SMTLIBv2Parser.Exists_termContext)
	 */
	@Test
	void shouldParseExistsTermCorrectly() {
		String content = "(exists ((x Bool)) x)";
		Term term = parser.parseInto(content);
		assertEquals(new QuantifiedTerm.ExistsTerm(List.of(new SortedVariable(new SymbolToken("x"), BOOL)),
				new IdentifierTerm(X, List.of())), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitExists_term(invismt.parser.antlr.SMTLIBv2Parser.Exists_termContext)
	 */
	@Test
	void shouldParseForallTermCorrectly() {
		String content = "(forall ((x Bool)) x)";
		Term term = parser.parseInto(content);
		assertEquals(new QuantifiedTerm.ForAllTerm(List.of(new SortedVariable(new SymbolToken("x"), BOOL)),
				new IdentifierTerm(X, List.of())), term);
	}

	/**
	 * Test {@link TermParserDispatcher#parseInto(String)}.
	 *
	 * @see SMTLIBExpressionProductionVisitor#visitTerm(invismt.parser.antlr.SMTLIBv2Parser.TermContext)
	 * @see SMTLIBExpressionProductionVisitor#visitAttribute(invismt.parser.antlr.SMTLIBv2Parser.AttributeContext)
	 * @see SMTLIBExpressionProductionVisitor#visitAttribute_value(invismt.parser.antlr.SMTLIBv2Parser.Attribute_valueContext)
	 */
	@Test
	void shouldParseAnnotationTermCorrectly() {
		String content = "(! true :named p)";
		Term term = parser.parseInto(content);
		assertEquals(new AnnotationTerm(new IdentifierTerm(TRUE, List.of()),
				List.of(new Attribute(new Token.Keyword(":named"),
						Optional.of(new AttributeValue.SymbolAttributeValue(new SymbolToken("p")))))), term);
	}

}
