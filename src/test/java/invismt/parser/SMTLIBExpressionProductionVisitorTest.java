/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.parser;

import invismt.grammar.Expression;
import invismt.grammar.SpecConstant;
import invismt.grammar.Token.Keyword;
import invismt.grammar.Token.SymbolToken;
import invismt.grammar.attributes.Attribute;
import invismt.grammar.attributes.AttributeValue.ConstantAttributeValue;
import invismt.grammar.attributes.AttributeValue.ListAttributeValue;
import invismt.grammar.identifiers.Identifier;
import invismt.grammar.sorts.Sort;
import invismt.grammar.term.Pattern;
import invismt.grammar.term.QualIdentifier;
import invismt.parser.antlr.SMTLIBv2Parser;
import invismt.parser.antlr.SMTLIBv2Parser.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests {@link SMTLIBExpressionProductionVisitor}. This class only tests
 * specific contexts that could not be easily tested via {@link TermParseTest}
 * or {@link ScriptParserTest}.
 *
 * @author Tim Junginger
 */
class SMTLIBExpressionProductionVisitorTest {

	private SMTLIBExpressionProductionVisitor visitor;

	/**
	 * Create visitor for each test anew.
	 */
	@BeforeEach
	void init() {
		visitor = new SMTLIBExpressionProductionVisitor();
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitSpec_constant(invismt.parser.antlr.SMTLIBv2Parser.Spec_constantContext)}.
	 */
	@Test
	void shouldReturnNullWhenConstantMoreThanOneChild() {
		Spec_constantContext ctx = mock(Spec_constantContext.class);
		when(ctx.getChildCount()).thenReturn(2);
		assertNull(visitor.visitSpec_constant(ctx));
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitSpec_constant(invismt.parser.antlr.SMTLIBv2Parser.Spec_constantContext)}.
	 */
	@Test
	void shouldReturnNullWhenUnknownConstant() {
		Spec_constantContext ctx = mock(Spec_constantContext.class);
		when(ctx.getChildCount()).thenReturn(1);
		assertNull(visitor.visitSpec_constant(ctx));
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitTerm(TermContext)}.
	 */
	@Test
	void shouldReturnNullWhenUnknownTerm() {
		TermContext ctx = mock(TermContext.class);
		when(ctx.getChildCount()).thenReturn(1);
		assertNull(visitor.visitTerm(ctx));
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitAttribute(AttributeContext)}.
	 */
	@Test
	void shouldReturnAttributeWithOnlyOneChild() {
		SMTLIBv2Parser.KeywordContext tree = mock(SMTLIBv2Parser.KeywordContext.class);
		AttributeContext ctx = mock(AttributeContext.class);
		Keyword word = mock(Keyword.class);
		when(ctx.getChildCount()).thenReturn(1);
		when(ctx.keyword()).thenReturn(tree);
		when(tree.accept(visitor)).thenReturn(word);
		Expression e = visitor.visitAttribute(ctx);
		assertNotNull(e);
		assert (e.getClass() == Attribute.class);
		assertSame(word, ((Attribute) e).getKeyword());
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitAttribute_value(Attribute_valueContext)}.
	 */
	@Test
	void shouldReturnAttributeValueWithConstant() {
		Spec_constantContext tree = mock(Spec_constantContext.class);
		Attribute_valueContext ctx = mock(Attribute_valueContext.class);
		SpecConstant c = mock(SpecConstant.class);
		when(ctx.getChildCount()).thenReturn(1);
		when(ctx.spec_constant()).thenReturn(tree);
		when(tree.accept(visitor)).thenReturn(c);

		Expression e = visitor.visitAttribute_value(ctx);
		assertNotNull(e);
		assert (e.getClass() == ConstantAttributeValue.class);
		ConstantAttributeValue av = (ConstantAttributeValue) e;
		assertSame(c, av.getConstant());
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitAttribute_value(Attribute_valueContext)}.
	 */
	@Test
	void shouldReturnAttributeValueWithSExpressions() {
		SMTLIBv2Parser.S_exprContext tree = mock(SMTLIBv2Parser.S_exprContext.class);
		Attribute_valueContext ctx = mock(Attribute_valueContext.class);
		SymbolToken token = mock(SymbolToken.class);
		when(ctx.getChildCount()).thenReturn(4);
		when(ctx.s_expr()).thenReturn(List.of(tree, tree));
		when(tree.accept(visitor)).thenReturn(token);

		Expression e = visitor.visitAttribute_value(ctx);
		assertNotNull(e);
		assert (e.getClass() == ListAttributeValue.class);
		ListAttributeValue av = (ListAttributeValue) e;
		assertSame(token, av.getChildren().get(0));
		assertSame(token, av.getChildren().get(1));
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitAttribute_value(Attribute_valueContext)}.
	 */
	@Test
	void shouldReturnNullWhenUnknownAttribute() {
		Attribute_valueContext ctx = mock(Attribute_valueContext.class);
		when(ctx.getChildCount()).thenReturn(1);
		assertNull(visitor.visitAttribute_value(ctx));
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitQual_identifier(Qual_identifierContext)}.
	 */
	@Test
	void shouldReturnQualIdentifierWithAs() {
		Qual_identifierContext ctx = mock(Qual_identifierContext.class);
		IdentifierContext identifierCtx = mock(IdentifierContext.class);
		SortContext sortCtx = mock(SortContext.class);
		Identifier identifier = mock(Identifier.class);
		Sort sort = mock(Sort.class);
		when(ctx.getChildCount()).thenReturn(3);
		when(ctx.identifier()).thenReturn(identifierCtx);
		when(ctx.sort()).thenReturn(sortCtx);
		when(identifierCtx.accept(visitor)).thenReturn(identifier);
		when(sortCtx.accept(visitor)).thenReturn(sort);

		Expression e = visitor.visitQual_identifier(ctx);
		assertNotNull(e);
		assert (e.getClass() == QualIdentifier.class);
		QualIdentifier i = (QualIdentifier) e;
		assertSame(identifier, i.getQualifiedIdentifier());
		assertSame(sort, i.getIdentifiedSort().get());
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitPattern(invismt.parser.antlr.SMTLIBv2Parser.PatternContext)}.
	 */
	@Test
	void shouldReturnPatternWithMoreThanOneChild() {
		ParseTree tree = mock(ParseTree.class);
		PatternContext ctx = mock(PatternContext.class);
		SymbolToken token = mock(SymbolToken.class);
		when(ctx.getChildCount()).thenReturn(5);
		when(ctx.getChild(1)).thenReturn(tree);
		when(ctx.getChild(2)).thenReturn(tree);
		when(ctx.getChild(3)).thenReturn(tree);
		when(tree.accept(visitor)).thenReturn(token);

		Expression e = visitor.visitPattern(ctx);
		assertNotNull(e);
		assert (e.getClass() == Pattern.class);
		Pattern p = (Pattern) e;
		assertSame(token, p.getSymbol());
		assertSame(token, p.symbolList().get(0));
		assertSame(token, p.symbolList().get(1));
	}

	/**
	 * Tests
	 * {@link SMTLIBExpressionProductionVisitor#visitSort(SortContext)}.
	 */
	@Test
	void shouldReturnSortWithMultipleChildren() {

		// (identifier sort sort)

		IdentifierContext tree_identifier = mock(IdentifierContext.class);
		SortContext tree_sort = mock(SortContext.class);
		SortContext ctx = mock(SortContext.class);
		Identifier identifier = mock(Identifier.class);
		Sort sort = mock(Sort.class);
		when(ctx.getChildCount()).thenReturn(5);
		when(ctx.identifier()).thenReturn(tree_identifier);
		when(ctx.sort()).thenReturn(List.of(tree_sort, tree_sort));
		when(tree_sort.accept(visitor)).thenReturn(sort);
		when(tree_identifier.accept(visitor)).thenReturn(identifier);

		Expression e = visitor.visitSort(ctx);
		assertNotNull(e);
		assert (e.getClass() == Sort.class);
		Sort s = (Sort) e;
		assertSame(identifier, s.getSortIdentifier());
		assertSame(sort, s.getSorts().get(0));
		assertSame(sort, s.getSorts().get(1));
	}

}
