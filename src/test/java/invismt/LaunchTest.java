/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt;

import javafx.application.Application;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisabledIfEnvironmentVariable(named = "INVISMT_TEST_ENV", matches = "GitLab")
class LaunchTest {

	private static final int TIMEOUT_MS = 3000;

	private volatile boolean success = false;

	@Test
	@DisplayName("should launch application without uncaught exceptions")
	void shouldLaunchAppWithoutUncaughtExceptions() {
		Thread thread = new Thread(() -> {
			try {
				Application.launch(App.class);
				success = true;
			} catch (Throwable t) {
				if (t.getCause() != null && t.getCause().getClass().equals(InterruptedException.class)) {
					success = true;
				}
			}
		});

		thread.setDaemon(true);
		thread.start();

		try {
			Thread.sleep(TIMEOUT_MS);
		} catch (InterruptedException ignored) {
		}

		thread.interrupt();

		try {
			thread.join();
		} catch (InterruptedException ignored) {
		}

		assertTrue(success);
	}

}
