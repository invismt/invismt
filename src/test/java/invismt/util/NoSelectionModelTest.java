/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.util;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;

class NoSelectionModelTest {

	NoSelectionModel noSelectionModel;

	/**
	 * Build {@link NoSelectionModel} Constructor
	 */
	@BeforeEach
	void build() {
		noSelectionModel = new NoSelectionModel<>();
	}

	/**
	 * Test {@link NoSelectionModel#getSelectedIndices}
	 */
	@Test
	void shouldSelectedIndicesReturnEmpty() {
		assertSame(noSelectionModel.getSelectedIndices(), FXCollections.emptyObservableList());
	}

	/**
	 * Test {@link NoSelectionModel#getSelectedIndices}
	 */
	@Test
	void shouldSelectedItemsReturnEmpty() {
		assertSame(noSelectionModel.getSelectedIndices(), FXCollections.emptyObservableList());
	}

	/**
	 * Test {@link NoSelectionModel#isSelected}
	 */
	@Test
	void shouldReturnFalseBySelected() {
		assertFalse(noSelectionModel.isSelected(0));
	}

	/**
	 * Test {@link NoSelectionModel#isSelected}
	 */
	@Test
	void shouldReturnFalseByEmpty() {
		assertFalse(noSelectionModel.isSelected(0));
	}
}