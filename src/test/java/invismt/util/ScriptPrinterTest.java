/*******************************************************************************
 * Copyright (c) 2021 The InViSMT Development Team
 * 	
 * This file is part of InViSMT.
 *     
 * InViSMT is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * InViSMT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with InViSMT.  If not, see <https://www.gnu.org/licenses/>.
 *******************************************************************************/
package invismt.util;

import invismt.grammar.Script;
import invismt.testutil.FileToScriptUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class ScriptPrinterTest {

	ScriptPrinter scriptPrinter;

	@BeforeEach
	void setUp() {
		scriptPrinter = new ScriptPrinter();
	}

	@Test
	void createTempSolverFile() {
		Path path = FileToScriptUtil.getFile("UtilTestFile.smt2");
		Script script;
		try {
			script = FileToScriptUtil.toScript(path);
			File file = ScriptPrinter.createTempSolverFile(script);
			assertEquals(Files.readString(file.toPath()), """
					(declare-fun IsNat (Int) Bool)
					(assert (IsNat 1))
					(check-sat)
					""");
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void shouldReturnEmptyStringByPrintWithEmptyList() {
		assertEquals("", scriptPrinter.print(new Script(new LinkedList<>())));
	}

	@Test
	void shouldReturnFileToStringByPrintWithScript() {
		Path path = FileToScriptUtil.getFile("UtilTestFile.smt2");
		Script script;
		try {
			script = FileToScriptUtil.toScript(path);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
			return;
		}
		assertEquals(scriptPrinter.print(script), """
				(declare-fun IsNat (Int) Bool)
				(assert (IsNat 1))
				(check-sat)
				""");
	}
}