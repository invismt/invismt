(declare-const a Bool)
(declare-const b Bool)
(assert (= (and a b) (not ( or (not a) (not b)))))
(check-sat)