(assert (and (forall ((y Int)) (= 0 y)) (forall ((x Int) (y Int)) (=> (= x y) (and (= (+ x 1) y) (= (- x 1) y))))))
(check-sat)