(set-option :print-success true)
(set-option :produce-unsat-cores true)
(set-option :produce-models true)
; Declaration of sorts.
(declare-sort u 0)
; Predicates used in formula:
(declare-fun p_0 () Bool)
(declare-fun q_1 () Bool)
(assert
  (not
    ; The formula to be proved:
    (=> (=> p_0 q_1) (=> (not q_1) (not p_0)))))
; End of assert.
(check-sat)
; end of smt problem declaration
