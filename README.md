# InViSMT

The goal of this project is to provide a visual user interface to simplify the work with SMT-problems.

## Motivation

This project was created in the context of "Praxis der Software Entwicklung"
(practice of software development) for the university course "Informatik B.Sc."
(computer science)
from KIT (https://www.kit.edu/, 08.09.2021 at 18:13) 2021.

## Status

Currently working on a command line interface for InViSMT that mimics the command line interface of Z3. This would allow InViSMT to be used in between other verification tools and Z3 to improve understanding of the problems to verify and make small manual changes to the problem even in toolchains that normally would not allow interaction.

Known major or important bugs:

- Rule applications sometimes aren't possible as the expression that the user clicked on is not always correctly recognized, depending on the chosen Formatter.

Many unknown bugs may exist.

Functions that we aim to add in the future:

- Add more inference rules and improve existing ones (e.g. make Skolemization applicable everywhere instead of just to top level terms).
- Export the whole proof state of an SMT problem and make it possible to open that state again at a later time: merge all its deduced problems together with their current satisfiability status.
- Add a dark mode to the GUI.

## Requirements

* InViSMT uses the SMT-LIB-Standard (http://smtlib.cs.uiowa.edu/, 28.08.2021 at 23:57) in version 2.6.

* InViSMT requires either **Windows 10**, **macOS 11.3**, **Debian 11**, **Devuan 4** or **Ubuntu 21.04**.
  Similar operation systems might work but are **not** explicitly supported.

* InViSMT also requires a **Java Runtime Environment Version 16** to be executed.
  (possibly from here: https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=hotspot, 28.08.2021 at 23:57)

* It is also advised to install Microsoft Z3 in version 4.8.10 or similar.
  (from here: https://github.com/Z3Prover/z3, 28.08.2021 at 23:57)

## Installation

To install InViSMT follow these steps:

1. Download the repository from https://gitlab.com/invismt/invismt.

2. If you have Gradle installed you can execute `gradle shadowJar` inside of the downloaded repository. If you do not have a local Gradle installation, 
the repository contains `gradlew`, the Gradle wrapper. This wrapper can be used like the actual Gradle executable, i.e. `gradlew shadowJar`.

3. In the folder *invimst/build/libs* you can find the file **InViSMT-all.jar**.

4. (Optional) Move the file **InViSMT-all.jar** to any location you wish.

5. (Optional) Delete the repository.

6. You can now run **InViSMT-all.jar** with Java.

## Configuration

InViSMT uses some configuration files in the directory *src/main/resources/config*:

* **solvers.txt** contains the name of solverspecific configuration files. These files will be read on startup to
  determine the arguments and names of available solvers. These files should contain the property `solvertype`.
  If `solvertype=Z3Solver`, the properties `name` and `args` are required too where `args` is the path to the executable
  followed by command line arguments.

* **shortcuts.properties** contains a mapping of actions available in this program and corresponding hotkeys/shortcuts.
  (uses https://docs.oracle.com/javase/8/javafx/api/javafx/scene/input/KeyCombination.html)

* **PrettyPrinter.json** contains the configuration of the used pretty printer. It contains a list of identifiers which
  each have the following attributes:
    * `name`: the name of the identifier in the SMT-LIB standard
    * `alias`: an alias to print instead
    * `notation`: `infix`, `prefix` or `PN` for Polish notation, `RPN` or `postfix` for reverse Polish notation
    * `useParentheses`: (optional) either `true` or `false`

## User Manual

### Commandline arguments

InViSMT aims to support all common commandline arguments from Z3 (and possible other solvers).

Currently supported commandline arguments:
* -version, --version
* -log, -verbose
* -in, --in 
* --debug, --help (not fully implemented?)

When using the option -in the button to open a file will instead try to open a smt-problem description from stdin.
This will block until such a problem is recognized which means until (check-sat) is encountered.
This can be repeated to load multiple problems from stdin during the same session. In doing so InViSMT will output the current proof state of the last problem onto stdout to comply with the SMT-LIB standard.

### Buttons

![buttons](./misc/screenshots_readme/buttons.PNG)

1) **Open files** in the SMT-LIB standard with the file ending ".smt2".

2) **Save** an opened file in the SMT-LIB standard.

3) **Undo** the last action (Rule application or SMT solver start).

4) **Redo** the action that has been undone with 3 previously.

5) **Show** the currently opened script in the **SMT-LIB standard**.

6) **Show** the currently opened script **formatted by a PrettyPrinter**.

7) Activate or deactivate **type checking**:

   If activated (green check), the currently chosen SMT solver is used to semantically and syntactically check the
   currently opened script after each change (additionally to the limited checks that are done by InViSMT itself).

   If deactivated (red cross), no type-checking is done, e.g. there is no check whether variables entered by the user
   are declared anywhere in the opened script.



### ProofObligationView

![buttons](./misc/screenshots_readme/proofObligationView.PNG)

The ProofObligationView shows the single proof obligations that have been created out of the opened smt2-file caused by multiple *check-sat*-commands, *reset*-commands, etc.

These commands lead to SMT solvers checking multiple SMT problems (sets of assertions, called "assertion stacks") in one file. Each of these single checked SMT problems (together with other commands) makes up one proof obligation.



### ProofTreeView

![buttons](./misc/screenshots_readme/proofTreeView.PNG)

The ProofTreeView shows SMT problems that result from an initial proof obligation by applying inference rules (e.g. Cut, Induction, ...; see for example: https://en.wikipedia.org/wiki/List_of_rules_of_inference, 08.09.2021 at 19:33)

Each of these SMT problems can be given to an SMT solver individually.



### ScriptView

The ScriptView contains the content of a single SMT problem in the ProofTree. Some expressions may not be shown, depending on the chosen formatter.

Each SMT-LIB command is contained in a single list cell:

![buttons](./misc/screenshots_readme/scriptView.PNG)

1. **Scrollbar** that is only shown if an expression is longer than its list cell.

2. **Collapsing** button that causes expressions not to be shown.

   If the button is clicked, expressions are hidden:

   ![buttons](./misc/screenshots_readme/collapsed.PNG)

   If the button is clicked anew, hidden expressions are shown again.



### Inference Rules

When hovering on an *SMT-LIB* term and pressing right click, there will be a context menu with all the inference rules that are applicable to the chosen term.

![rules](./misc/screenshots_readme/rules.png)

There are currently 11 inference rules that can be applied to *SMT-LIB terms* inside of an *assert*-command in InViSMT:

- **Induction** is applicable to all *forall*-terms containing at least one sorted variable of sort *Int*; "(forall ((x Int)) A(x))" is changed to "(forall ((x Int)) (=> A(x) (and A(+ x 1) A(- x 1)))"; the rule is an equivalent conversion

- **Skolemization** is applicable to all top-level *exists*-terms; "(assert (exists ((x Sort)) A(x)))" is changed to "(declare-const x Sort) (assert (A(x)))"; the rule is an equivalent conversion

- **Instantiation** is applicable to all *forall*-terms; "(forall ((x Sort)) A(x))" is changed to "(and A(x_0) (forall ((x Sort)) (A(x))))" for an instance x_0; Note that type-checking is only guaranteed if *external type checking* is activated; the rule is an equivalent conversion

- **Rewrite** is applicable to every term that does not contain a free variable which is bound in one of its parent terms; creates two new SMT problems out of one by replacing "F(s)" by "(and F(s) (not (= s t)))" or "(and F(t) (= s t))", respectively; the rule is an equivalent conversion

- **Cut** is applicable to every top-level term; creates two new SMT problems out of one by adding "(assert b)" or "(assert (not b))", respectively; the rule is an equivalent conversion

- **SplitAnd** is applicable to every top-level *and*-term; replaces "(assert (and a b))" by "(assert a) (assert b)"; the rule is an equivalent conversion

- **SplitOr** is applicable to every top-level *or*-term; creates two new SMT problems out of one by replacing "(assert (or a b))" by "(assert a)" or "(assert b)", respectively; the rule is an equivalent conversion

- **DoubleNegation** is applicable to every term that starts with two negations and removes these negations; replaces "(not (not a))" by "a"; the rule is an equivalent conversion

- **DeMorgan** is applicable to any term starting with an *and*, *or*, *forall* or *exists*; generally replaces "(not (forall ...))" by "(exists (not ...))" and vice versa as well as "(not (and a b))" by "(or (not a) (not b))", etc.; the rule is an equivalent conversion

- **Hide** is applicable to every top-level term that does not contain an *annotation*-term whenever there are at least two assertions in a script; removes one assertion from the script; the rule is not an equivalent conversion

- **RemoveAnnotations** is applicable to any *annotation*-term; copies the annotated term of an *annotation*-term without the annotations; the rule is an equivalent conversion

  

For more information on inference rules see for example https://en.wikipedia.org/wiki/List_of_rules_of_inference (28.09.2021 at 23:58).

### Solvers

**Installed SMT solvers** can be chosen in the **drop down menu**:

![buttons](./misc/screenshots_readme/solverChoice.PNG)

The currently chosen solver will be used whenever a solver is activated.

The start button next to the drop down menu activates an SMT solver on the currently opened script.



### Satisfiability and proof states

![buttons](./misc/screenshots_readme/cascade.PNG)

Depending on the applied inference rule (whether it is an equivalent conversion or not), closing one or more childs of an SMT problem may enable InViSMT to evaluate the satisfiability of the problem at hand from these children.

The program does that automatically, marking internally evaluated satisfiability states with a "+" at the end.

If a solver and the internal calculations differ (e.g. UNSAT vs. SAT+) there will be a warning.

A solver can still be started on an SMT problem with an internally calculated satisfiability state, but not one one that has already been solved by an external SMT solver.
